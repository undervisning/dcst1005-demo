Get-AzContext
Get-AzTenant
$tenants = Get-AzTenant | Where-object { $_.name -like "UNDERVISNING DIGSEC" }
$tenants.id
$tenants.Name
$tenants.domains

Set-AzContext -Tenant $tenants.id
Get-AzContext


$LocationName = "west europe"
$ResourceGroupName = "hub-spok-tim-demo2"
$vnetName = "hub-vnet-demo2"
$defaultSubnet = "ManagementSubnet-demo2"
$vnetAddressSpace = "10.10.0.0/19"
$subnetvnetAddressSpace = "10.10.1.0/24"

$vnetName = "spoke1-vnet-demo2"
$defaultSubnet = "WorkloadSubnet"
$vnetAddressSpace = "10.20.0.0/19"
$subnetvnetAddressSpace = "10.20.1.0/24"

$vnetName = "spoke2-vnet-demo2"
$vnetAddressSpace = "10.30.0.0/19"
$subnetvnetAddressSpace ="10.30.1.0/24"

$hubvnetName = "hub-vnet-demo2"
$spoke1vnetName = "spoke1-vnet-demo2"
$spoke2vnetName = "spoke2-vnet-demo2"

$kvName = "KeyVault-tim-demo-test"
$secretName = "vm-password"
$localAdminUsername = "melling"

############################
# AZURE HUB SPOKE - Del 2  #
############################

# Create VM
# - Scriptbit kommer

# Deretter må vi installere noe som kan nås på nettet
# Skal denne gangen kjøre en enkel kommando som installerer IIS, som gjør det enkelt å sjekke om en kan nå port 80.

# Install IIS
$PublicSettings = '{"commandToExecute":"powershell Add-WindowsFeature Web-Server"}'

Set-AzVMExtension `
        -ExtensionName "IIS" `
        -ResourceGroupName $ResourceGroupName `
        -VMName "tim-vm-spoke2-demo2" `
        -Publisher "Microsoft.Compute" `
        -ExtensionType "CustomScriptExtension" -TypeHandlerVersion 1.4 `
        -SettingString $PublicSettings -Location $LocationName

# DEMO VPN og IIS / RDP

# MERK: Lokal maskin når ikke spoke 1 og spoke 2
# Konfigurer lokal maskin til å nå spoke1 og spoke2
# Konfigurer alle peering-tilkoblinger for å tillate videresendt trafikk.
# Konfigurer peering-forbindelsen i hub å tillate gateway "gjennomreise".

# Update Hub to Spoke 1.
$LinkHubToSpoke1 = Get-AzVirtualNetworkPeering `
                      -VirtualNetworkName $hubvnetName `
                      -ResourceGroupName $resourceGroupName `
                      -Name "LinkHubToSpoke1-demo2"

$LinkHubToSpoke1.AllowGatewayTransit = $True
$LinkHubToSpoke1.AllowForwardedTraffic = $True

Set-AzVirtualNetworkPeering -VirtualNetworkPeering $LinkHubToSpoke1


# Update Hub to Spoke 2.
$LinkHubToSpoke2 = Get-AzVirtualNetworkPeering `
                      -VirtualNetworkName $hubvnetName `
                      -ResourceGroupName $resourceGroupName `
                      -Name "LinkHubToSpoke2-demo2"

$LinkHubToSpoke2.AllowGatewayTransit = $True
$LinkHubToSpoke2.AllowForwardedTraffic = $True

Set-AzVirtualNetworkPeering -VirtualNetworkPeering $LinkHubToSpoke2

# Konfigurer peering-tilkoblingen i hver spoke til å bruke eksterne gateways.
$LinkSpoke1ToHub = Get-AzVirtualNetworkPeering `
                      -VirtualNetworkName $spoke1vnetName `
                      -ResourceGroupName $resourceGroupName `
                      -Name "LinkSpoke1ToHub-demo"

$LinkSpoke1ToHub.UseRemoteGateways = $True
$LinkSpoke1ToHub.AllowForwardedTraffic = $True

Set-AzVirtualNetworkPeering -VirtualNetworkPeering $LinkSpoke1ToHub

# Update Spoke 2 to Hub.
$LinkSpoke2ToHub = Get-AzVirtualNetworkPeering `
                      -VirtualNetworkName $spoke2vnetName `
                      -ResourceGroupName $resourceGroupName `
                      -Name "LinkSpoke2ToHub-demo2"

$LinkSpoke2ToHub.UseRemoteGateways = $True
$LinkSpoke2ToHub.AllowForwardedTraffic = $True

Set-AzVirtualNetworkPeering -VirtualNetworkPeering $LinkSpoke2ToHub

# Etter at peering for virtuelt nettverk er etablert,
# last ned og installer P2S-client på nytt,
# slik at P2S-Client får de oppdaterte rutene til det virtuelle spoke-nettverket

# Azure Firewall
# Implementering av Azure Firewall i Hub-Spoke nettverkstopologi i Azure
# Vi er nå stand til å kommunisere mellom hub og spoke, men det er ingen kommunikasjon mellom spokes.
# I dette innlegget skal vi se litt på hvordan Intra-Region Spokes VNET-ruting fungerer.

# Vi starter med å opprette Azure Firewall. I likhet med gateway-subnettet, trenger Azure-brannmur også et dedikert subnet.
# Siden Azure brannmuren er en delt tjeneste (benyttes av spokes og den styrer trafikken til flere), setter vi den i hub-vnet.
# Lager et AzurefirwallSubnet i hub-vnet. Azure Firewall trenger et / 26 subnettstørrelse fordi
# en må sørge for at brannmuren har nok IP-adresser tilgjengelig for å spinne opp flere VM instanser når den skaleres.

$vnetName = "hub-vnet-demo2"
$azfirewallvnetAddressSpace = "10.10.10.0/26"

$hubVnet = Get-AzVirtualNetwork -Name $vnetName

# Subnet for AzFirewall må hete AzureFirewallSubnet
$azureFirewallSubnet = Add-AzVirtualNetworkSubnetConfig `
                          -Name "AzureFirewallSubnet" `
                          -AddressPrefix $azfirewallvnetAddressSpace `
                          -VirtualNetwork $hubVnet

$updateHubVnet = Set-AzVirtualNetwork -VirtualNetwork $hubVnet

# Når subnettet er det klart for å deploye Azure Firewall. Vi trenger også en offentlig ip til brannmuren.
$azFirewallName = "HubAzFirewall-demo2"
# Public IP
$firewallPip = New-AzPublicIpAddress `
                  -ResourceGroupName $ResourceGroupName `
                  -Location $LocationName `
                  -AllocationMethod Static `
                  -Sku Standard `
                  -Name "$($azFirewallName)-PublicIp"

# Oppretter Firewall
$azFirewall = New-AzFirewall `
                 -Name $azFirewallName `
                 -ResourceGroupName $ResourceGroupName  `
                 -Location $LocationName `
                 -VirtualNetworkName $updateHubVnet.Name`
                 -PublicIpName $firewallPip.Name

# Når vi er ferdig med utrullingen, må vi lage en user defined route sånn at all outbound flow går først til azure firewall.
# Oppretter Route Table
$spoke1ToAzFwUdr = New-AzRouteTable `
                      -Name 'Spoke1ToAzFwUdr-demo2' `
                      -ResourceGroupName $ResourceGroupName `
                      -location $LocationName

$updatedSpoke1ToAzFwUdr = $spoke1ToAzFwUdr `
                            | Add-AzRouteConfig `
                                 -Name "ToAzureFirewall-demo2" `
                                 -AddressPrefix 0.0.0.0/0 `
                                 -NextHopType "VirtualAppliance" `
                                 -NextHopIpAddress $azFirewall.IpConfigurations.privateipaddress `
                            | Set-AzRouteTable

# Deretter vil vi legge til rutetabell i subnett som de trenger for å kommunisere hverandre.
# I vårt tilfelle må WorkloadSubnet på spoke1-vnet kommunisere med WorkloadSubnet på spoke2-vnet.
# Så vi vil knytte nyopprettet UDR (user defined route) til begge subnettene.

$subNetinspokeVnet ="WorkloadSubnet-demo2"
$spoke1VnetName = "spoke1-vnet-demo2"
                        
$spoke1Vnet = Get-AzVirtualNetwork `
                            -Name $spoke1VnetName

$WorkloadSubnetAddressSpaceForSpoke1 = "10.20.1.0/24"

$spoke1Vnet =  Set-AzVirtualNetworkSubnetConfig `
                    -VirtualNetwork $spoke1Vnet `
                    -Name $subNetinspokeVnet `
                    -AddressPrefix $WorkloadSubnetAddressSpaceForSpoke1 `
                    -RouteTable $spokeToAzFwUdr | `
                Set-AzVirtualNetwork

$spoke2VnetName = "spoke2-vnet-demo2"
                        
$spoke2Vnet = Get-AzVirtualNetwork `
                            -Name $spoke2VnetName

$WorkloadSubnetAddressSpaceForSpoke2 = "10.30.1.0/24"

$spoke2Vnet =  Set-AzVirtualNetworkSubnetConfig `
                    -VirtualNetwork $spoke2Vnet `
                    -Name $subNetinspokeVnet `
                    -AddressPrefix $WorkloadSubnetAddressSpaceForSpoke2 `
                    -RouteTable $spokeToAzFwUdr | `
                Set-AzVirtualNetwork


# Nå er Next Hop for hver forespørsel som genereres fra Spoke1 og Spoke2 Azure-brannmuren,
# vi trenger deretter bare å tillate flyten fra WorkloadSubnet for spoke1-vnet til WorkloadSubnet for spoken2-vnet.
# Dette må gjøres ved å opprette en regel i Azure Firewall.

$sourceAddress = "10.20.1.0/24"

$destinationAddress ="10.30.1.0/24"

$spoke1ToSpoke2 = New-AzFirewallNetworkRule `
                     -Name "Allow-Spoke2-Routing-demo2" `
                     -Protocol TCP `
                     -SourceAddress $sourceAddress `
                     -DestinationAddress $destinationAddress `
                     -DestinationPort *
           
$netRuleCollection = New-AzFirewallNetworkRuleCollection `
                        -Name "Spoke-to-Spoke-demo2" `
                        -Priority 1000 `
                        -Rule $spoke1ToSpoke2 `
                        -ActionType "Allow"
           
$azFirewall.NetworkRuleCollections.Add($netRuleCollection)
$updatedFirewall = Set-AzFirewall -AzureFirewall $azFirewall


# Av standard blokker Azure Firewall all trafikk så lenge annet ikke er konfigurert
# Vi må derfor konfigurere Az Firewall for å tillatte kommunikasjon med omverden, og kanskje bare spesifikke adresser?
# Her tillater vi google.

$allowGoogle = New-AzFirewallApplicationRule `
                  -Name "Allow-Google-demo2"  `
                  -SourceAddress $sourceAddress `
                  -Protocol http, https  `
                  -TargetFqdn www.google.com

$appRuleCollection = New-AzFirewallApplicationRuleCollection `
                        -Name "Allow-Internet-demo2" `
                        -Priority 2000 `
                        -ActionType Allow `
                        -Rule $allowGoogle `

$azFirewall.ApplicationRuleCollections.Add($appRuleCollection)
$updatedFirewall = Set-AzFirewall -AzureFirewall $azFirewall


# En kan også kontrollere inngående internettrafikk med Azure Firewall DNAT. 
# Hvis f.eks. VPN-en tjenesten ikke fungerer, men en må få tilgang til Hub-VM-en som bare har privat ip.
# For å få tilgang på den kan en DNAT-regel.

$rdpToHubJumpbox =  New-AzFirewallNatRule `
                       -Name "RDP To Hub VM-demo2" `
                       -Protocol "TCP" `
                       -SourceAddress * `
                       -DestinationAddress $firewallPip.IpAddress `
                       -DestinationPort "4500" `
                       -TranslatedAddress "10.10.1.4" `
                       -TranslatedPort "3389"


$natRuleCollection = New-AzFirewallNatRuleCollection `
                       -Name "Allow-RDP-To-Hub-VM-demo2" `
                       -Priority 500 `
                       -Rule $rdpToHubJumpbox

$azFirewall.NatRuleCollections.Add($natRuleCollection)

$updatedFirewall = Set-AzFirewall -AzureFirewall $azFirewall

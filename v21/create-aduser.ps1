$Password = Read-Host -Prompt 'Enter Password' -AsSecureString

New-ADUser -Name "Tor Ivar Melling" `
 -GivenName "Tor Ivar" `
 -Surname "Melling" `
 -SamAccountName  "melling" `
 -UserPrincipalName  "melling@undervisning.sec" `
 -Path "CN=Users,DC=undervisning,DC=sec" `
 -AccountPassword $Password -Enabled $true
 
 Add-ADGroupMember -Identity Administrators -Members melling
 Get-ADGroupMember -Identity Administrators | Select-Object Name

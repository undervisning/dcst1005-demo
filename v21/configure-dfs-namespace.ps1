# - DFS Namespaces - SRV1 - #
Enter-PSSession tim-srv1
Get-WindowsFeature -Name fs*
Install-WindowsFeature -Name FS-DFS-Namespace,FS-DFS-Replication,RSAT-DFS-Mgmt-Con -IncludeManagementTools
Get-WindowsFeature -Name fs*
Import-Module dfsn
# cd \
$folders = (‘C:\dfsroots\files’,’C:\shares\sales’,’C:\shares\accounts’,’C:\shares\production’) 
mkdir -path $folders
$folders | ForEach-Object {$sharename = (Get-Item $_).name; New-SMBShare -Name $shareName -Path $_ -FullAccess Everyone}

#MERK: tim-srv1 er navnet på min server - undervisning.sec er mitt domene. Denne informasjonen må erstattes i henhold til eget oppsett.
New-DfsnRoot -TargetPath \\tim-srv1\files -Path \\undervisning.sec\files -Type DomainV2
$folders | Where-Object {$_ -like "*shares*"} | ForEach-Object {$name = (Get-Item $_).name; $DfsPath = (‘\\undervisning.sec\files\’ + $name); $targetPath = (‘\\tim-srv1\’ + $name);New-DfsnFolderTarget -Path $dfsPath -TargetPath $targetPath}
dir \\undervisning.sec\files

# - Legge til share fra DC1 (bare for å vise at det kan være forskjellige servere som aksesseres fra samme nanv) - #
Enter-PSSession tim-dc1
Get-WindowsFeature -Name fs*
Install-WindowsFeature -Name FS-DFS-Namespace,FS-DFS-Replication,RSAT-DFS-Mgmt-Con -IncludeManagementTools
Get-WindowsFeature -Name fs*
Import-Module dfsn
#$folders = (‘C:\dfsroots\files’,’C:\shares\installerfiles’,’C:\shares\projects’)
$folders = (’C:\shares\it-admins’)
mkdir -path $folders
$folders | ForEach-Object {$sharename = (Get-Item $_).name; New-SMBShare -Name $shareName -Path $_ -FullAccess Everyone}

#MERK: tim-dc1 er navnet på min server - undervisning.sec er mitt domene. Denne informasjonen må erstattes i henhold til eget oppsett.
New-DfsnRoot -TargetPath \\tim-dc1\files -Path \\undervisning.sec\files -Type DomainV2
$folders | Where-Object {$_ -like "*shares*"} | ForEach-Object {$name = (Get-Item $_).name; $DfsPath = (‘\\undervisning.sec\files\’ + $name); $targetPath = (‘\\tim-dc1\’ + $name);New-DfsnFolderTarget -Path $dfsPath -TargetPath $targetPath}
dir \\undervisning.sec\files




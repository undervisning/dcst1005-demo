#Run Get-NetAdapter to find correct InterfaceIndex
Get-NetAdapter
# Enter corret interfaceIndex in variabel
$ifindex=""
$ipaddress="172.16.230.101"
$gateway="172.16.230.1"
$dns1="172.16.230.1"
$prefix="24" # prefix må settes etter hvilken nettmaske en benytter 255.255.252. benytter prefix 22.

$vmname="tim-dc1"

New-NetIPAddress -InterfaceIndex $ifindex -IPAddress $ipaddress -PrefixLength $prefix -DefaultGateway $gateway
Set-DnsClientServerAddress -InterfaceIndex $ifindex -ServerAddresses ("$dns1")

# Remote Desktop check - Remember to edit port if non-standard port is in use:
Test-NetConnection $ipaddress -Port 3389
Set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Control\Terminal Server'-name "fDenyTSConnections" -Value 0
Enable-NetFirewallRule -DisplayGroup "Remote Desktop"


# Hente informasjon om maskinen:
Get-ComputerInfo
Get-ComputerInfo | Select-Object CsDNSHostName
Rename-Computer -NewName $vmname -Restart

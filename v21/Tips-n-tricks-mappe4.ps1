Get-AzContext
Get-AzTenant
$tenants = Get-AzTenant | Where-object { $_.name -like "UNDERVISNING DIGSEC" }
$tenants.id
$tenants.Name
$tenants.domains

Set-AzContext -Tenant $tenants.id
Get-AzContext


##########################################################
#                                                        #
# I delen av scriptet blir det opprettet:                #
# - tre vNet med subnet                                  #
# - hub-vnet-$initials                                   #
#   - AzureFirewallSubnet                                #
#       - Med AzureFirewall                              #
#   - GatewaySubnet                                      #
#       - Med AzVPN                                      #
#   - managmentSubnet                                    #
# - spoke1-vnet-$initials                                #
#   - vm-subnet                                          #
#       - Gateway transit                                #
#       - Use Remote Gateway                             #
# - spoke2-vnet-$initials                                #
#   - vm-subnet                                          #
#       - ingen konfigurasjon inntil videre              #
#                                                        #
##########################################################
#              Variabler brukt i scriptet                #
##########################################################
$initials = "demo" # for å gjøre det enkelt å endre min navngivning
$rgName = "hub-spoke-rg-$initials"
$LocationName = "west europe"

# Application security group
$businessspoke1subnetASG = "spoke1-business-subnet-ASG-$initials"
$dataspoke1subnetASG = "spoke1-data-subnet-ASG-$initials"
 
# Hub vNet
$hubvnetName = "hub-vnet-$initials"
$hubvnetaddressprefix = "10.10.0.0/19"
$hubfwsubnetName = "AzureFirewallSubnet" # Må være dette navnet
$hubfwsubnetaddressprefix = "10.10.10.0/26"
$hubvpnsubnetName = "GatewaySubnet" # Må være dette navnet
$hubvpnsubnetaddressprefix = "10.10.31.0/27"
$hubmgmtsubnetName = "managmenetSubnet"
$hubmgmtsubnetaddressprefix = "10.10.1.0/24"
$hubspoke1peeringName = "hub-spoke1-peering-$initials"
$hubspoke2peeringName = "hub-spoke2-peering-$initials"


# Spoke 1 vNet
$spoke1vnetName = "spoke1-vnet-$initials"
$spoke1vnetaddressprefix = "10.20.0.0/19"
$spoke1vnetsubnetName = "vm-subnet-$initials"
$spoke1vnetsubnetaddressprefix = "10.20.1.0/24"
$spoke1hubpeeringName = "spoke1-hub-peering-$initials"
$spoke1vnetbusinessSubnetName = "business-subnet-$initials"
$spoke1vnetbusinesssubnetaddressprefix = "10.20.2.0/24"
$spoke1vnetdataSubnetName = "data-subnet-$initials"
$spoke1vnetdatasubnetaddressprefix = "10.20.3.0/24"

# Spoke 2 vNet
$spoke2vnetName = "spoke2-vnet-$initials"
$spoke2vnetaddressprefix = "10.30.0.0/19"
$spoke2vnetsubnetName = "vm-subnet-$initials"
$spoke2vnetsubnetaddressprefix = "10.30.1.0/24"
$spoke2hubpeeringName = "spoke2-hub-peering-$initials"

# Azure VPN Gateway
$AzVPNgwPIpName = "azvpngw-pip-$initials"
$AzVPNgwName = "azvpngw-$initials"
$AzVPNgwConfig = "azvpngwConfig-$initials"
$azVPNgwClientAddressPool="192.168.0.0/24"

# Azure Firewall
$AzFirewallpipName = "azfw-pip-$initials"
$AzFirewallName = "azfw-$initials"
$AzFirewallRouteTableName = "azfirewallRouteTable-$initials"
$azFirewallDefaultRoute = "azfw-defaultroute-$initials"
$AzFirewallNatRuleName = "rpd-fw-$vmspoke1vm"
$AzFirewallNatRuleCollectionName = "azfw-rdp-collection"
 
# Network Security Group (NSG) - Knyttes til Subnet
$mgmtnsg = "hub-$hubmgmtsubnetName-$initials-nsg"
$vm1subnetnsg = "spoke1-$spoke1vnetsubnetName-nsg"
$vm2subnetnsg = "spoke2-$spoke2vnetsubnetName-nsg"
$businesssubnetnsg = "spoke1-$spoke1vnetbusinessSubnetName-nsg"
$datasubnetnsg = "spoke1-$spoke1vnetdataSubnetName-nsg"

# VM-er
$vmhubmgmg = "vm-hub-$initials"
$vmspoke1vm = "vm-spoke1-$initials"
$vmspoke2vm = "vm-spoke2-$initials"
$businessspoke1vm = "vm-s1-bn-$initials"
$dataspoke1vm = "vm-s1-data-$initials"

# Key Vault
$kvrandom = -join ((48..57) + (97..122) | Get-Random -Count 4 | % {[char]$_})
$kvName = "kv-$kvrandom-$initials"
$secretName = "vm-secret-$initials"
$localVMadmin = "melling"
 
#########################################################
#              Oppretter ressurser i Azure              #
#########################################################

#######################
# Lager Resource Group
#######################
$newrg = @{
    Name = $rgName
    Location = $LocationName
}
New-AzResourceGroup @newrg

################################################
# Lager Hub vNet med FW, GW og managementSubnet
################################################
$hubvnet = @{
    ResourceGroupName = $rgName
    Location = $LocationName
    Name = $hubvnetName
    AddressPrefix = $hubvnetaddressprefix
}
$hubvnetinfo = New-AzVirtualNetwork @hubvnet # $hubvnetinfo inneholder nødvendig informasjon til videre konfigurering

# FW subnet
$fwsn = @{
    Name = $hubfwsubnetName
    AddressPrefix = $hubfwsubnetaddressprefix
    VirtualNetwork = $hubvnetinfo
}
$fwsubnetinfo = Add-AzVirtualNetworkSubnetConfig @fwsn

# Legger til nylig opprettet subnet i hub-vnet
$hubvnetinfo = $hubvnetinfo | Set-AzVirtualNetwork

# GW Subnet
$gwsn = @{
    Name = $hubvpnsubnetName
    AddressPrefix = $hubvpnsubnetaddressprefix
    VirtualNetwork = $hubvnetinfo
}
$gwsubnetinfo = Add-AzVirtualNetworkSubnetConfig @gwsn

# Legger til nylig opprettet subnet i hub-vnet
$hubvnetinfo = $hubvnetinfo | Set-AzVirtualNetwork

# MGMT Subnet
$mgmtsn = @{
    Name = $hubmgmtsubnetName
    AddressPrefix = $hubmgmtsubnetaddressprefix
    VirtualNetwork = $hubvnetinfo
}
$mgmtsubnetinfo = Add-AzVirtualNetworkSubnetConfig @mgmtsn

# Legger til nylig opprettet subnet i hub-vnet
$hubvnetinfo = $hubvnetinfo | Set-AzVirtualNetwork

###################################
# Oppretter spokes vNet med subnet
###################################

# Oppretter spoke 1 vNet
$spoke1vnet = @{
    ResourceGroupName = $rgName
    Location = $LocationName
    Name = $spoke1vnetName
    AddressPrefix = $spoke1vnetaddressprefix
}
$spoke1vnetinfo = New-AzVirtualNetwork @spoke1vnet # $spoke1info inneholder nødvendig informasjon til videre konfigurering

# Oppretter subnet for spoke1
# VM Subnet
$vm1sn = @{
    Name = $spoke1vnetsubnetName
    AddressPrefix = $spoke1vnetsubnetaddressprefix
    VirtualNetwork = $spoke1vnetinfo
}
$vm1subnetinfo = Add-AzVirtualNetworkSubnetConfig @vm1sn

# Legger til nylig opprettet subnet i spoke1-vnet
$spoke1vnetinfo = $spoke1vnetinfo | Set-AzVirtualNetwork

$spoke2vnet = @{
    ResourceGroupName = $rgName
    Location = $LocationName
    Name = $spoke2vnetName
    AddressPrefix = $spoke2vnetaddressprefix
}
$spoke2vnetinfo = New-AzVirtualNetwork @spoke2vnet # $spoke1info inneholder nødvendig informasjon til videre konfigurering

# Oppretter subnet for spoke2
# VM Subnet
$vm2sn = @{
    Name = $spoke2vnetsubnetName
    AddressPrefix = $spoke2vnetsubnetaddressprefix
    VirtualNetwork = $spoke2vnetinfo
}
$vm2subnetinfo = Add-AzVirtualNetworkSubnetConfig @vm2sn

# Legger til nylig opprettet subnet i spoke2-vnet
$spoke2vnetinfo = $spoke2vnetinfo | Set-AzVirtualNetwork

##################################
# Peering mellom hub og spokes
##################################

$hubvnetinfo = Get-AzVirtualNetwork -Name $hubvnetName
$spoke1vnetinfo = Get-AzVirtualNetwork -Name $spoke1vnetName
$spoke2vnetinfo = Get-AzVirtualNetwork -Name $spoke2vnetName

# Peer Hub to Spoke 1.
$hubtospoke1 = @{
    Name = $hubspoke1peeringName
    VirtualNetwork = $hubvnetinfo
    RemoteVirtualNetworkId = $spoke1vnetinfo.Id
}
Add-AzVirtualNetworkPeering @hubtospoke1

# Peer Spoke 1 to Hub
$spoke1tohub = @{
    Name = $spoke1hubpeeringName
    VirtualNetwork = $spoke1vnetinfo
    RemoteVirtualNetworkId = $hubvnetinfo.Id
}
Add-AzVirtualNetworkPeering @spoke1tohub

# Peer Hub to Spoke 2
$hubtospoke2 = @{
    Name = $hubspoke2peeringName
    VirtualNetwork = $hubvnetinfo
    RemoteVirtualNetworkId = $spoke2vnetinfo.Id
}
Add-AzVirtualNetworkPeering @hubtospoke2

# Peer Spoke 2 to Hub.
$spoke2tohub = @{
    Name = $spoke2hubpeeringName
    VirtualNetwork = $spoke2vnetinfo 
    RemoteVirtualNetworkId = $hubvnetinfo.Id
}
Add-AzVirtualNetworkPeering @spoke2tohub

####################################################################
# Oppretter Network Security Group (NSG) som kan knyttes til subnet
####################################################################
# MERK: NSG-er skal ikke benyttes på GatewaySubnet og AzureFirewallSubnet,  
# denne muligheten er deaktivert for å unngå tjenesteavbrudd. 

$mgmtnsginfo = New-AzNetworkSecurityGroup -Name $mgmtnsg -ResourceGroupName $rgName -Location $LocationName
$vm1subnetnsginfo = New-AzNetworkSecurityGroup -Name $vm1subnetnsg -ResourceGroupName $rgName -Location $LocationName
$vm2subnetnsginfo = New-AzNetworkSecurityGroup -Name $vm2subnetnsg -ResourceGroupName $rgName -Location $LocationName

# Legger NSG til subnet. NSG-er kan legges til på subnet eller nettverksgrensesnitt.
# Ved å legge til på subnet vil NSG-reglene være gjeldende for alle på dette subnettet
# Legger vNet i korrekt variabel
$hubvnetinfo = Get-AzVirtualNetwork -Name $hubvnetName
$spoke1vnetinfo = Get-AzVirtualNetwork -Name $spoke1vnetName
$spoke2vnetinfo = Get-AzVirtualNetwork -Name $spoke2vnetName

# Hekter på opprettet NSG på subnet
Set-AzVirtualNetworkSubnetConfig -Name $hubmgmtsubnetName `
                                -VirtualNetwork $hubvnetinfo `
                                -AddressPrefix $hubmgmtsubnetaddressprefix `
                                -NetworkSecurityGroup $mgmtnsginfo
$hubvnetinfo | Set-AzVirtualNetwork
Set-AzVirtualNetworkSubnetConfig -Name $spoke1vnetsubnetName `
                                -VirtualNetwork $spoke1vnetinfo `
                                -AddressPrefix $spoke1vnetsubnetaddressprefix `
                                -NetworkSecurityGroup $vm1subnetnsginfo
$spoke1vnetinfo | Set-AzVirtualNetwork
Set-AzVirtualNetworkSubnetConfig -Name $spoke2vnetsubnetName `
                                -VirtualNetwork $spoke2vnetinfo `
                                -AddressPrefix $spoke2vnetsubnetaddressprefix `
                                -NetworkSecurityGroup $vm2subnetnsginfo
$spoke2vnetinfo | Set-AzVirtualNetwork

################################################
# Oppretter VPN Gateway og Azure Firewall i hub
################################################

$hubvnetinfo = Get-AzVirtualNetwork -Name $hubvnetName # legger hub vNet i variablen $hubvnetinfo

# VPN GATEWAY

# VPN Gateway trenger en public IP
$gwpublicip = @{
    Name = $AzVPNgwPIpName
    ResourceGroupName = $rgName
    AllocationMethod ="Dynamic"
    Location = $LocationName
}
$gwpip = New-AzPublicIpAddress @gwpublicip

$gwSubnet = Get-AzVirtualNetworkSubnetConfig -Name $hubvpnsubnetName -VirtualNetwork $hubvnetinfo
$gwipconf = New-AzVirtualNetworkGatewayIpConfig -Name $AzVPNgwConfig -Subnet $gwSubnet -PublicIpAddress $gwpip

# Oppretter VPN Gateway
New-AzVirtualNetworkGateway -Name $AzVPNgwName -ResourceGroupName $rgName `
                            -Location $LocationName -IpConfigurations $gwipconf -GatewayType Vpn `
                            -VpnType RouteBased -GatewaySku Basic

# Oppretter sertifikat for sikker tilkobling
$certStoreLocation = "Cert:\CurrentUser\My"
Get-ChildItem -Path "Cert:\CurrentUser\My"

# Oppretter self-signed root certificate
$cert = New-SelfSignedCertificate -Type Custom -KeySpec Signature `
                                    -Subject "CN=P2SRootCert" -KeyExportPolicy Exportable `
                                    -HashAlgorithm sha256 -KeyLength 2048 `
                                    -CertStoreLocation $certStoreLocation -KeyUsageProperty Sign -KeyUsage CertSign

New-SelfSignedCertificate -Type Custom -DnsName "P2SRootClient" -KeySpec Signature `
                            -Subject "CN=P2SRootClient" -KeyExportPolicy Exportable `
                            -HashAlgorithm sha256 -KeyLength 2048 `
                            -CertStoreLocation $certStoreLocation `
                            -Signer $cert -TextExtension @("2.5.29.37={text}1.3.6.1.5.5.7.3.2")

$azVPNgwinfo = Get-AzVirtualNetworkGateway -Name $AzVPNgwName -ResourceGroupName $rgName
$rootCaBase64 = [system.convert]::ToBase64String($cert.RawData)

# Disabling BGP
# VPN: You can, optionally use BGP. For details, see BGP with site-to-site VPN connections.
# https://docs.microsoft.com/en-us/azure/vpn-gateway/vpn-gateway-bgp-overview?toc=/azure/virtual-network/toc.json
$azVPNgwinfo.BgpSettings =$null

# Setting the VPN Client Address Pool
Set-AzVirtualNetworkGateway -VirtualNetworkGateway $azVPNgwinfo -VpnClientAddressPool $azVPNgwClientAddressPool

# Uploading the Certificate to the gateway
Add-AzVpnClientRootCertificate -VpnClientRootCertificateName "P2SRootCA" `
                                -VirtualNetworkGatewayname $AzVPNgwName `
                                -ResourceGroupName $rgName `
                                -PublicCertData $rootCaBase64

Get-ChildItem -Path "Cert:\CurrentUser\My"

# MÅ VURDERES - Skal maskin(er) tilkoblet med VPN få kommunisere med spokes? og i såfall med hvilke?
# I dette eksemplet konfigureres KUN spoke 1 til å tillate Gateway Transit (kan gjøres for så mange og hvilke en vil)
# Dvs. når en er tilkoblet med VPN vil "on-prem" maskinen kunne kommunisere med Spoke1
# MERK at en må laste ned Client på nytt etter denne konfigurasjonen.

#https://docs.microsoft.com/en-us/azure/vpn-gateway/vpn-gateway-peering-gateway-transit
$LinkHubToSpoke1 = Get-AzVirtualNetworkPeering `
                      -VirtualNetworkName $hubvnetName `
                      -ResourceGroupName $rgName `
                      -Name $hubspoke1peeringName

$LinkHubToSpoke1.AllowGatewayTransit = $True
$LinkHubToSpoke1.AllowForwardedTraffic = $True
Set-AzVirtualNetworkPeering -VirtualNetworkPeering $LinkHubToSpoke1

# MÅ vurderes - Skal de benytte ekstern gateway?
# Konfigurer peering-tilkoblingen i spoke1 til å bruke eksterne gateways.

$LinkSpoke1ToHub = Get-AzVirtualNetworkPeering `
                      -VirtualNetworkName $spoke1vnetName `
                      -ResourceGroupName $rgName `
                      -Name $spoke1hubpeeringName

$LinkSpoke1ToHub.UseRemoteGateways = $True
$LinkSpoke1ToHub.AllowForwardedTraffic = $True

Set-AzVirtualNetworkPeering -VirtualNetworkPeering $LinkSpoke1ToHub

# AZURE FIREWALL 
$hubvnetinfo = Get-AzVirtualNetwork -Name $hubvnetName # legger hub vNet i variablen $hubvnetinfo
# Azure Firewall trenger en Public IP
$firewallPip = New-AzPublicIpAddress `
                  -ResourceGroupName $rgName `
                  -Location $LocationName `
                  -AllocationMethod Static `
                  -Sku Standard `
                  -Name $AzFirewallpipName

# Oppretter Firewall
$azFirewall = New-AzFirewall `
                 -Name $azFirewallName `
                 -ResourceGroupName $rgName  `
                 -Location $LocationName `
                 -VirtualNetworkName $hubvnetinfo.Name `
                 -PublicIpName $firewallPip.Name

# Vi må deretter opprette en default route for hvert subnet som skal gå via AzFirewall
# Route table
$AzFirewallRouteTable = New-AzRouteTable `
                -Name $AzFirewallRouteTableName `
                -ResourceGroupName $rgName `
                -location $LocationName

# Oppretter ny route - Default route
Get-AzRouteTable -ResourceGroupName $rgName `
                -Name $AzFirewallRouteTableName `
                | Add-AzRouteConfig `
                -Name $azFirewallDefaultRoute `
                -AddressPrefix 0.0.0.0/0 `
                -NextHopType "VirtualAppliance" `
                -NextHopIpAddress $AzFirewall.IpConfigurations.privateipaddress `
                | Set-AzRouteTable

# Hekter på default route på vm subnet i spoke1
$spoke1vnetinfo = Get-AzVirtualNetwork -Name $spoke1vnetName

Set-AzVirtualNetworkSubnetConfig `
                -VirtualNetwork $spoke1vnetinfo `
                -Name $spoke1vnetsubnetName `
                -AddressPrefix $spoke1vnetsubnetaddressprefix `
                -RouteTable $AzFirewallRouteTable | `
                Set-AzVirtualNetwork

# MERK at det kan være ønskelig å konfigurere det samme for spoke2

##########################################################
#                                                        #
# I denne delen av sciptet blir det opprettet et         #
#                  Key Vault med:                        #
# - Tilgang for pålogget bruker til å uføre:             #
#   - tilgang til Key Vault: set, get, list              #
# - Secret for bruk som credentials for VMer             #
#                                                        #
##########################################################

#########################################################
#              Oppretter ressurser i Azure              #
#########################################################

# Create a new Azure Key Vault
New-AzKeyVault `
    -VaultName $kvName `
    -ResourceGroupName $rgName `
    -Location $LocationName `
    -EnabledForDeployment `
    -EnabledForTemplateDeployment `
    -EnabledForDiskEncryption `
    -Sku standard

# Gi deretter tilgangsrettigheter til Azure Key Vault for den påloggede brukerkontoen
Get-AzAdUser | Format-Table # hvis en ønsker å liste ut brukere
Set-AzKeyVaultAccessPolicy `
    -VaultName $kvName `
    -ResourceGroupName $rgName `
    -UserPrincipalName "melling_ntnu.no#EXT#@digsec.onmicrosoft.com" `
    -PermissionsToSecrets get, set, list

# Opprett en secret med passord for VM-er
$password = read-host -assecurestring
Set-AzKeyVaultSecret `
    -VaultName $kvName `
    -Name $secretName `
    -SecretValue $password

# Etter at en har opprettet en secret, kan en hente ut secreten, opprette et nytt PSCredential-objekt
# som kan brukes i f.eks. VM-utrulling.

$secret = Get-AzKeyVaultSecret `
    -VaultName $kvName `
    -Name $secretName
# Create a new PSCredential object
$cred = [PSCredential]::new($localVMadmin,$secret.
SecretValue)

##########################################################
#                                                        #
# I delen av sciptet blir det VM-er i forskjellige vNet  #
# - VM opprettes i vNet $hubvnetName                     #
#   - subnet: $hubmgmtsubnetName                         #
# - VM opprettes i vNet $spoke1vnetName                  #
#   - subnet: $spoke1vmsubnetname                        #
# - VM opprettes i vNet $spoke2vnetName                  #
#   - subnet: $spoke2vmsubnetname                        #
# - KeyVault Secret brukes som passord for VM-er         #
#   - Tidligere opprettet sammen med KeyVault            #
#                                                        #
##########################################################
#       Variabler opprettet for denne delen              #
##########################################################

# Virtual Machine (VM)
$hubspokeVMs = @(
  @{
    VmName = $vmhubmgmg
    nicName = "$vmhubmgmg-nic"
    ip = "10.10.1.4"
    vmsize = "Standard_D2s_v3"
    vNet = $hubvnetName
    subnet = $hubmgmtsubnetName
  },
  @{
    VmName = $vmspoke1vm
    nicName = "$vmspoke1vm-nic"
    ip = "10.20.1.4"
    vmsize = "Standard_D2s_v3"
    vNet = $spoke1vnetName
    subnet = $spoke1vnetsubnetName
  },
  @{
    VmName = $vmspoke2vm
    nicName = "$vmspoke2vm-nic"
    ip = "10.30.1.4"
    vmsize = "Standard_D2s_v3"
    vNet = $spoke2vnetName
    subnet = $spoke2vnetsubnetName
  }
)

#########################################################
#              Oppretter ressurser i Azure              #
#########################################################
# Oppretter VM-er - info som kan benyttes til $vm er .VMname .nicName .ip .vmsize .vNet .subnet
# $vm.VMname, $vm.nicName etc..
# Merk at om vNetene har flere subnet må en finne riktig subnet og ID. I mitt eksempel er det kun et. 
foreach($vm in $hubspokeVMs){
 
    $vnetinfo = Get-AzVirtualNetwork -name $vm.vNet
    $subnetinfo = Get-AzVirtualNetworkSubnetConfig -Name $vm.subnet -VirtualNetwork $vnetinfo
    $IPconfig = New-AzNetworkInterfaceIpConfig -Name "IPConfig1" -PrivateIpAddressVersion IPv4 -PrivateIpAddress $vm.ip -SubnetId $subnetinfo.id
    $NIC = New-AzNetworkInterface -Name $vm.nicName -ResourceGroupName $rgName -Location $LocationName -IpConfiguration $IPconfig
     
    $secret = Get-AzKeyVaultSecret `
    -VaultName $kvName `
    -Name $secretName
    # Create a new PSCredential object
    $cred = [PSCredential]::new($localVMadmin,$secret.
    SecretValue)
     
    $VirtualMachine = New-AzVMConfig -VMName $vm.VmName -VMSize $vm.vmsize
    $VirtualMachine = Set-AzVMOperatingSystem -VM $VirtualMachine -Windows -ComputerName $vm.VmName -Credential $cred -ProvisionVMAgent -EnableAutoUpdate
    $VirtualMachine = Add-AzVMNetworkInterface -VM $VirtualMachine -Id $NIC.Id
    $VirtualMachine = Set-AzVMSourceImage -VM $VirtualMachine -PublisherName 'MicrosoftWindowsServer' -Offer 'WindowsServer' -Skus '2019-Datacenter' -Version latest
     
    New-AzVM -ResourceGroupName $rgName -Location $LocationName -VM $VirtualMachine -Verbose
}

##########################################################
#                                                        #
# I denne delen blir jobbet videre med hub-spoke vNET    #
# - Oppretter flere subnet i spoke1                      #
#   - Oppretter Azure Security Groups (ASG)              #
#   - ASG tillates å kommunisere inn til subnet          #
#   - Ressurser legges i ASG for å nå subnet             #
# - Muliggjør spoke to spoke via Azure Firewall          #
#   - muliggjør kommunikasjon mellom spoke 1 og spoke 2  #
#                                                        #
##########################################################
#              Variabler brukt i scriptet                #
##########################################################

# Ny vm i spoke 1 business og data subnet (OBS: VM name kan ikke være over 15 tegn)
$hubspokeVMs = @(
  @{
    VmName = $businessspoke1vm
    nicName = "$businessspoke1vm-nic"
    ip = "10.20.2.4"
    vmsize = "Standard_D2s_v3"
    vNet = $spoke1vnetName
    subnet = $spoke1vnetbusinessSubnetName
  },
  @{
    VmName = $dataspoke1vm
    nicName = "$dataspoke1vm-nic"
    ip = "10.20.3.4"
    vmsize = "Standard_D2s_v3"
    vNet = $spoke1vnetName
    subnet = $spoke1vnetdataSubnetName
  }
)

#########################################################
#              Oppretter ressurser i Azure              #
#########################################################

# Oppretter business subnet i Spoke1
$spoke1vnetinfo = Get-AzVirtualNetwork -Name $spoke1vnetName
$vm1sn = @{
    Name = $spoke1vnetbusinessSubnetName
    AddressPrefix = $spoke1vnetbusinesssubnetaddressprefix
    VirtualNetwork = $spoke1vnetinfo
}
$vm1subnetinfo = Add-AzVirtualNetworkSubnetConfig @vm1sn

# Legger til nylig opprettet subnet i spoke1-vnet
$spoke1vnetinfo = $spoke1vnetinfo | Set-AzVirtualNetwork

# Oppretter NSG for business subnet og hekter på korrekt subnet
$businesssubnetnsginfo = New-AzNetworkSecurityGroup -Name $businesssubnetnsg -ResourceGroupName $rgName -Location $LocationName
$spoke1vnetinfo = Get-AzVirtualNetwork -Name $spoke1vnetName
Set-AzVirtualNetworkSubnetConfig -Name $spoke1vnetbusinesssubnetName -VirtualNetwork $spoke1vnetinfo -AddressPrefix $spoke1vnetbusinesssubnetaddressprefix -NetworkSecurityGroup $businesssubnetnsginfo
$spoke1vnetinfo | Set-AzVirtualNetwork

# Oppretter data subnet i Spoke1
$spoke1vnetinfo = Get-AzVirtualNetwork -Name $spoke1vnetName
$vm1sn = @{
    Name = $spoke1vnetdataSubnetName
    AddressPrefix = $spoke1vnetdatasubnetaddressprefix
    VirtualNetwork = $spoke1vnetinfo
}
$vm1subnetinfo = Add-AzVirtualNetworkSubnetConfig @vm1sn

# Legger til nylig opprettet subnet i spoke1-vnet
$spoke1vnetinfo = $spoke1vnetinfo | Set-AzVirtualNetwork

# Oppretter NSG for Data subnet og hekter på korrekt subnet
$datasubnetnsginfo = New-AzNetworkSecurityGroup -Name $datasubnetnsg -ResourceGroupName $rgName -Location $LocationName
$spoke1vnetinfo = Get-AzVirtualNetwork -Name $spoke1vnetName
Set-AzVirtualNetworkSubnetConfig -Name $spoke1vnetdatasubnetName -VirtualNetwork $spoke1vnetinfo -AddressPrefix $spoke1vnetdatasubnetaddressprefix -NetworkSecurityGroup $datasubnetnsginfo
$spoke1vnetinfo | Set-AzVirtualNetwork

###########################################
# Oppretter VM for business og data subnet
###########################################
foreach($vm in $hubspokeVMs){
 
    $vnetinfo = Get-AzVirtualNetwork -name $vm.vNet
    $subnetinfo = Get-AzVirtualNetworkSubnetConfig -Name $vm.subnet -VirtualNetwork $vnetinfo
    $IPconfig = New-AzNetworkInterfaceIpConfig -Name "IPConfig1" -PrivateIpAddressVersion IPv4 -PrivateIpAddress $vm.ip -SubnetId $subnetinfo.id
    $NIC = New-AzNetworkInterface -Name $vm.nicName -ResourceGroupName $rgName -Location $LocationName -IpConfiguration $IPconfig
     
    $secret = Get-AzKeyVaultSecret `
    -VaultName $kvName `
    -Name $secretName
    # Create a new PSCredential object
    $cred = [PSCredential]::new($localVMadmin,$secret.
    SecretValue)
     
    $VirtualMachine = New-AzVMConfig -VMName $vm.VmName -VMSize $vm.vmsize
    $VirtualMachine = Set-AzVMOperatingSystem -VM $VirtualMachine -Windows -ComputerName $vm.VmName -Credential $cred -ProvisionVMAgent -EnableAutoUpdate
    $VirtualMachine = Add-AzVMNetworkInterface -VM $VirtualMachine -Id $NIC.Id
    $VirtualMachine = Set-AzVMSourceImage -VM $VirtualMachine -PublisherName 'MicrosoftWindowsServer' -Offer 'WindowsServer' -Skus '2019-Datacenter' -Version latest
     
    New-AzVM -ResourceGroupName $rgName -Location $LocationName -VM $VirtualMachine -Verbose
}

############################
# Test trafikk mellom VM-er
############################

# Oppretter regel som låser ned data subnet (inbound og outbound)
# InBound
Get-AzNetworkSecurityGroup `
                    -Name $datasubnetnsg `
                    -ResourceGroupName $rgName |
                    Add-AzNetworkSecurityRuleConfig `
                        -Name "DenyAllInBound-test" `
                        -Description "Deny All In-Test" `
                        -Access "Deny" `
                        -Protocol "*" `
                        -Direction "Inbound" `
                        -Priority 4000 `
                        -SourceAddressPrefix "*" `
                        -SourcePortRange "*" `
                        -DestinationAddressPrefix "*" `
                        -DestinationPortRange "*" |
                            Set-AzNetworkSecurityGroup

# OutBound
Get-AzNetworkSecurityGroup `
                    -Name $datasubnetnsg `
                    -ResourceGroupName $rgName |
                    Add-AzNetworkSecurityRuleConfig `
                        -Name "DenyAllInOut-test" `
                        -Description "Deny All Out-Test" `
                        -Access "Deny" `
                        -Protocol "*" `
                        -Direction "Outbound" `
                        -Priority 4000 `
                        -SourceAddressPrefix "*" `
                        -SourcePortRange "*" `
                        -DestinationAddressPrefix "*" `
                        -DestinationPortRange "*" |
                            Set-AzNetworkSecurityGroup

# Oppretter Application Security Group
$sourcespoke1vmAsg = New-AzApplicationSecurityGroup -ResourceGroupName $rgName -Name $businessspoke1subnetASG -Location $LocationName
$destspoke1vmAsg = New-AzApplicationSecurityGroup -ResourceGroupName $rgName -Name $dataspoke1subnetASG -Location $LocationName

Get-AzNetworkSecurityGroup -Name $datasubnetnsg -ResourceGroupName $rgName |
Add-AzNetworkSecurityRuleConfig `
        -Name rdp-rule-ASG `
        -Description "Allow RDP" `
        -Access Allow `
        -Protocol Tcp `
        -Direction Inbound `
        -Priority 100 `
        -SourceApplicationSecurityGroup $sourcespoke1vmAsg `
        -SourcePortRange * `
        -DestinationApplicationSecurityGroup $destspoke1vmAsg `
        -DestinationPortRange 3389 |
Set-AzNetworkSecurityGroup

# Legg til VM fra business subnet i business ASG
$Vm = Get-AzVM -Name $businessspoke1vm
$nic = Get-AzNetworkInterface -ResourceId $Vm.NetworkProfile.NetworkInterfaces.id

$asg = Get-AzApplicationSecurityGroup -Name $businessspoke1subnetASG

# MERK - kan gjøres sånn siden VM-en har kun et nic og en adresse
$nic.IpConfigurations[0].ApplicationSecurityGroups = $asg
$nic | Set-AzNetworkInterface

# Legg til VM fra data subnet i data ASG
$Vm = Get-AzVM -Name $dataspoke1vm
$nic = Get-AzNetworkInterface -ResourceId $Vm.NetworkProfile.NetworkInterfaces.id

$asg = Get-AzApplicationSecurityGroup -Name $dataspoke1subnetASG

# MERK - kan gjøres sånn siden VM-en har kun et nic og en adresse
$nic.IpConfigurations[0].ApplicationSecurityGroups = $asg
$nic | Set-AzNetworkInterface

###############################################################
# Muliggjør spoke1 til spoke2 kommunikasjon via Azure Firewall
###############################################################

# Hekter på default route til Azure Firewall for spoke2-vm-subnet
$spoke2vnetinfo = Get-AzVirtualNetwork -Name $spoke2vnetName
$AzFirewallRouteTable = Get-AzRouteTable -Name $AzFirewallRouteTableName -ResourceGroupName $rgName

Set-AzVirtualNetworkSubnetConfig `
                -VirtualNetwork $spoke2vnetinfo `
                -Name $spoke2vnetsubnetName `
                -AddressPrefix $spoke2vnetsubnetaddressprefix `
                -RouteTable $AzFirewallRouteTable | `
                Set-AzVirtualNetwork


####################################################################
# Åpner for ekstern tilkobling gjennom Azure Firewall til spoke1 vm
####################################################################

$AzFirewallPublicIP = Get-AzPublicIpAddress -name $AzFirewallpipName
$Vm = Get-AzVM -Name $vmspoke1vm
$nic = Get-AzNetworkInterface -ResourceId $Vm.NetworkProfile.NetworkInterfaces.id
$nic.IpConfigurations.PrivateIpAddress
 
$natrulerdp = New-AzFirewallNatRule `
            -Name $AzFirewallNatRuleName `
            -Protocol "TCP" `
            -SourceAddress "*" `
            -DestinationAddress $AzFirewallPublicIP.IpAddress `
            -DestinationPort "33891" `
            -TranslatedAddress $nic.IpConfigurations.PrivateIpAddress `
            -TranslatedPort "3389"
                
$rdpcollectionrule = New-AzFirewallNatRuleCollection `
                -Name $AzFirewallNatRuleCollectionName `
                -Priority 100 `
                -Rule $natrulerdp
 
$AzFirewall.NatRuleCollections = $rdpcollectionrule
$AzFirewall | Set-AzFirewall

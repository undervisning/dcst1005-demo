Install-Module -Name Az -Scope CurrentUser -Repository PSGallery -Force

Connect-AzAccount
Get-AzTenant
$tenants = Get-AzTenant | Where-object { $_.name -like "UNDERVISNING DIGSEC" }
$tenants.id
$tenants.Name
$tenants.domains

Set-AzContext -Tenant $teanants.id
Get-AzContext

#Variabler
$id = 'tim'
$rg = 'rg_azureintro'
$location = 'westeurope' # Get-AzLocation | ft
$vmname = 'win19'
$pubIp = 'win19-ip'

#Create Resource Group
New-AzResourceGroup -Name "$rg-$id" -Location $location

$newrg = @{
    Name = "$rg-$id"
    Location = $location
}
New-AzResourceGroup @newrg

Get-AzResourceGroup | ft

#Create Virtual Network
$vnet = @{
    Name = "vNET-$id"
    ResourceGroupName = "$rg-$id"
    Location = $location
    AddressPrefix = '10.0.0.0/16'    
}
$virtualNetwork = New-AzVirtualNetwork @vnet

Get-AzVirtualNetwork | Select-Object name,resourcegroupname

#Create Subnet
$subnet = @{
    Name = 'default'
    VirtualNetwork = $virtualNetwork
    AddressPrefix = '10.0.0.0/24'
}
$subnetConfig = Add-AzVirtualNetworkSubnetConfig @subnet
$virtualNetwork | Set-AzVirtualNetwork
$virtualNetwork | Get-AzVirtualNetworkSubnetConfig

#Credentials for VM
$cred = Get-Credential -Message "Enter a username and password for the virtual machine."

# Create VM
$vmParams = @{
    ResourceGroupName = "$rg-$id"
    Name = "$vmname-$id"
    Location = $location
    ImageName = 'Win2019Datacenter'
    VirtualNetworkName = "vNET-$id"
    SubnetName = "default"
    PublicIpAddressName = "$pubIp-$id"
    Credential = $cred
    OpenPorts = 3389
  }
$newVM1 = New-AzVM @vmParams
$newVM1

#List useful VM information
$newVM1.OSProfile | Select-Object ComputerName,AdminUserName    

$newVM1 | Get-AzNetworkInterface |
  Select-Object -ExpandProperty IpConfigurations |
    Select-Object Name,PrivateIpAddress

#Get public IP
$publicIp = Get-AzPublicIpAddress -Name "$pubIp-$id" -ResourceGroupName "$rg-$id"
$publicIp | Select-Object Name,IpAddress,@{label='FQDN';expression={$_.DnsSettings.Fqdn}}

mstsc.exe /v 40.114.150.204
#To be run in VM - Check if VM1 and 2 can communicate
New-NetFirewallRule –DisplayName "Allow ICMPv4-In" –Protocol ICMPv4 


#Create VM 2
$vm2Params = @{
    ResourceGroupName = "$rg-$id"
    Name = "$vmname-2-$id"
    ImageName = 'Win2019Datacenter'
    VirtualNetworkName = "vNET-$id"
    SubnetName = "default"
    PublicIpAddressName = 'win19-02-pubip'
    Credential = $cred
    OpenPorts = 3389
  }
$newVM2 = New-AzVM @vm2Params
$newVM2



#Remove Resource Group and all resources (in correct order)
$job = Remove-AzResourceGroup -Name "$rg-$id" -Force -AsJob
$job
Wait-Job -Id $job.Id



# Virual machine Scale Set
Invoke-WebRequest -Uri "https://gitlab.com/undervisning/dcst1005-demo/-/raw/master/Uke_15-web-test.txt?inline=false" -OutFile "web-test.txt"
az group create --name TIMWebSite --location "West Europe"

az vmss create --name timweb --resource-group TIMWebSite --image UbuntuLTS --location "West Europe" --generate-ssh-keys --lb weblb --custom-data web-test.txt
az network lb rule create --resource-group TIMWebSite --lb-name weblb --name lb-rule --backend-pool-name weblbbePool --backend-port 80 --frontend-ip-name loadbalancerfrontend --frontend-port 80 --protocol tcp

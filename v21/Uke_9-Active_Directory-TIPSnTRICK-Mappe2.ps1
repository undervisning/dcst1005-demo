#############################
######## IIS webpage ########
#############################

# Prerequisites
# - Choco is installed
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
# - 7z is installed
choco install 7zip.install
# Content for your web page - go go https://free-css.com for other alternatives
$source = 'https://www.free-css.com/assets/files/free-css-templates/download/page264/expertum.zip'
# Destination to save the file
$destination = "C:\Users\$env:username.$env:UserDomain\Downloads\expertum.zip"
# Download the file
Invoke-WebRequest -Uri $source -OutFile $destination
7z x $destination -ofolder
dir \\undervisning.sec\files
Copy-Item -Path "C:\Users\$env:username.$env:UserDomain\Downloads\folder" -Destination \\undervisning.sec\files\production\webpage -recurse -Force


# Install IIS - srv1 #
Enter-PSSession -ComputerName tim-srv1 
Install-WindowsFeature -name Web-Server -IncludeManagementTools
# Copy files to correct folder 
Copy-Item -Path \\undervisning.sec\files\production\webpage\* -Destination "C:\inetpub\wwwroot" -recurse -Force


#############################
#### Create OU-struktur #####
#############################

Enter-PSSession -ComputerName tim-dc1
New-ADOrganizationalUnit "opit-users" -Description "Containing OUs and users"
New-ADOrganizationalUnit "hr" -Description "HR OU" -Path "OU=opit-users,DC=undervisning,DC=sec"
New-ADOrganizationalUnit "it-admins" -Description "IT admins OU" -Path "OU=opit-users,DC=undervisning,DC=sec"
New-ADOrganizationalUnit "developers" -Description "Developer Team OU" -Path "OU=opit-users,DC=undervisning,DC=sec"
New-ADOrganizationalUnit "Regnskap" -Description "Regnskap OU" -Path "OU=opit-users,DC=undervisning,DC=sec"
New-ADOrganizationalUnit "Renhold" -Description "Renhold OU" -Path "OU=opit-users,DC=undervisning,DC=sec"

New-ADOrganizationalUnit "opit-computers" -Description "Containing company servers and workstations"
New-ADOrganizationalUnit "opit-servers" -Description "Containing company servers" -Path "OU=opit-computers,DC=undervisning,DC=sec"
New-ADOrganizationalUnit "opit-workstations" -Description "Containing company servers" -Path "OU=opit-computers,DC=undervisning,DC=sec"

Get-ADComputer "tim-srv1" | Move-ADObject -TargetPath "OU=opit-servers,OU=opit-computers,DC=undervisning,DC=sec" -Verbose
Get-ADComputer "tim-cli1" | Move-ADObject -TargetPath "OU=opit-workstations,OU=opit-computers,DC=undervisning,DC=sec" -Verbose

New-ADOrganizationalUnit "resources" -Description "Containing company resources" -Path "DC=undervisning,DC=sec"

#############################
####### Create groups #######
#############################

New-ADGroup -GroupCategory Security -GroupScope Global -Name "g_it_admins" -Path "OU=it-admins,OU=opit-users,DC=undervisning,DC=sec" -SamAccountName "g_it_admins"
New-ADGroup -GroupCategory Security -GroupScope Global -Name "g_hr" -Path "OU=hr,OU=opit-users,DC=undervisning,DC=sec" -SamAccountName "g_hr"
New-ADGroup -GroupCategory Security -GroupScope Global -Name "g_regnskap" -Path "OU=regnskap,OU=opit-users,DC=undervisning,DC=sec" -SamAccountName "g_regnskap"
New-ADGroup -GroupCategory Security -GroupScope Global -Name "g_renhold" -Path "OU=renhold,OU=opit-users,DC=undervisning,DC=sec" -SamAccountName "g_renhold"
New-ADGroup -GroupCategory Security -GroupScope Global -Name "g_developers" -Path "OU=developers,OU=opit-users,DC=undervisning,DC=sec" -SamAccountName "g_developers"

Get-ADGroup -filter "Name -like 'g_*'" | Select-Object name, DistinguishedName

#############################
### Create Resource Groups ##
#############################

$ressurser=@('printere','adgangskort')
$ressurser | foreach { New-ADGroup -name $_ -Path "OU=resources,DC=undervisning,DC=sec" -GroupCategory Security -GroupScope DomainLocal }


#############################
####### Create Users ########
#############################
Copy-Item -Path "C:\vscode-projects\dcst1005-demo\csv-files\my-users.csv" -Destination \\undervisning.sec\files\installerfiles -recurse -Force

$ADUsers = Import-csv \\undervisning.sec\files\installerfiles\my-users.csv -Delimiter ";" 
# Headers: Username;GivenName;SurName;UserPrincipalName;DisplayName;Password;Department;Path
foreach ($User in $ADUsers)
{   
    $name = $User.GivenName + " " + $User.Surname
    New-ADUser `
    -SamAccountName $User.Username `
    -UserPrincipalName $User.UserPrincipalName `
    -Name $name `
    -GivenName $user.GivenName `
    -Surname $user.SurName `
    -Enabled $True `
    -ChangePasswordAtLogon $false `
    -DisplayName $user.Displayname `
    -Department $user.Department `
    -Path $user.path `
    -AccountPassword (convertto-securestring $user.Password -AsPlainText -Force)

}

#TIPS OG TRIKS#
# Hva med æ ø og å?? Disse bokstavene godtas ikke SamAccountName/UserPrincipalName
$testnavn = "Øverli"
$testnavn = $testnavn.ToLower()
$testnavn = $testnavn.Replace('ø','o')
$testnavn


#############################
#### Add users to group #####
#############################

$UserList = Get-ADUser -Filter * -Properties department | Where-Object {$_.department -Like "IT-Admins"} | Select sAMAccountName
Add-ADGroupMember -Identity 'g_it_admins' -Members $UserList

Get-ADGroupMember -Identity 'g_it_admins' | Select-Object name

#############################
###### Share with ACL #######
#############################
[System.Enum]::GetNames([System.Security.AccessControl.FileSystemRights])

dir \\undervisning.sec\files
Enter-PSSession -ComputerName tim-srv1

$folders = ('C:\shares\it-admins')
mkdir -path $folders
$folders | ForEach-Object {$sharename = (Get-Item $_).name; New-SMBShare -Name $shareName -Path $_ -FullAccess Everyone}
$folders | Where-Object {$_ -like "*shares*"} | ForEach-Object {$name = (Get-Item $_).name; $DfsPath = (‘\\undervisning.sec\files\’ + $name); $targetPath = (‘\\tim-srv1\’ + $name);New-DfsnFolderTarget -Path $dfsPath -TargetPath $targetPath}
dir \\undervisning.sec\files

$folders = "\\undervisning.sec\files\it-admins"
Get-SmbShareAccess -name 'it-admins'
Get-Acl -Path $folders
(Get-Acl -Path $folders).Access
(Get-Acl -Path $folders).Access | Format-Table -AutoSize
(Get-Acl -Path $folders).Access | Where-Object {$_.IsInherited -eq $true} | Format-Table -AutoSize
(Get-ACL -Path $folders).Access | Format-Table IdentityReference,FileSystemRights,AccessControlType,IsInherited,InheritanceFlags -AutoSize

$folders = ('C:\shares\it-admins')
$acl = Get-Acl \\undervisning\files\it-admins
$AccessRule = New-Object System.Security.AccessControl.FileSystemAccessRule("undervisning\g_it_admins","FullControl","Allow")
$acl.SetAccessRule($AccessRule)
$ACL | Set-Acl -Path "\\undervisning\files\it-admins"

# To modify the inheritance properties of an object, we have to use the SetAccessRuleProtection method with the constructor: isProtected, preserveInheritance. 
# The first isProtected property defines whether or not the folder inherits its access permissions or not. Setting this value to $true will disable inheritance
$ACL = Get-Acl -Path "\\undervisning\files\it-admins"
$ACL.SetAccessRuleProtection($true,$true)
$ACL | Set-Acl -Path "\\undervisning\files\it-admins"

$acl = Get-Acl "\\undervisning\files\it-admins"
$acl.Access | where {$_.IdentityReference -eq "BUILTIN\Users" } | foreach { $acl.RemoveAccessRuleSpecific($_) }
Set-Acl "\\undervisning\files\it-admins" $acl
(Get-ACL -Path "\\undervisning\files\it-admins").Access | Format-Table IdentityReference,FileSystemRights,AccessControlType,IsInherited,InheritanceFlags -AutoSize












#############################
######## Create GPO #########
#############################
https://aka.ms/EdgeEnterprise # <- last ned 
Copy-Item -Path "C:\Users\$env:username.$env:UserDomain\Downloads\MicrosoftEdgePolicyTemplates\windows\admx\en-US\msedge.adml" -Destination \\undervisning.sec\files\production\ -recurse -Force

Enter-PSSession -ComputerName dc1
Copy-Item -Path \\undervisning.sec\files\production\msedge.adml -Destination "C:\" -recurse -Force

Get-Command -Module GroupPolicy

Get-GPO -All | select-object DisplayName
New-GPO -Name "EdgeStartPage"
New-GPLink -Name 'EdgeStartPage' -Target 'OU=opit-workstations,OU=opit-computers,DC=undervisning,DC=sec'

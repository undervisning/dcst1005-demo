# MERK: Som standard blir DDoS-beskyttelsesplan Basic er valgt.
# Det anbefales på det sterkeste å aktivere DDoS-beskyttelsesplan for produksjonsmiljø, 
# og dette gjelder alle virtuelle nettverk.

############################
# AZURE HUB SPOKE - Del 1  #
############################

# HUB vNET

$LocationName = "west europe"
$ResourceGroupName = "hub-spok-tim"
$vnetName = "hub-vnet"
$defaultSubnet = "ManagementSubnet"
$vnetAddressSpace = "10.10.0.0/19"
$subnetvnetAddressSpace = "10.10.1.0/24"

$newrg = @{
    Name = $ResourceGroupName
    Location = $LocationName
}
New-AzResourceGroup @newrg

$vnet = @{
    ResourceGroupName = $ResourceGroupName
    Location = $LocationName
    Name = $vnetName
    AddressPrefix = $vnetAddressSpace
}
$virtualnetwork = New-AzVirtualNetwork @vnet

$subnet = @{
    Name = $defaultSubnet
    VirtualNetwork = $virtualNetwork
    AddressPrefix = $subnetvnetAddressSpace
}
$subnetConfig = Add-AzVirtualNetworkSubnetConfig @subnet

$virtualNetwork = $virtualNetwork | Set-AzVirtualNetwork

# SPOKE 1 vNET

$vnetName = "spoke1-vnet"
$defaultSubnet = "WorkloadSubnet"
$vnetAddressSpace = "10.20.0.0/19"
$subnetvnetAddressSpace = "10.20.1.0/24"

$vnet = @{
    ResourceGroupName = $ResourceGroupName
    Location = $LocationName
    Name = $vnetName
    AddressPrefix = $vnetAddressSpace
}
$virtualnetwork = New-AzVirtualNetwork @vnet

$subnet = @{
    Name = $defaultSubnet
    VirtualNetwork = $virtualNetwork
    AddressPrefix = $subnetvnetAddressSpace
}
$subnetConfig = Add-AzVirtualNetworkSubnetConfig @subnet

$virtualNetwork = $virtualNetwork | Set-AzVirtualNetwork

# SPOKE 2

$vnetName = "spoke2-vnet"
$vnetAddressSpace = "10.30.0.0/19"
$subnetvnetAddressSpace ="10.30.1.0/24"

$vnet = @{
    ResourceGroupName = $ResourceGroupName
    Location = $LocationName
    Name = $vnetName
    AddressPrefix = $vnetAddressSpace
}
$virtualnetwork = New-AzVirtualNetwork @vnet

$subnet = @{
    Name = $defaultSubnet
    VirtualNetwork = $virtualNetwork
    AddressPrefix = $subnetvnetAddressSpace
}
$subnetConfig = Add-AzVirtualNetworkSubnetConfig @subnet

$virtualNetwork = $virtualNetwork | Set-AzVirtualNetwork

# MERK: To virtuelle nettverk kan ikke snakke med hverandre,
# hub-vnet vil som standard ikke kunne koble til spoke1-vnet og spoke2-vnet. 
# For å få det til å fungere, må vi aktivere vnet-peering mellom hub og eiker.

$hubvnetName = "hub-vnet"
$spoke1vnetName = "spoke1-vnet"
$spoke2vnetName = "spoke2-vnet"

$hubVnet = Get-AzVirtualNetwork -Name $hubvnetName
$spoke1Vnet = Get-AzVirtualNetwork -Name $spoke1vnetName
$spoke2Vnet = Get-AzVirtualNetwork -Name $spoke2vnetName


# Peer Hub to Spoke 1.
$hubtospoke1 = @{
    Name = 'LinkHubToSpoke1'
    VirtualNetwork = $hubVnet
    RemoteVirtualNetworkId = $spoke1Vnet.Id
}
Add-AzVirtualNetworkPeering @hubtospoke1

# Peer Spoke 1 to Hub
$spoke1tohub = @{
    Name = 'LinkSpoke1ToHub' 
    VirtualNetwork = $spoke1Vnet 
    RemoteVirtualNetworkId = $hubVnet.Id
}
Add-AzVirtualNetworkPeering @spoke1tohub

# Peer Hub to Spoke 2
$hubtospoke2 = @{
    Name = 'LinkHubToSpoke2'
    VirtualNetwork = $hubVnet
    RemoteVirtualNetworkId = $spoke2Vnet.Id
}
Add-AzVirtualNetworkPeering @hubtospoke2

# Peer Spoke 2 to Hub.
$spoke2tohub = @{
    Name = 'LinkSpoke2ToHub' 
    VirtualNetwork = $spoke2Vnet 
    RemoteVirtualNetworkId = $hubVnet.Id
}
Add-AzVirtualNetworkPeering @spoke2tohub

# Nå er det opprettet peering mellom hub og spokes, men ikke mellom spoke1 og spoke2.
# Dvs. at spoke1 og spoke2 kan ikke kommunisere med hverandre.
# En kan, men bør ikke peere spokes, siden all trafikk skal gå igjennom huben.

# Nå er alt bare et internt nettverk i Azure.
# For å kunne kommunisere med tjenester på innsiden kan det være lurt å opprette en VPN-gatewayen. 
# VPN-gateway trenger et GatewaySubnet i hub-vnet. Vi trenger også offentlig ip for det.
# Tester med RouteBased VPN og Basic Stok Keeping Unit (SKU).
# Nedenfor skal vi: opprette gateway-subnett i hub-vnet og opprette vpn-gateway.
# Disse trinnene kan ta rundt 45 minutter, og det avhenger av typen SKU.

$vnetName = "hub-vnet"
$gwAddressSpace = "10.10.31.0/27"

$hubVnet = Get-AzVirtualNetwork -Name $vnetName

# Create GatewaySubnet
$gwsn = @{
    Name = "GatewaySubnet"
    AddressPrefix = $gwAddressSpace
    VirtualNetwork = $hubVnet
}
$gwSubnet = Add-AzVirtualNetworkSubnetConfig @gwsn

$updateHubVnet = Set-AzVirtualNetwork -VirtualNetwork $hubVnet

$gwpublicip = @{
    Name = "GateWayPIp"
    ResourceGroupName = $ResourceGroupName
    AllocationMethod ="Dynamic"
    Location = $LocationName
}
$gwpip = New-AzPublicIpAddress @gwpublicip
               
$gwSubnet = Get-AzVirtualNetworkSubnetConfig -Name 'GatewaySubnet' -VirtualNetwork $updateHubVnet
$gwipconf = New-AzVirtualNetworkGatewayIpConfig -Name "VPNGatewayIp" -Subnet $gwSubnet -PublicIpAddress $gwpip

New-AzVirtualNetworkGateway -Name "AzVpn" -ResourceGroupName $ResourceGroupName `
                            -Location $LocationName -IpConfigurations $gwipconf -GatewayType Vpn `
                            -VpnType RouteBased -GatewaySku Basic


# Når VPN Gateway er ferdig opprettet, kan en enten konfigurere Site to site eller Point to Site.
# Vi vil ta en P2S tilnærming i dette eksemplet.

# Azure bruker sertifikat for å autentisere VPN-klient. 
# For å koble seg til hub-vnetet, må en ha klientsertifikatet installert.
# Nå for å generere et klientsertifikat må en først opprette et rotsertifikat,
# og deretter laste opp den offentlige nøkkelinformasjonen til Azure.
# Rotsertifikatet betraktes da som klarert.
# Du må også generere klientsertifikatet fra det samme rootsertifikatet.
# Vi vil i dette eksemplet benytte self-signed sertifikat.

$certStoreLocation = "Cert:\CurrentUser\My"
Get-ChildItem -Path "Cert:\CurrentUser\My"

# Oppretter self-signed root certificate
$cert = New-SelfSignedCertificate -Type Custom -KeySpec Signature `
-Subject "CN=P2SRootCert" -KeyExportPolicy Exportable `
-HashAlgorithm sha256 -KeyLength 2048 `
-CertStoreLocation $certStoreLocation -KeyUsageProperty Sign -KeyUsage CertSign

New-SelfSignedCertificate -Type Custom -DnsName "P2SRootClient" -KeySpec Signature `
                            -Subject "CN=P2SRootClient" -KeyExportPolicy Exportable `
                            -HashAlgorithm sha256 -KeyLength 2048 `
                            -CertStoreLocation $certStoreLocation `
                            -Signer $cert -TextExtension @("2.5.29.37={text}1.3.6.1.5.5.7.3.2")

$GatewayName="AzVpn"
$Gateway=Get-AzVirtualNetworkGateway -Name $GatewayName -ResourceGroupName $ResourceGroupName

$VPNClientAddressPool="192.168.0.0/24"
$rootCaBase64 = [system.convert]::ToBase64String($cert.RawData)

#Disabling BGP
$Gateway.BgpSettings =$null

#Setting the VPN Client Address Pool
Set-AzVirtualNetworkGateway -VirtualNetworkGateway $Gateway -VpnClientAddressPool $VPNClientAddressPool

#Uploading the Certificate to the gateway
Add-AzVpnClientRootCertificate -VpnClientRootCertificateName "P2SRootCA" `
                                -VirtualNetworkGatewayname $GatewayName `
                                -ResourceGroupName $ResourceGroupName `
                                -PublicCertData $rootCaBase64

Get-ChildItem -Path "Cert:\CurrentUser\My"



############################
# AZURE KEY VAULT - Del 2  #
############################

# Variable values til videre bruk
$kvName = "KeyVault-tim-demo"
$secretName = "localAdmin"
$localAdminUsername = "myLocalAdmin"

# Create a new Azure Key Vault
New-AzKeyVault `
-VaultName $kvName `
-ResourceGroupName $ResourceGroupName `
-Location $LocationName `
-EnabledForDeployment `
-EnabledForTemplateDeployment `
-EnabledForDiskEncryption `
-Sku standard

# Gi deretter tilgangsrettigheter til Azure Key Vault for den påloggede brukerkontoen
Get-AzAdUser | ft
Set-AzKeyVaultAccessPolicy `
-VaultName $kvName `
-ResourceGroupName $ResourceGroupName `
-UserPrincipalName "melling_ntnu.no#EXT#@digsec.onmicrosoft.com" `
-PermissionsToSecrets get, set

# Nå kan brukeren opprette en secret
$password = read-host -assecurestring
Set-AzKeyVaultSecret `
-VaultName $kvName `
-Name $secretName `
-SecretValue $password

# Etter at en har opprettet en secret, kan en hente ut secreten, opprette et nytt PSCredential-objekt
# som kan brukes i f.eks. VM-distribusjon.

$secret = Get-AzKeyVaultSecret `
-VaultName $kvName `
-Name $secretName
# Create a new PSCredential object
$cred = [PSCredential]::new($localAdminUsername,$secret.
SecretValue)

# Create VM
$vmParams = @{
    ResourceGroupName = $ResourceGroupName
    Name = "tim-vm-mi-demo" # managed identity
    Location = $LocationName
    ImageName = 'Win2019Datacenter'
    VirtualNetworkName = $spoke1vnetName
    SubnetName = $spoke1Vnet.subnets.name  # For å liste ut: $spoke1Vnet.subnets | Select-Object name
    #PublicIpAddressName = "$pubIp-$id"
    Credential = $cred
    OpenPorts = 3389
  }
$newVM1 = New-AzVM @vmParams
$newVM1

Update-AzVM -ResourceGroupName $ResourceGroupName -VM $newVM1 -IdentityType SystemAssigned
# Når en oppretter en ny policy for tilgang til Key Vaultet, kan en nå velge identitet med
# samme navn (som VM-en) for å gi tilgang til Key Vaultets enheter

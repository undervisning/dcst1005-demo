$folders = (’C:\shares\backup’) 
mkdir -path $folders
$folders | ForEach-Object {$sharename = (Get-Item $_).name; New-SMBShare -Name $shareName -Path $_ -FullAccess Everyone}
$folders | Where-Object {$_ -like "*shares*"} | ForEach-Object {$name = (Get-Item $_).name; $DfsPath = (‘\\undervisning.sec\files\’ + $name); $targetPath = (‘\\tim-srv1\’ + $name);New-DfsnFolderTarget -Path $dfsPath -TargetPath $targetPath}
dir \\undervisning.sec\files


$destination = '\\undervisning.sec\files\backup'


Get-ChildItem -Path 'C:\Users\melling.undervisning\Documents' -File -Recurse |

    ForEach-Object {
 
        $dupeFile = Join-Path -Path $destination -ChildPath $_.Name
        if (Test-Path -Path $dupeFile) {
            # Sjekker om filen eksisterer fra før, hvis den gjør det OG er nyere, skipper den backup
            if ((Get-Item -Path $dupeFile).LastWriteTime -gt $_.LastWriteTime ) {
                Write-Host "Backupfil '$dupeFile' er nyere. Hopper over." # <-- Kanskje denne informasjonen bør sendes til en fil?
                $skipFile = $true
            }
        }
        if (!$skipFile) {
            $_ | Copy-Item -Destination $destination -Force
        }
  } 

Import-Module ActiveDirectory
Get-Command -Module ActiveDirectory
Get-ADOrganizationalUnit -filter * | ft name 

New-ADOrganizationalUnit "Sales" -Description "Sales OU"
New-ADOrganizationalUnit "IT Admins" -Description "IT Admin OU"
Get-ADObject
Get-ADObject -Filter *
Get-ADObject -Identity "OU=Sales,DC=undervisning,DC=sec" | Set-ADObject -ProtectedFromAccidentalDeletion:$false
Remove-ADObject -Identity "OU=Sales,DC=Undervisning,DC=sec" -Recursive -Confirm $False

Set-ADOrganizationalUnit -Identity "OU=Sales,DC=Undervisning,DC=sec" -ManagedBy "CN=Tor Ivar Melling,CN=Users,DC=undervisning,DC=sec"


Get-ADUser -Filter * | ft name
$Password = Read-Host -Prompt 'Enter Password' -AsSecureString

New-ADUser -Name "Hans Hansen" `
 -GivenName "Hans" `
 -Surname "Hansen" `
 -SamAccountName  "hans.hansen" `
 -UserPrincipalName  "hans.hansen@undervisning.sec" `
 -Path "CN=Users,DC=undervisning,DC=sec" `
 -AccountPassword $Password -Enabled $true

# I OU Sales
 New-ADUser -Name "Hans Hansen" `
 -GivenName "Hans" `
 -Surname "Hansen" `
 -SamAccountName  "hans.hansen" `
 -UserPrincipalName  "hans.hansen@undervisning.sec" `
 -Path "OU=Sales,DC=undervisning,DC=sec" `
 -AccountPassword $Password -Enabled $true

Remove-ADUser -Identity "CN=Hans Hansen,CN=Users,DC=Undervisning,DC=sec"

Move-ADObject -Identity "CN=Hans Hansen,OU=Sales,DC=Undervisning,DC=sec" -TargetPath "OU=IT Admins,DC=undervisning,DC=sec"

Rename-ADObject -Identity "CN=User,CN=Users,DC=Undervisning,DC=sec"  -NewName "User Account"

Disable-ADAccount -Identity "CN=Hans Hansen,OU=IT Admins,DC=Undervisning,DC=sec"
Enable-ADAccount -Identity "CN=Hans Hansen,OU=IT Admins,DC=Undervisning,DC=sec"

Search-ADAccount –AccountDisabled –UsersOnly –ResultPageSize 2000 –ResultSetSize $null | Select-Object SamAccountName, DistinguishedName

Search-ADAccount -LockedOut -UsersOnly | Format-Table Name,LockedOut -AutoSize
Unlock-ADAccount -Identity "CN=Hans Hansen,CN=IT Admins,DC=Undervisning,DC=sec"
Search-ADAccount -LockedOut -UsersOnly | Unlock-ADAccount
Set-ADAccountControl -Identity hans.hansen -Enable $false
Set-ADAccountControl -Identity hans.hansen -Enable $true
Set-ADAccountControl -Identity User -PasswordNotRequired $true
Set-ADAccountControl -Identity User -CannotChangePassword $true
Set-ADAccountControl -Identity User -AllowReversiblePasswordEncryption $true
Set-ADAccountControl -Identity User -PasswordNeverExpires $true
Set-ADAccountControl -Identity User -MNSLogonAccount $true

Set-ADAccountExpiration -Identity "CN=User,OU=Organizational Unit,DC=Undervisning,DC=sec" -DateTime "03/01/2019 00:00:00"

New-ADGroup -GroupCategory Security -GroupScope Global -Name "IT Administrators" -Path "OU=IT Admins,DC=Undervisning,DC=sec" -SamAccountName "it.admins"

New-ADGroup -Name "IT Administrators"
Add-ADGroupMember -Identity "CN=IT Administrators,OU=IT Admins,DC=Undervisning,DC=sec" -Members "Hans.Hansen" 
Remove-ADGroupMember -Identity "CN=Group,OU=Organizational Unit,DC=Undervisning,DC=sec" -Members "User"

Add-ADGroupMember -Identity "CN=Group,OU=Organizational Unit,DC=Undervisning,DC=sec" -Members "User" -MemberTimeToLive (New-TimeSpan Days 14)
Get-ADGroup "CN=Group,OU=Organizational Unit,DC=Undervisning,DC=sec" -Property member -ShowMemberTimeToLive
Set-ADGroup -Identity "CN=Group,OU=Organizational Unit,DC=Undervisning,DC=sec" -GroupScope Global
Set-ADGroup -Identity "CN=Group,OU=Organizational Unit,DC=Undervisning,DC=sec" -GroupScope Universal
Set-ADGroup -Identity "CN=Group,OU=Organizational Unit,DC=Undervisning,DC=sec" -GroupScope DomainLocal
Set-ADGroup -Identity "CN=Group,OU=Organizational Unit,DC=Undervisning,DC=sec" -GroupCategory Security
Set-ADGroup -Identity "CN=Group,OU=Organizational Unit,DC=Undervisning,DC=sec" -GroupCategory Distribution


Get-ADGroupMember -Identity "CN=Group,OU=Organizational Unit,DC=Undervisning,DC=sec" -Recursive | Out-GridView

Get-ADGroup -Filter * -Properties members | Where-Object {$_.Members.count -eq 0}  | Out-GridView


New-ADComputer -Name "Computer" -sAMAccountName "Computer" -Path "CN=Computers,DC=Undervisning,DC=sec"
Remove-ADComputer -Identity "CN=Computer,CN=Computers,DC=Undervisning,DC=sec"

Add-Computer -DomainName undervisning.sec -Credential UNDERVISNING\Administrator

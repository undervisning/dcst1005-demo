# Microsoft docs - Active Directory PowerShell
# https://docs.microsoft.com/en-us/powershell/module/activedirectory/?view=windowsserver2022-ps 
Import-Module ActiveDirectory

# Fra Win10 mgr koble til dc1 via PowerShell
Enter-PSSession -ComputerName dc1-demo

Get-ADUser -Filter * | Format-Table
Get-ADUser -Filter * | Select-Object Name

<#
cloudbase-init is a service conceived and maintained by 
Cloudbase Solutions Srl, currently working on NT systems.
It was designed to initialize and configure guest operating
systems under OpenStack, OpenNebula, CloudStack, MaaS and many others.

The KRBTGT account is a domain default account that acts
as a service accountfor the Key Distribution Center (KDC) service.
This account cannot be deleted, account name cannot be changed,
and it cannot be enabled in Active Directory
#>

# AD Group (samle brukere med felles behvo)
Get-ADGroup -Filter * | Format-Table

# OU - Gruppering av maskiner, brukere og grupper (kan settes reler )
Get-ADOrganizationalUnit -Filter * | Format-Table name

<# 
Installer Windows Admin Center på MGR
https://www.microsoft.com/en-us/evalcenter/evaluate-windows-admin-center
#>

<#
1. Opprette OU-struktur for brukere og grupper (New, Remove, Set)
    URL: https://docs.microsoft.com/en-us/powershell/module/activedirectory/new-adorganizationalunit?view=windowsserver2022-ps
2. Opprette grupper hvor en ønsker å legge brukere med felles behov (New, Remove, Set)
    URL: https://docs.microsoft.com/en-us/powershell/module/activedirectory/new-adgroup?view=windowsserver2022-ps
3. Opprette brukere (New, Remove, Set)
    URL: https://docs.microsoft.com/en-us/powershell/module/activedirectory/new-aduser?view=windowsserver2022-ps
#>
Get-ADOrganizationalUnit -Filter * | Format-Table name
# Oppretter to nye OU-er
New-ADOrganizationalUnit "Sales" -Description "Sales OU"
New-ADOrganizationalUnit "IT Admins" -Description "IT Admin OU"

Get-ADOrganizationalUnit -Filter * | Format-Table name,DistinguishedName
<#
The DN is the name that uniquely identifies an entry in the directory.
The DN contains one component for each level of the directory hierarchy
from the root down to the level where the entry resides
#>
Remove-ADOrganizationalUnit -Identity "OU=Sales,DC=Undervisning,DC=sec" -Recursive -Confirm:$false
Get-ADObject -Identity "OU=Sales,DC=undervisning,DC=sec" | Set-ADObject -ProtectedFromAccidentalDeletion:$false
Get-ADObject -Identity "OU=IT Admins,DC=undervisning,DC=sec" | Set-ADObject -ProtectedFromAccidentalDeletion:$false
Remove-ADOrganizationalUnit -Identity "OU=IT Admins,DC=Undervisning,DC=sec" -Recursive -Confirm:$false


<# GRUPPER
Groups are generally used for security purposes,
like giving permissions on a resource or granting privileges in an application.
An OU (Organizational Unit) is more of a logical boundary.
It can contain groups, users, computers and other OUs.
                                    brukere, sales, 
                                    brukere, hr  , 
                                    brukere, it  , printere, read/write/print
The abbreviation AGDLP stands for “Account, Global, Domain Local, Permission” 
and represents Microsoft's recommended procedure for implementing role-based
access control within Windows domains. 

#> 

New-ADGroup -GroupCategory Security `
    -GroupScope Global `
    -Name "IT Administrators" `
    -Path "OU=IT Admins,DC=Undervisning,DC=sec" `
    -SamAccountName "it.admins"
New-ADGroup -GroupCategory Security `
    -GroupScope Global `
    -Name "Sales" `
    -Path "OU=Sales,DC=Undervisning,DC=sec"`
    -SamAccountName "Sales"

<# OPPRETT BRUKERE
Opprett brukere i Domenet som skal kunne logge på cl1 og eventuelt domeneadministratorer
#>
Get-ADUser -Filter * | ft name,DistinguishedName
$Password = Read-Host -Prompt 'Enter Password' -AsSecureString

New-ADUser -Name "Tor Ivar Melling" `
    -GivenName "Tor Ivar" `
    -Surname "Melling" `
    -SamAccountName  "tor.melling" `
    -UserPrincipalName  "tor.i.melling@undervisning.sec" `
    -Path "CN=Users,DC=undervisning,DC=sec" `
    -AccountPassword $Password -Enabled $true

Get-ADUser -Identity hans.hansen | Move-ADObject -TargetPath "OU=Sales,DC=undervisning,DC=sec"
Remove-ADUser -Identity hans.hansen

# I OU Sales
    New-ADUser -Name "Hans Hansen" `
    -GivenName "Hans" `
    -Surname "Hansen" `
    -SamAccountName  "hans.hansen" `
    -UserPrincipalName  "hans.hansen@undervisning.sec" `
    -Path "OU=Sales,DC=undervisning,DC=sec" `
    -Department "Sales" `
    -AccountPassword $Password -Enabled $true

Get-ADUser -Filter * -Properties department | Where-Object {$_.department -eq 'Sales'} | format-table
$salesusers = Get-ADUser -Filter * -Properties department | Where-Object {$_.department -eq 'Sales'}
$salesusers.SamAccountName

Get-ADGroup -identity 'sales'
Add-ADGroupMember -Identity 'sales' -Members $salesusers.samaccountname
Get-ADGroupMember -Identity 'sales' | Select-Object name

# Distributed File System
# https://docs.microsoft.com/en-us/windows-server/storage/dfs-namespaces/dfs-overview
# - DFS Namespaces - SRV1 - #
Enter-PSSession srv1-demo
Get-WindowsFeature -Name fs*
Install-WindowsFeature -Name FS-DFS-Namespace,FS-DFS-Replication,RSAT-DFS-Mgmt-Con -IncludeManagementTools
Get-WindowsFeature -Name fs*
Import-Module dfsn

# Oppretter mapper for share og DFS Root folder
# In DFS terms the Root is the share and a Link is a virtual Folder name to a remote server Share\folder.
$folders = ('C:\dfsroots\files','C:\shares\finance','C:\shares\sale','C:\shares\it','C:\shares\dev','C:\shares\hr')
mkdir -path $folders
# Deler alle mappene så de er tilgjengelige på nettverket
$folders | ForEach-Object {$sharename = (Get-Item $_).name; New-SMBShare -Name $shareName -Path $_ -FullAccess Everyone}

<# LES OG GJØR:
Følgende kommando: 
    New-DfsnRoot -TargetPath \\srv1\files -Path \\undervisning.sec\files -Type DomainV2
Gir feilmelding:
    [srv1]: PS C:\Users\Administrator\Documents> New-DfsnRoot -TargetPath \\srv1\files -Path \\undervisning.sec\files -Type DomainV2
    New-DfsnRoot : A general error occurred that is not covered by a more specific error code.
        + CategoryInfo          : NotSpecified: (MSFT_DFSNamespace:ROOT\Microsoft\...FT_DFSNamespace) [New-DfsnRoot], 
        CimException
        + FullyQualifiedErrorId : MI RESULT 110,New-DfsnRoot

Når kommandoen kjøres lokalt på srv1 (ikke PSSession) maskinen i PowerShell 7.x som administrator:

Path                     Type      Properties TimeToLiveSec State  Description
----                     ----      ---------- ------------- -----  -----------
\\undervisning.sec\files Domain V2            300           Online

PS C:\Users\Administrator> 

Kjør kommandoen lokalt på srv1 maskinen, pass på at maskinnavn og domenenavn er korrekt.

#>

# Benytter innholdet i $folders variabelen fra tidligere og peker alle delte områdene fra \\srv1\ til --> \\undervisning.sec\files
$folders | Where-Object {$_ -like "*shares*"} | ForEach-Object {$name = (Get-Item $_).name; $DfsPath = ('\\undervisning.sec\files\' + $name); $targetPath = ('\\srv1-lc\' + $name);New-DfsnFolderTarget -Path $dfsPath -TargetPath $targetPath}
dir \\undervisning\files

# - Legge til share fra DC1 (bare for å vise at det kan være forskjellige servere som aksesseres fra samme nanv) - #
Enter-PSSession dc1-demo
Get-WindowsFeature -Name fs*
Install-WindowsFeature -Name FS-DFS-Namespace,FS-DFS-Replication,RSAT-DFS-Mgmt-Con -IncludeManagementTools
Get-WindowsFeature -Name fs*
Import-Module dfsn
$folders = ('C:\dfsroots\files','C:\shares\installerfiles','C:\shares\projects','C:\shares\it-admins')
mkdir -path $folders
$folders | ForEach-Object {$sharename = (Get-Item $_).name; New-SMBShare -Name $shareName -Path $_ -FullAccess Everyone}

#MERK: dc1 er navnet på  server - undervisning.sec er mitt domene. Denne informasjonen må erstattes i henhold til eget oppsett om en benytter andre navn.
New-DfsnRoot -TargetPath \\dc1-lc\files -Path \\undervisning.sec\files -Type DomainV2
$folders | Where-Object {$_ -like "*shares*"} | ForEach-Object {$name = (Get-Item $_).name; $DfsPath = ('\\undervisning.sec\files\' + $name); $targetPath = ('\\dc1-lc\' + $name);New-DfsnFolderTarget -Path $dfsPath -TargetPath $targetPath}
dir \\undervisning\files

(Get-Acl -Path \\undervisning.sec\files\accounts).Access

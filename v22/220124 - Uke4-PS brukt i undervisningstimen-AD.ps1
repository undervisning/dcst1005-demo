# Lister ut maskinnavn og endrer til ønsket navn
$env:COMPUTERNAME
$newcompname = Read-host "Skriv inn ønsket hostname på maskinen"
Rename-Computer -Newname $newcompname -Restart -Force


# Installajon av AD DS - Kjøres lokalt på maskinen som skal ha installert AD DS.
# Gjør det enklere å copy / paste passordet fra OpenStack: (IKKE anbefalt med passord i klartekst i script)
# $Password = ConvertTo-SecureString "P@ssW0rD!" -AsPlainText -Force
# run as administrator
Install-WindowsFeature AD-Domain-Services, DNS -IncludeManagementTools
$Password = Read-Host -Prompt 'Enter Password' -AsSecureString
Set-LocalUser -Password $Password Administrator
$Params = @{
    DomainMode = 'WinThreshold'
    DomainName = 'undervisning.sec'
    DomainNetbiosName = 'undervisning'
    ForestMode = 'WinThreshold'
    InstallDns = $true
    NoRebootOnCompletion = $true
    SafeModeAdministratorPassword = $Password
    Force = $true
}
Install-ADDSForest @Params
Restart-Computer
# Log in as undervisning\Administrator with password from above, test our domain
Get-ADRootDSE
Get-ADForest
Get-ADDomain
# Any computers joined the domain?
Get-ADComputer -Filter * | Select-Object DNSHostName


# GJØRES PÅ DE ANDRE MASKINENE SOM SKAL MELDES INN I DOMENET
# Sett IP-adressen til DC1 som DNS på de andre maskinene. Dette må gjøres for at de skal kjenne til navnet til domenet.
# Kjør kommandoen ipconfig på DC1
ipconfig
Get-NetAdapter | Set-DnsClientServerAddress -ServerAddresses IP_ADDRESS_OF_DC1
# Kontroller deretter konfigurasjonen med følgene kommando:
ipconfig /all
# Se etter: DNS Servers . . . . . . . . . . . : IP.ADR.TIL.DC (192.168.x.x)

# Legger til maskin i domenet
# For Windows 10 må en gjøre følgende i Windows PowerShell 5.1
#

$cred = Get-Credential -UserName 'UNDERVISNING\Administrator' -Message 'Cred'
Add-Computer -Credential $cred -DomainName undervisning.sec -PassThru -Verbose
Restart-Computer

# Koble seg til andre maskiner over PowerShell
# Hvis maskinen ikke tillater autentisering for oppkoblingen, kjør følgende i powershell:
# winrm set winrm/config/service/auth '@{Kerberos="true"}' 
# for å liste ut: winrm get winrm/config/service/auth
# Hvis maskina ikke har aktivert PSRemote:
# Enable-PSRemoting -Force
Enter-PSSession -ComputerName dc1
Enter-PSSession -ComputerName srv1

####################################################
#             Opprette AD struktur                 #
#    - OU-er                                       #
#    - Grupper                                     #
#    - Brukere                                     #
####################################################

Enter-PSSession -ComputerName dc1-lc
Get-ADComputer -Filter * | Select-Object DNSHostName
Get-ADOrganizationalUnit -Filter * | Format-Table name,DistinguishedName

# Opprette OU-struktur
# Topp OU for eksempelbedriftens brukere og maskiner
$rootuserou = 'learnIT-users'
$rootmachineou = 'learnIT-machines'

$topou=@($rootuserou, $rootmachineou)
$topou | foreach {New-ADOrganizationalUnit -Name $_}

$rootuserou = Get-ADOrganizationalUnit -filter * | Where-Object name -eq $rootuserou | Select-Object name,DistinguishedName
$rootmachineou = Get-ADOrganizationalUnit -filter * | Where-Object name -eq $rootmachineou | Select-Object name,DistinguishedName

$usersou=@('it', 'dev', 'finance', 'sale', 'hr')
$usersou | foreach { New-ADOrganizationalUnit -name $_ -Path $rootuserou.DistinguishedName}

$computerou=@('workstations', 'servers') # Merk at workstations gjennbrukes som ren tekst nedenfor, endring her må endres nedenfor
$computerou | foreach { New-ADOrganizationalUnit -name $_ -Path $rootmachineou.DistinguishedName}

$computerouws=@('it', 'dev', 'finance', 'sale', 'hr')
$wsou = Get-ADOrganizationalUnit -filter * | Where-Object name -eq 'workstations' | Select-Object name,DistinguishedName
$computerouws | foreach { New-ADOrganizationalUnit -name $_ -Path $wsou.DistinguishedName}


# Opprette OU for grupper under root user OU
$groupou=@('global', 'local')
$groupou | foreach { New-ADOrganizationalUnit -name $_ -Path $rootuserou.DistinguishedName
        If ($_ -eq "global") {$globalou = Get-ADOrganizationalUnit -filter * | Where-Object name -eq $_ | Select-Object name,DistinguishedName}
        ElseIf ($_ -eq "local") {$localou = Get-ADOrganizationalUnit -filter * | Where-Object name -eq $_ | Select-Object name,DistinguishedName}        
    }

$globalgroups=@('g_it', 'g_dev','g_finance','g_sale', 'g_hr')
$globalgroups | foreach { New-ADGroup -GroupCategory Security `
    -GroupScope Global `
    -Name $_ `
    -Path $globalou.DistinguishedName `
    -SamAccountName "$_"}

$localgroups=@('l_it', 'l_dev','l_finance','l_sale', 'l_hr')
$localgroups | foreach { New-ADGroup -GroupCategory Security `
    -GroupScope DomainLocal `
    -Name $_ `
    -Path $localou.DistinguishedName `
    -SamAccountName "$_"}

# Lister ut en oversikt over opprettede OU-er
Get-ADOrganizationalUnit -Filter * | Format-Table name,DistinguishedName
Get-ADOrganizationalUnit -filter * | Where-Object name -eq 'it' | Select-Object name,DistinguishedName

<# 
Oppretter brukere fra .csv-fil som får tilhørighet i riktig gruppe samt plasseres i riktig OU.
I tillegg kjører vi en sjekk mot ugyldige bokstaver. MERK at denne filen må kopieres til et område
som kan nås maskinen som utfører brukerimporten. Vi må derfor kopiere .csv-filen til ett av de delte 
områdene vi har laget tidligere. F.eks: it-admins
Merk at path må være det en selv har på maskina si, min .csv-fil ligger på C:\gitprojects\dcst1005-demo\v22\
#>
Copy-Item -Path "C:\gitprojects\dcst1005-demo\csv-files\my-users.csv" `
        -Destination \\undervisning.sec\files\it-admins -recurse -Force

# Dette hadde jeg fra før: $ADUsers = Import-csv "C:\gitprojects\dcst1005-demo\v22\my-users.csv" -Delimiter ";" 
# Dette må jeg ha nå:
$ADUsers = Import-csv "\\undervisning.sec\files\it-admins\my-users.csv" -Delimiter ";" 
# Headers: Username;GivenName;SurName;UserPrincipalName;DisplayName;Password;Department;Path
foreach ($User in $ADUsers)
{   
    $name = $User.GivenName + " " + $User.Surname
    New-ADUser `
    -SamAccountName $User.Username `
    -UserPrincipalName $User.UserPrincipalName `
    -Name $name `
    -GivenName $user.GivenName `
    -Surname $user.SurName `
    -Enabled $True `
    -ChangePasswordAtLogon $false `
    -DisplayName $user.Displayname `
    -Department $user.Department `
    -Path $user.path `
    -AccountPassword (convertto-securestring $user.Password -AsPlainText -Force)

}

# Hvordan finne grupper (mine brukergrupper starter på: g_ for global)
$globalgroups = Get-ADGroup -Filter * | Where-Object name -like '*g_*'
$globalgroups
$globalgroups | ft
$globalgroups | Select-Object name,ObjectGUID

# Henter ut alle som har department dev, it, sale, hr, finance. 
# MERK: jeg har departmentnavnet likt det som kommer etter g_ i gruppenavnet
$department=@('dev', 'sale', 'finance', 'it', 'hr')
$department | foreach {
    $Userlist = Get-ADUser -Filter * -Properties department | Where-Object department -Like "$_"
    $Userlist | Select-Object name,samaccountname
    $groupename = Get-ADGroup -filter * | Where-object name -like "g_*$_"
    $groupename | Select-Object name
    $groupename | Add-ADGroupMember -Members $UserList.samaccountname
}

# Lister ut medlemmene
$department | foreach {
    $groupname = "g_$_"
    "---------------"
    Write-Host "Gruppen $groupname har følgende medlemmer"
    $groupusers = Get-ADGroupMember -Identity $groupname
    $groupusers.name
}

# Legger alle globale grupper i de lokale gruppene som skal få tilgang på ressursene
$department | foreach {
    $localgroups = Get-ADgroup -Filter * | Where-object name -like "l_*$_"
    $localgroups | Select-Object name,samaccountname
    $globalgroup = Get-ADGroup -filter * | Where-object name -like "g_*$_"
    $globalgroup | Select-Object name,samaccountname
    $localgroups | Add-ADGroupMember -Members $globalgroup.samaccountname
}

# Lister ut medlemmene i de lokale gruppene
$department | foreach {
    $groupname = "l_$_"
    "---------------"
    Write-Host "Gruppen $groupname har følgende medlemmer"
    $groupmembers = Get-ADGroupMember -Identity $groupname
    $groupmembers.name
}


<# Slette brukere fra gruppene
$department | foreach {
    $Userlist = Get-ADUser -Filter * -Properties department | Where-Object department -Like "$_"
    $group = Get-ADGroup -filter * | Where-object name -like "g_*$_" | Remove-AdGroupMember -Members $UserList.samaccountname
}
#>




####################################################
#         Rettigheter på delte områder             #
#   - Sales-området skal kun være tilgjengelig     #
#     for de som jobber med salg                   #
####################################################

dir \\undervisning\files

# Liste ut eksisterende rettigheter
Get-Acl \\undervisning\files\sales | Format-List
(Get-Acl \\undervisning\files\sales).Owner
(Get-Acl \\undervisning\files\sales).Access | Format-List

<#
Før vi kan angi et objekts ACL må vi opprette det. 
Den enkleste måten å gjøre det på er å lagre gjeldende ACL i en variabel og deretter endre den.
For å gjøre det setter vi bare utdataene fra Get-Acl til $acl.
#>

$acl = Get-Acl \\undervisning\files\sales

<#
Alternativt å opprette nytt objekt for acl:
#>
$acl = New-Object System.Security.AccessControl.DirectorySecurity
# Hvis sistnenvte så må Owner legges til:
$owner = New-Object System.Security.Principal.NTAccount("BUILTIN\Administrators")
$acl.SetOwner($owner)

<# MERK at vi ikke har endret noe på filsystemet ennå. Vi har bare endret $acl-objektet

Arv
Når vi oppretter tilgangsregler, må vi også spesifisere arv. Dette gjøres ved fire alternativer. 
En vil mest sannsynlig angi både ContainerInheritance- og ObjectInheritance-flaggene.
Dette indikerer at en ønsker at tillatelsen vår skal spres til alle underordnede kataloger og filer.

ContainerInherit - Forplant til underordnede beholdere (kataloger).
None - Ikke spre denne tillatelsen til noen underordnede beholdere eller objekter.
ObjectInherit - Forplante seg til underordnede objekter (filer).

Propagation Flags
I tillegg til arveflagg kan vi også sette propagation.
Standard i Windows None.
Tre alternativene for propagation flags. 
Hvis du setter dette flagget til InheritOnly, vil tilgangsregelen kun gjelde for underordnede objekter i beholderen.

InheritOnly - Spesifiserer at ACE-en bare spres til underordnede objekter.
None - No inheritance flagg satt.
NoPropagateInherit - ACE går ikke nedover til underelementer.

Flag Combinations and Effect
The table below details how a rule will be apply to the current object and child objects based on the flags set in the access rule.


Access Rule Applies To                  Inheritance Flags                   Propagation Flags
Subfolders and Files only               ContainerInherit,ObjectInherit      InheritOnly
This Folder, Subfolders and Files       ContainerInherit,ObjectInherit	    None
This Folder, Subfolders and Files       ContainerInherit,ObjectInherit	    NoPropagateInherit
This folder and Subfolders	            ContainerInherit	                None
Subfolders only	Container               Inherit                             InheritOnly
This Folder and Files                   ObjectInherit                       None
This Folder and Files                   ObjectInherit                       NoPropagateInherit
This Folder only                        None                                None

GUI: Inheritance Flags

#>

#Enter-PSSession srv1-lc

$folders = "\\undervisning.sec\files\sale"
Get-SmbShareAccess -name 'sale'
Get-Acl -Path $folders
(Get-Acl -Path $folders).Access
(Get-Acl -Path $folders).Access | Format-Table -AutoSize
(Get-Acl -Path $folders).Access | Where-Object {$_.IsInherited -eq $true} | Format-Table -AutoSize
(Get-ACL -Path $folders).Access | Format-Table IdentityReference,FileSystemRights,AccessControlType,IsInherited,InheritanceFlags -AutoSize

$folders = ('C:\shares\sale')
$acl = Get-Acl \\undervisning\files\sale
$AccessRule = New-Object System.Security.AccessControl.FileSystemAccessRule("undervisning\l_sale","FullControl","Allow")
$acl.SetAccessRule($AccessRule)
$ACL | Set-Acl -Path "\\undervisning\files\sale"

# To modify the inheritance properties of an object, we have to use the SetAccessRuleProtection method with the constructor: isProtected, preserveInheritance. 
# The first isProtected property defines whether or not the folder inherits its access permissions or not. Setting this value to $true will disable inheritance
$ACL = Get-Acl -Path "\\undervisning\files\sale"
$ACL.SetAccessRuleProtection($true,$true)
$ACL | Set-Acl -Path "\\undervisning\files\sale"

$acl = Get-Acl "\\undervisning\files\sale"
$acl.Access | where {$_.IdentityReference -eq "BUILTIN\Users" } | foreach { $acl.RemoveAccessRuleSpecific($_) }
Set-Acl "\\undervisning\files\sale" $acl
(Get-ACL -Path "\\undervisning\files\sale").Access | Format-Table IdentityReference,FileSystemRights,AccessControlType,IsInherited,InheritanceFlags -AutoSize




New-ADGroup -GroupCategory Security `
    -GroupScope Global `
    -Name 'g_alleansatte' `
    -Path 'OU=global,OU=learnIT-users,DC=undervisning,DC=sec' `
    -SamAccountName 'g_alleansatte'

    $department=@('dev', 'sale', 'finance', 'it', 'hr')
    $department | foreach {
        $Userlist = Get-ADUser -Filter * -Properties department | Where-Object department -Like "$_"
        $Userlist | Select-Object name,samaccountname
        $groupename = Get-ADGroup -filter * | Where-object name -like 'g_alleansatte'
        $groupename | Select-Object name
        $groupename | Add-ADGroupMember -Members $UserList.samaccountname
    }

New-ADGroup -GroupCategory Security `
    -GroupScope DomainLocal `
    -Name 'l_remotedesktop' `
    -Path 'OU=local,OU=learnIT-users,DC=undervisning,DC=sec' `
    -SamAccountName 'l_remotedesktop'

$rdpgroup = Get-AdGroup -filter * | Where-Object name -like 'l_remote*'
$rdpgroup | Add-ADGroupMember -Members 'g_alleansatte'
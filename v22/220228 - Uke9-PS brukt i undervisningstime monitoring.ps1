<#

Installere først IIS på srv1
Dette gir oss flere tjenester å overvåke

#>


# Install IIS - srv1 #
Enter-PSSession -ComputerName srv1-lc 
# https://docs.microsoft.com/en-us/powershell/module/servermanager/install-windowsfeature?view=windowsserver2022-ps
Install-WindowsFeature -name Web-Server -IncludeManagementTools
# Uninstall-WindowsFeature -name Web-Server -IncludeManagementTools
# Exit Remote


<#
Get-Counter (testes på mgr i starten)
https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.diagnostics/get-counter?view=powershell-7.2
#>

Get-Counter -ListSet * |
    Sort-Object CounterSetName |
    Select-Object -Property CounterSetName |
    Out-GridView
# F.eks kan en søke etter process, processor etc etc.. MERK: Out-GridView funker ikke remote


Get-Counter -ListSet * |
    Select-Object -ExpandProperty counter |
    Measure-Object


# Hvilken informasjon som kan hentes ut om en prosess i Windows
Get-Counter -ListSet Process | Select-Object -ExpandProperty counter
Get-Counter -ListSet PhysicalDisk | Select-Object -ExpandProperty counter


# Nederst finner vi: \Process(*)\Working Set


# Sjekk hvor mye minne all prosesser benytter , merk forskjellen på (*) og (_Total)
Get-Counter -Counter '\Process(_Total)\Working Set'
Get-Counter -Counter '\Process(*)\Working Set'

# Få tak i egenskapen:
(Get-Counter -Counter `
    '\Process(_Total)\Working Set').CounterSamples

# TypeName: Microsoft.PowerShell.Commands.GetCounter.PerformanceCounterSample
(Get-Counter -Counter `
    '\Process(_Total)\Working Set').CounterSamples | Get-Member

<#
Hva kan være interessant?
Timestamp? Er det viktig å vite tidspunktet for måleverdien?
"So apparently the CookedValue is the result of combining the counter's 
raw data to get a usable value that you can understand and work with."
#>
# Henter siste sekunds måling
(Get-Counter -Counter `
    '\Process(_Total)\Working Set').CounterSamples.CookedValue

# Henter tre målinger i tre sekunder, får da tre innslag som er fra 3 sekunder siden, 2 sekunder siden og 1 sekund siden
(Get-Counter -Counter `
    '\Process(_Total)\Working Set' -MaxSamples 3).CounterSamples.CookedValue

(Get-Counter -Counter `
    '\Process(_Total)\Working Set' -MaxSamples 3 -SampleInterval 2).CounterSamples.CookedValue

Get-Counter -Counter `
    '\Process(_Total)\Working Set' -Continuous


<#
Siden jeg er innlogget som min bruker, med administratorrettigheter i domenet, kan jeg også hente informasjon fra andre maskiner
Her fra eksempelvis dc1 og srv1
#>

$scriptblock = {
    Get-Counter -Counter '\Process(_Total)\Working Set'
    Get-Counter -Counter '\Processor(_Total)\% processor time'
}
Invoke-Command -ComputerName dc1-lc,srv1-lc `
    -ScriptBlock $scriptblock

<#
Eksporter til fil for visualisering
Hvis en er usikker på navn, kan en søke:
Get-Counter -ListSet * | where-Object -Property CounterSetName -like "*disk" <-- gjør at en finner alle som inneholder disk
    Get-Counter -ListSet PhysicalDisk | Select-Object -ExpandProperty counter <-- Når en søker på disk, kommer PhysicalDisk som et av treffene
Get-Counter -ListSet * | where-Object -Property CounterSetName -like "*network"
Get-Counter -ListSet * | where-Object -Property CounterSetName -like "*memory"
#>
# Hva som overvåkes
$counters = '\Process(_Total)\Working Set',
    '\Processor(_Total)\% processor time',
    '\PhysicalDisk(_Total)\% Disk Read Time'

Get-Counter -Counter $counters -MaxSamples 10 |
    ForEach-Object {
        $_.CounterSamples | ForEach-Object {
            [PSCustomObject]@{
                TimeStamp = $_.Timestamp
                Path = $_.Path
                Value = $_.CookedValue
            }
        }
    } | Export-CSV -Path "C:\Users\$env:username\Downloads\demo-countersamles.csv" -NoTypeInformation






<#
Monitorer tjenester og status på tjenester

#>


Get-Help *-Service

<#
Get-Service
New-Service
Remove-Service
Restart-Service
Resume-Service
Set-Service
Start-Service
Stop-Service
Suspend-Service 
#>

Enter-PSSession dc1-lc
$Services='DNS','DFS Replication','Intersite Messaging','Kerberos Key Distribution Center','NetLogon','Active Directory Domain Services','DFS Namespace','wuauserv','DsRoleSvc'
$services | foreach {Get-Service $_ | Select-Object Name, Status}
$services | foreach {Get-Service $_ | where-Object -Property Status -eq 'Stopped'}
ForEach ($Service in $Services) {Get-Service $Service | Select-Object Name, Status}
# Exit PS Session

# Invoke for å hente service status på remote server
$scriptblock = {
    $Services='DNS','DFS Replication','Intersite Messaging','Kerberos Key Distribution Center','NetLogon','Active Directory Domain Services','DFS Namespace','wuauserv','DsRoleSvc'
    Get-Service $Services | Select-Object Name,Status | ft
}

Invoke-Command -ComputerName dc1-lc `
    -ScriptBlock $scriptblock

# Starter tjenesten igjen hvis status er satt til 'Stopped'
$scriptblock = {
    $Service = 'W3SVC'
    while ((Get-Service $Service).Status -eq 'Stopped') 
    {
        Start-Service $Service -ErrorAction SilentlyContinue
        Start-Sleep 10
    } 
    Return "$($Service) has STARTED"
}

Invoke-Command -ComputerName srv1-lc `
    -ScriptBlock $scriptblock


(Get-Service -Name wuauserv).StartupType

<#
Test-NetConnection for å sjekke om HTTP svarer på web tjeneren
#>
Test-NetConnection -ComputerName "srv1-lc" -InformationLevel "Detailed"
Test-NetConnection -ComputerName "dc1-lc" -CommonTCPPort HTTP -InformationLevel "Detailed"
Test-NetConnection -ComputerName "srv1-lc" -CommonTCPPort RDP -InformationLevel "Detailed"

<#
Finn disker på maskinen og opprett partisjon.
MERK: EN MÅ LEGGE TIL VOLUME I SKYHIGH (OpenStack)
FØR EN KAN FINNE DET I OPERATIVSYSTEMET (egen video)
#>

# Lister ut alle diskene på maskinen:
# https://docs.microsoft.com/en-us/powershell/module/storage/get-disk?view=windowsserver2022-ps
Get-Disk

# Create partition
# https://docs.microsoft.com/en-us/powershell/module/storage/new-partition?view=windowsserver2022-ps
# Husk å erstatt <nummer> med tallet for ønsket disk
new-partition -disknumber <nummer> -usemaximumsize | format-volume -filesystem NTFS -newfilesystemlabel newdrive 
# Opprett drive letter
# https://docs.microsoft.com/en-us/powershell/module/storage/get-partition?view=windowsserver2022-ps
get-partition -disknumber <nummer> | set-partition -newdriveletter X

# Setter disk offline / online
# https://docs.microsoft.com/en-us/powershell/module/storage/set-disk?view=windowsserver2022-ps
Set-Disk -Number 1 -IsOffline $False # Online
Set-Disk -Number 1 -IsOffline $True # Offline

<#
Script for backup ved bruk av Robocopy
#>

$source = '\\undervisning\files\sale\'
$destination = 'G:\'

# Hente ut dato, for å kunne opprette en mappe som inneholder datoen
# https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/get-date?view=powershell-7.2
$date = Get-date -UFormat "%Y%m%d" # for å navngi folder
$yesterdaydate = (Get-Date).AddDays(-1).ToString('yyyyMMdd') # Hvis vi trenger gårsdagens dato
$checkday = Get-date -UFormat "%A" # sjekke dag for å avgjøre om en skal kjøre full eller incrementell backup
$logfile = "G:\logs\backuplogfile-$date.txt"

# sjekker om destinasjon eksisterer, oppretter mappe og kjører backup hvis mappen ikke finnes fra før
$checkdest = -join($destination,$date) # slår sammen G:\ og datoen
Test-Path -Path $checkdest

# Sjekker om mappen G:\<dagens dato> eksisterer, hvis den ikke eksisterer, opprettes den
# Kjører FULL BACKUP av alt sammen i dato-mappen HVER DAG
# Hvis mappen eksisterer, antas at backup er tatt og scriptet gjør ingenting
# https://docs.microsoft.com/en-us/powershell/scripting/learn/deep-dives/everything-about-if?view=powershell-7.2
if (-not(Test-Path -Path $checkdest -PathType Container)) {
    New-Item -Path $checkdest -ItemType Directory

    # Robocopy kopierer alle mapper og filer som er endret siden sist
    # Source er definert i toppen mens checkdest består av \\undervisning\files\sale\<dagensdato>
    # /e tar med alle mapper (også tomme) og alle filer. DATSOU vil si alt av properties for filene.
    # https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/robocopy
    Robocopy $source $checkdest /e /copy:DAT
}

# Sjekker om mappen G:\<dagens dato> eksisterer, hvis den IKKE eksisterer OG dagen ER Sunday, opprettes mappen og FULL backup tas
# Hvis mappen eksisterer, antas at backup er tatt og scriptet gjør ingenting
if (-not(Test-Path -Path $checkdest -PathType Container) -and ($checkday -eq "Sunday")) {
    New-Item -Path $checkdest -ItemType Directory

    Robocopy $source $checkdest /e /copy:DAT
}

# Sjekker om mappen G:\<dagens dato> eksisterer, hvis den IKKE eksisterer OG dagen IKKE er Sunday, opprettes mappen.
# Hvis mappen eksisterer, antas at backup er tatt og scriptet gjør ingenting
if (-not(Test-Path -Path $checkdest -PathType Container) -and (-not($checkday -eq "Sunday"))) {
    New-Item -Path $checkdest -ItemType Directory
    Robocopy $source $checkdest /e /im /copy:DAT /LOG:$logfile
}




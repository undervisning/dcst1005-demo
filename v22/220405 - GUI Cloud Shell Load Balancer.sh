group=rg-demo-vm
az group create -g $group -l norwayeast
username=azundervisning
password='mitThemmeliGePass0r123!'

az network vnet create \
  -n vnet-lb-demo-norwayeast \
  -g $group \
  -l norwayeast \
  --address-prefixes '10.0.0.0/16' \
  --subnet-name snet-lb-demo-norwayeast \
  --subnet-prefixes '10.0.1.0/24'

  
az vm availability-set create \
  -n vm-as-demo-norwayeast \
  -l norwayeast \
  -g $group

for NUM in 1 2 3
do
  az vm create \
    -n vm-demo-ne-0$NUM \
    -g $group \
    -l norwayeast \
    --size Standard_B1s \
    --image Win2019Datacenter \
    --admin-username $username \
    --admin-password $password \
    --vnet-name vnet-demo-norwayeast-001 \
    --subnet snet-demo-norwayeast-001 \
    --public-ip-address "" \
    --availability-set vm-as-demo-norwayeast \
	  --nsg nsg-vm-as-demo
done

for NUM in 1 2 3
do
  az vm open-port -g $group --name vm-demo-ne-0$NUM --port 80
done

for NUM in 1 2 3
do
  az vm extension set \
    --name CustomScriptExtension \
    --vm-name vm-demo-ne-0$NUM \
    -g $group \
    --publisher Microsoft.Compute \
    --version 1.8 \
    --settings '{"commandToExecute":"powershell Add-WindowsFeature Web-Server; powershell Add-Content -Path \"C:\\inetpub\\wwwroot\\Default.htm\" -Value $($env:computername)"}'
done

<#

#>


Connect-AzAccount -TenantID bd0944c8-c04e-466a-9729-d7086d13a653

<# Merk at om en ikke spesifisere Tenant ID må en spesifisere hvilken tenant en ønsker å jobbe med

Get-AzTenant
Get-AzTenant | Where-object {$_.name -like "Undervisning DIGSEC" }
$tenants = Get-AzTenant | Where-object {$_.name -like "Undervisning DIGSEC" }
$tenants
Set-AzContext -TenantId $tenants.Id
Get-AzContext
#>

# Variabler brukt videre i scriptet
$location = "norwayeast"
$rgss = "rg-demo-scaleset-$location"
# Variabler for Scale Set
$ssname = "vmss-webapp-001"
$ssvnetname = "vnet-vmss-tim-norwayeast"
$sssnetname = "snet-vmss-norwayeast-001"
$ssvnetaddressprefix = "10.40.0.0/19"
$sssnetaddressprefix = "10.40.1.0/24"
$ssvnetinfo = ""
$sssnetinfo = ""
$sspipname = "pip-vmss-webapp-001"
$sslb = "lb-vmss-webapp"
$nsgFrontendName = "nsg-vmss-webapp-001"


# Oppretter Resource Group
New-AzResourceGroup -ResourceGroupName $rgss -Location $location

# Oppretter Nettverk
$newvnet = @{
    Name = $ssvnetname
    AddressPrefix = $ssvnetaddressprefix
    ResourceGroupName = $rgss
    Location = $location
}
$ssvnetinfo = New-AzVirtualNetwork @newvnet

# Oppretter subnet
$ssvnetsssubnet = @{
    Name = $sssnetname
    AddressPrefix = $sssnetaddressprefix
    VirtualNetwork = $ssvnetinfo
}
$ssvnetsssubnetinfo = Add-AzVirtualNetworkSubnetConfig @ssvnetsssubnet
# Setter konfigurasjonen på VNET-et med nytt subnet
$ssvnetinfo = $ssvnetinfo | Set-AzVirtualNetwork

# Oppretter Scale Set 
New-AzVmss `
  -ResourceGroupName $rgss `
  -Location $location `
  -VMScaleSetName $ssname `
  -VirtualNetworkName $ssvnetname `
  -SubnetName $sssnetname `
  -PublicIpAddressName $sspipname `
  -LoadBalancerName $sslb `
  -UpgradePolicyMode "Automatic"


<#
Legger til script som skal kjøres på VM-ene i Scale Set-et
.ps1-fila inneholder:
Add-WindowsFeature Web-Server
Set-Content -Path "C:\inetpub\wwwroot\Default.htm" -Value "Hello World from host $($env:computername) !"
#> 
$publicSettings = @{
    "fileUris" = (,"https://raw.githubusercontent.com/Azure-Samples/compute-automation-configurations/master/automate-iis.ps1");
    "commandToExecute" = "powershell -ExecutionPolicy Unrestricted -File automate-iis.ps1"
}

# Henter informasjon fra VM Scale Set
$vmss = Get-AzVmss `
            -ResourceGroupName $rgss `
            -VMScaleSetName $ssname

# Install IIS og konfigurerer den med scriptet ovenfor
Add-AzVmssExtension -VirtualMachineScaleSet $vmss `
    -Name "customScript" `
    -Publisher "Microsoft.Compute" `
    -Type "CustomScriptExtension" `
    -TypeHandlerVersion 1.8 `
    -Setting $publicSettings

# Oppdaterer Scale set-et og VM-ene (Tar en stund, vurder -AsJob)
Update-AzVmss `
    -ResourceGroupName $rgss `
    -Name $ssname `
    -VirtualMachineScaleSet $vmss `
    -AsJob


# Henter ut informasjon om scale set-et
$vmss = Get-AzVmss `
            -ResourceGroupName $rgss `
            -VMScaleSetName $ssname

# Oppretter en NSG-regel for innkommende trafikk
$nsgFrontendRule = New-AzNetworkSecurityRuleConfig `
  -Name myFrontendNSGRule `
  -Protocol Tcp `
  -Direction Inbound `
  -Priority 200 `
  -SourceAddressPrefix * `
  -SourcePortRange * `
  -DestinationAddressPrefix * `
  -DestinationPortRange 80 `
  -Access Allow

# Oppretter NSG og hekter på nylig opprettet regel
$nsgFrontend = New-AzNetworkSecurityGroup `
  -ResourceGroupName  $rgss `
  -Location $location `
  -Name $nsgFrontendName `
  -SecurityRules $nsgFrontendRule

$vnetinfo = Get-AzVirtualNetwork `
  -ResourceGroupName  $rgss `
  -Name $ssvnetname

$frontendSubnet = $vnetinfo.Subnets[0]

$frontendSubnetConfig = Set-AzVirtualNetworkSubnetConfig `
  -VirtualNetwork $vnetinfo `
  -Name $sssnetname `
  -AddressPrefix $frontendSubnet.AddressPrefix `
  -NetworkSecurityGroup $nsgFrontend

Set-AzVirtualNetwork -VirtualNetwork $vnetinfo

# Henter informasjon fra VM Scale Set
$vmss = Get-AzVmss `
            -ResourceGroupName $rgss `
            -VMScaleSetName $ssname

Update-AzVmss `
    -ResourceGroupName $rgss `
    -Name $ssname `
    -VirtualMachineScaleSet $vmss

Get-AzPublicIpAddress -ResourceGroupName $rgss | Select IpAddress



################################################################################################
#                   Containers
################################################################################################
$port1 = New-AzContainerInstancePortObject -Port 8000 -Protocol TCP
$port2 = New-AzContainerInstancePortObject -Port 8001 -Protocol TCP
$container = New-AzContainerInstanceObject -Name test-container -Image nginx -RequestCpu 1 -RequestMemoryInGb 1.5 -Port @($port1, $port2)
$containerGroup = New-AzContainerGroup -ResourceGroupName myResourceGroup -Name test-cg -Location norwayeast -Container $container -OsType Linux -RestartPolicy "Never" -IpAddressType Public





################################################################################################
#                   Kubernetes Cluster
################################################################################################



$location = "norwayeast"
$rgaks = "rg-demo-aks-$location"
$aksName = "aks-demo-$location"

New-AzResourceGroup -Name $rgaks -Location $location

# Merk at det opprettes en Service Principal med Contributer, denne slettes ikke ved sletting av RG
New-AzAksCluster -ResourceGroupName $rgaks `
  -Name $aksName `
  -NodeCount 1

Install-AzAksKubectl
Import-AzAksCredential -ResourceGroupName $rgaks `
  -Name $aksName



kubectl get nodes
kubectl apply -f azure-vote.yaml

kubectl get service azure-vote-front --watch

# Finne verb
Get-Verb
# Finne kommandoer og hva de gjør:
Get-Command # viser alle tilgjengelige cmdlets. Filtrer listen for raskt å finne kommandoen en trenger.
# Finne ved bruk av Get-Command
Get-Command -Verb Get
Get-Command -Noun File*
Get-Command -Verb Get -Noun File*
Get-Help # cmdlet for å starte det innebygd hjelpesystem
Get-Member # Svaret er et objekt som kan inneholder mange egenskaper. Kjør Get-Member cmdleten for å se nærmere svaret
Get-Alias

Get-TimeZone | Get-Member
Get-TimeZone | Get-Member | Where-Object {$_.MemberType -eq 'Property'}

# Fiks keyboard layout på VM    
# Microsoft Docs: https://docs.microsoft.com/en-us/powershell/module/international/get-winuserlanguagelist?view=windowsserver2022-ps
Get-WinUserLanguageList
$languagelist = Get-WinUserLanguageList
$LanguageList.Add("nb")
Set-WinUserLanguageList $languagelist

# Pass på å ha riktig TimeZone
# Get-TimeZone Docs: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.management/get-timezone?view=powershell-7.2
Get-TimeZone
Get-TimeZone -ListAvailable
Get-TimeZone -ListAvailable | Where-Object {$_.BaseUtcOffset -eq '01:00:00'}
Get-TimeZone -ListAvailable | Where-Object {$_.Id -eq 'Central Europe Standard Time'}
# Lenke Comparison Operators: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_comparison_operators?view=powershell-7.2

# Set-Time Docs: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.management/set-timezone?view=powershell-7.2
Set-TimeZone -id 'Central Europe Standard Time'
ex
# Sett ExecutionPolicy
# ExecutionPolicy Docs: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_execution_policies?view=powershell-7.2
Set-ExecutionPolicy -ExecutionPolicy unrestricted -Scope LocalMachine

# Installere Choco 
# Hva er Choco: https://chocolatey.org/
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
choco upgrade chocolatey
# Installere programvare med Choco
choco install -y powershell-core
choco install -y git.install
choco install -y vscode
#choco install -y sysinternals

# Konfigurer Git
git config --global user.name "Tor Ivar Melling"
git config --global user.email "tor.i.melling@ntnu.no"


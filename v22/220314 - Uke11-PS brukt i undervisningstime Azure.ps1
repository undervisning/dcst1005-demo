# Kobler til Azure ved bruk av Az Module
# Install-Module -Name Az
# Install-module -Name AzureAD

# https://docs.microsoft.com/en-us/graph/powershell/installation
# Find-Module Microsoft.Graph*
# Install-Module Microsoft.Graph -Scope AllUsers


Connect-AzAccount

# Merk at om en ikke spesifisere Tenant ID må en spesifisere hvilken tenant en ønsker å jobbe med
Get-AzTenant
Get-AzTenant | Where-object {$_.name -like "Undervisning DIGSEC" }
$tenants = Get-AzTenant | Where-object {$_.name -like "Undervisning DIGSEC" }
$tenants
Set-AzContext -TenantId $tenants.Id
Get-AzContext
$Subscription = Get-AzSubscription -SubscriptionName "DCST1005 - Undervisning"

# https://docs.microsoft.com/en-us/powershell/module/az.resources/get-azaduser?view=azps-7.3.0
Get-AzADUser
# https://docs.microsoft.com/en-us/powershell/module/az.resources/new-azaduser?view=azps-7.3.0
New-AzADUser
# https://docs.microsoft.com/en-us/powershell/module/az.resources/remove-azaduser?view=azps-7.3.0
Remove-AzADUser

# https://docs.microsoft.com/en-us/powershell/module/az.resources/get-azadgroup?view=azps-7.3.0
Get-AzureADGroup
# https://docs.microsoft.com/en-us/powershell/module/az.resources/new-azadgroup?view=azps-7.3.0
New-AzureADGroup
# https://docs.microsoft.com/en-us/powershell/module/az.resources/remove-azadgroup?view=azps-7.3.0
Remove-AzADGroup

# Forslag til god navnekonvensjon i Azure: 
# https://docs.microsoft.com/en-us/azure/cloud-adoption-framework/ready/azure-best-practices/resource-naming 

# 
$rgname="rg-undervisning-demo-001"
$location="norwayeast"
$rg = New-AzResourceGroup -Name "rg-undervisning-demo-001" -Location "Norway East"
New-AzResourceGroup -Name $rgname -Location $location
# Remove-AzResourceGroup -Name $rg.ResourceGroupName
# Om en lurer på Location: Get-AzLocation

<# Opprette en storage account:
An Azure storage account contains all of your Azure Storage data objects, 
including blobs, file shares, queues, tables, and disks.
https://docs.microsoft.com/en-us/azure/storage/common/storage-account-create?tabs=azure-powershell
#> 
$storageaccount = New-AzStorageAccount -ResourceGroupName $rg.ResourceGroupName `
  -Name "sttim001" `
  -Location $rg.location `
  -SkuName Standard_RAGRS `
  -Kind StorageV2


# https://docs.microsoft.com/en-us/powershell/module/az.storage/new-azstorageshare?view=azps-7.3.0
$ctx = (Get-AzStorageAccount -ResourceGroupName $rg.ResourceGroupName -Name $storageaccount.StorageAccountName).Context  
$fileshare = New-AzStorageShare -Context $ctx -Name "fs-demo001-norwayeast"

# TAGS
# https://docs.microsoft.com/en-us/powershell/module/az.resources/new-aztag?view=azps-7.3.0
$Tags = @{"costcenter"="12345"; "owner"="tor.i.melling@ntnu.no"}
# Eksempel på å hente rg i norwayeast $rgid = Get-AzResourceGroup | Where-Object {$_.Location -eq "norwayeast"}
New-AzTag -Tag $Tags -ResourceId <SPESIFISER ID TIL RESSURS>
Set-AzResourceGroup -Name $rg.ResourceGroupName -Tag $tags


<#
Del 2 - Policy, Locks og Tags (RBAC tas etter at vi har fått inn flere brukere)
#>

# Policy
# https://docs.microsoft.com/en-us/powershell/module/az.resources/new-azpolicydefinition?view=azps-7.3.2



Get-AzPolicyDefinition -Builtin | select -ExpandProperty Properties | select Displayname
# https://docs.microsoft.com/en-us/azure/governance/policy/samples/built-in-policies



$Policy = Get-AzPolicyDefinition -BuiltIn | Where-Object {$_.Properties.DisplayName -eq 'Allowed locations'}
$Locations = Get-AzLocation | Where-Object displayname -like '*norway*'
$AllowedLocations = @{'listOfAllowedLocations'=($Locations.location)}
New-AzPolicyAssignment -Name 'RestrictLocationPolicyAssignment' -PolicyDefinition $Policy -Scope "/subscriptions/$($Subscription.Id)" -PolicyParameterObject $AllowedLocations

$Policy = Get-AzPolicyDefinition -BuiltIn | Where-Object {$_.Properties.DisplayName -eq 'Allowed virtual machine SKUs'}
$VMSKU = Get-AzVMImageSku | Where-Object displayname -like 'standard_d2s_v3'
$sku = "standard_d2s_v3" # , "standard_b2ms", "standard_ds1_v2", "standard_ds2_v2"
New-AzPolicyAssignment -Name 'RestrictVirtualMachineSKU' -PolicyDefinition $Policy -Scope "/subscriptions/$($Subscription.Id)" -PolicyParameterObject $sku


<# Management Groups
https://docs.microsoft.com/en-us/powershell/module/az.resources/new-azmanagementgroup?view=azps-7.3.2
https://docs.microsoft.com/en-us/powershell/module/az.resources/get-azmanagementgroup?view=azps-7.3.2
https://docs.microsoft.com/en-us/powershell/module/az.resources/remove-azmanagementgroup?view=azps-7.3.2
https://docs.microsoft.com/en-us/powershell/module/az.resources/update-azmanagementgroup?view=azps-7.3.2

Management groups are containers that help you manage access, policy, and compliance across multiple subscriptions.
Create these containers to build an effective and efficient hierarchy that can be used with Azure Policy and
Azure Role Based Access Controls
Navngivningforslag: mg-<business unit>[-<environment type>]
#>

# Ny Management groups
 New-AzManagementGroup -GroupID 'mg-it-tmp' -DisplayName 'MGMT IT TMP'
 Get-AzManagementGroup
 Remove-AzManagementGroup


# Lag hierarki 
$parentGroup = Get-AzManagementGroup -GroupID 'mg-it-tmp'
New-AzManagementGroup -GroupID 'mg-it-prod-tmp' -ParentId $parentGroup.id
New-AzManagementGroup -GroupID 'mg-it-dev-tmp' -ParentId $parentGroup.id
New-AzManagementGroup -GroupID 'mg-it-test-tmp' -ParentId $parentGroup.id

Get-AzManagementGroup
# MERK: Vi har kun opprettet MGMT Groups, det er ikke satt rettigheter eller lagt noen subscription i disse gruppene.

# Legg til subscription i MGMT Group
# For å finne Subscription ID: Get-AzSubscription
New-AzManagementGroupSubscription -GroupId 'mg-it-prod' -SubscriptionId '12345678-1234-1234-1234-123456789012'
Remove-AzManagementGroupSubscription -GroupId 'mg-it-prod' -SubscriptionId '12345678-1234-1234-1234-123456789012'

<# Tags
Get-AzTag - https://docs.microsoft.com/en-us/powershell/module/az.resources/get-aztag?view=azps-7.3.2
New-AzTag - https://docs.microsoft.com/en-us/powershell/module/az.resources/new-aztag?view=azps-7.3.2
Remove-AzTag - https://docs.microsoft.com/en-us/powershell/module/az.resources/remove-aztag?view=azps-7.3.2
Update-AzTag - https://docs.microsoft.com/en-us/powershell/module/az.resources/update-aztag?view=azps-7.3.2

Opprett tags og sett de på en ressurs. MERK at sammen kommando overskriver eksisterende tags.
#>
$rgtags = 'rg-demo-tag'
$location = 'norwayeast'
$Tags = @{"owner"="tor.i.melling@ntnu.no"; "costcenter"="1234"}
New-AzResourceGroup -Name $rgtags -Location $location
Set-AzResourceGroup -Name $rgtags -Tag $Tags

# Erstatt eller oppdater
#https://docs.microsoft.com/en-us/powershell/module/az.resources/update-aztag?view=azps-7.3.2
$Tags = @{"department"="it"}
$rgid = Get-AzResourceGroup -Name $rgtags
Update-AzTag -ResourceId $rgid.ResourceId -Tag $Tags -Operation Merge

Get-AzResourceGroup -Tag @{'owner'='tor.i.melling@ntnu.no'} | ft
Get-AzResource -Tag @{'owner'='tor.i.melling@ntnu.no'} | ft

$storageaccount = New-AzStorageAccount -ResourceGroupName $rgtags `
  -Name "sttim004" `
  -Location $rgid.location `
  -SkuName Standard_RAGRS `
  -Kind StorageV2

Set-AzStorageAccount -Name "sttim004" -ResourceGroupName $rgtags -Tag $tags
Update-AzTag -ResourceId $storageaccount.id -Tag $tags -Operation Merge

<# Azure locks
New-AzResourceLock - https://docs.microsoft.com/en-us/powershell/module/az.resources/new-azresourcelock?view=azps-7.3.2
Get-AzResourceLock - https://docs.microsoft.com/en-us/powershell/module/az.resources/get-azresourcelock?view=azps-7.3.2
Remove-AzResourceLock - https://docs.microsoft.com/en-us/powershell/module/az.resources/remove-azresourcelock?view=azps-7.3.2
Set-AzResourceLock - https://docs.microsoft.com/en-us/powershell/module/az.resources/set-azresourcelock?view=azps-7.3.2

#>

$rglocks = 'rg-demo-lock'
$location = 'norwayeast'
$rglockname = 'RG-Lock'

1..5 | foreach { 
    New-AzResourceGroup -Name $rglocks$_ -Location $location
}

$storageaccount = New-AzStorageAccount -ResourceGroupName "rg-demo-lock1" `
    -Name "sttim005" `
    -Location $location `
    -SkuName Standard_RAGRS `
    -Kind StorageV2

# -Locklevel Accepted values: CanNotDelete, ReadOnly
1..5 | foreach { 
    New-AzResourceLock -LockLevel CanNotDelete -LockName $rglockname -ResourceGroupName $rglocks$_
}

Get-AzResourceLock

# List ut basert på porarty ResourceName
Get-AzResourceLock
$getlock = Get-AzResourceLock | Where-Object -Property resourcename -eq 'RG-Lock'
$getlock.ResourceGroupName

$getlock | foreach {
    Remove-AzResourceLock -LockName 'RG-Lock' -ResourceGroupName $_.ResourceGroupName -Force
}

# Fjerne lock
Remove-AzResourceLock `
      -LockName 'RG-Lock' `
      -ResourceGroupName $rglocks



<# Create Users in Azure
New-AzADUser - https://docs.microsoft.com/en-us/powershell/module/az.resources/new-azaduser?view=azps-7.3.2
Get-AzADUser - https://docs.microsoft.com/en-us/powershell/module/az.resources/get-azaduser?view=azps-7.3.2
Update-AzADUser - https://docs.microsoft.com/en-us/powershell/module/az.resources/update-azaduser?view=azps-7.3.2
Remove-AzADUser - https://docs.microsoft.com/en-us/powershell/module/az.resources/remove-azaduser?view=azps-7.3.2
#> 
$newUsers = Import-Csv csv-files\my-users-azure.csv -Delimiter ";"

foreach ($user in $newUsers) {
    $usercheck=Get-AzADUser -UserPrincipalName $user.UserPrincipalName
    if (!$usercheck) {
        Write-Host "User $user.UserPrincipalName not found. Createing user $user.UserPrincipalName" -ForegroundColor Green
        $SecureStringPassword = ConvertTo-SecureString -String $user.Password -AsPlainText -Force
        New-AzADUser -DisplayName $user.DisplayName `
        -UserPrincipalName $user.UserPrincipalName `
        -Password $SecureStringPassword `
        -MailNickname $user.MailNickName
    }
    if ($usercheck) {
        Write-Host "User $user.UserPrincipalName found. User not created but will be updated with information from CSV" -ForegroundColor Green
        # Update-AzADUser
        # Her kan en velge om en skal oppdatere informasjon til allerede eksisterende brukere, eventuelt slette de
        # Remove-AzADUser -UserPrincipalName $user.UserPrincipalName -Confirm:$false
    }
}

<# Tildel rolle til bruker
New-AzRoleassignment - https://docs.microsoft.com/en-us/powershell/module/az.resources/new-azroleassignment?view=azps-7.3.2
Get-AzRoleassignment - https://docs.microsoft.com/en-us/powershell/module/az.resources/get-azroleassignment?view=azps-7.3.2
Remove-AzRoleassignment - https://docs.microsoft.com/en-us/powershell/module/az.resources/remove-azroleassignment?view=azps-7.3.2
#>
# For å finne rollene
$roledef = Get-AzRoleDefinition # Veldig veldig lang liste
Get-AzRoleDefinition | Where-Object -Property Name -eq 'Owner'
Get-AzRoleDefinition | Where-Object -Property Name -eq 'Reader'
Get-AzRoleDefinition | Where-Object -Property Name -like 'Monitor*'

$location = 'norwayeast'
$rgrbac = 'rg-demo-rbac'
$rbacgroup = 's_demogroup-rbac'
New-AzResourceGroup -Name $rgrbac -Location $location
$newgroup = new-azadgroup -DisplayName $rbacgroup -MailNickname $rbacgroup
$newgroup

$demouser = Get-AzADUser -UserPrincipalName 'Demo.Undervisning@digsec.onmicrosoft.com'

# Merk at hvis en ikke spesifisere hvor rollen skal settes på den under, vil den velge subscription:
New-AzRoleAssignment -ObjectId $newgroup.Id -RoleDefinitionName Contributor -ResourceGroupName $rgrbac
New-AzRoleAssignment -ResourceGroupName $rgrbac -SignInName $demouser.UserPrincipalName -RoleDefinitionName Reader


Add-AzADGroupMember -TargetGroupObjectId $newgroup.id -MemberUserPrincipalName $demouser.UserPrincipalName

$rolesrg = Get-AzRoleAssignment -ResourceGroupName $rgrbac | Select-Object DisplayName,RoleDefinitionName

$delrg | foreach {Remove-AzResourceGroup -ResourceGroupName $_.ResourceGroupName -Force -AsJob}
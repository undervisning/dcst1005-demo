Set-Disk -Number 1 -IsOffline $False # Online

$date = Get-date -UFormat "%Y%m%d" # for å navngi folder
$checkday = Get-date -UFormat "%A" # sjekke dag for å avgjøre om en skal kjøre full eller incrementell backup
$logfile = "G:\logs\backuplogfile-$date.txt"
$fullbackup = "-full"
$logdir = 'G:\logs\'
$source = '\\undervisning\files\sale\'
$destination = 'G:\'

# sjekker om destinasjon eksisterer, oppretter mappe og kjører backup hvis mappen ikke finnes fra før
$checkdest = -join($destination,$date,$fullbackup) # slår sammen G:\ og datoen
#Test-Path -Path $checkdest

# sjekk om logfolder eksisterer
if (-not(Test-Path -Path $logdir  -PathType Container)) {
    New-Item -Path $logdir  -ItemType Directory
}

# Sjekker om mappen G:\<dagens dato> eksisterer, hvis den IKKE eksisterer OG dagen ER Saturday, opprettes mappen og FULL backup tas
# Hvis mappen eksisterer, antas at backup er tatt og scriptet gjør ingenting
if (-not(Test-Path -Path $checkdest -PathType Container) -and ($checkday -eq "Saturday")) {
    New-Item -Path $checkdest -ItemType Directory

    Robocopy $source $checkdest /e /z /copy:DAT /r:5 /w:5 /LOG:$logfile
}



Set-Disk -Number 1 -IsOffline $True # Offline
Set-Disk -Number 1 -IsOffline $False # Online

$date = Get-date -UFormat "%Y%m%d" # for å navngi folder
$day = 'daily-week-'
$week = get-date -UFormat %V
$logfile = "G:\logs\backuplogfile-$date-$day$week.txt"
$logdir = 'G:\logs\'
$source = '\\undervisning\files\sale\'
$destination = 'G:\'

# sjekker om destinasjon eksisterer, oppretter mappe og kjører backup hvis mappen ikke finnes fra før
$checkdest = -join($destination,$day,$week) # slår sammen G:\ og datoen
#Test-Path -Path $checkdest

# sjekk om logfolder eksisterer
if (-not(Test-Path -Path $logdir  -PathType Container)) {
    New-Item -Path $logdir  -ItemType Directory
}

if (-not(Test-Path -Path $checkdest -PathType Container)) {
    New-Item -Path $checkdest -ItemType Directory
}

Robocopy $source $checkdest /e /z /im /copy:DAT /r:5 /w:5 /LOG:$logfile

Set-Disk -Number 1 -IsOffline $True # Offline
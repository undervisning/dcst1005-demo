
$lit_users = "LearnIT_Users"
$lit_groups = "LearnIT_Groups"
$lit_computers = "LearnIT_Computers"

$topOUs = @($lit_users,$lit_groups,$lit_computers )

foreach ($ou in $topOUs) {
    Get-ADOrganizationalUnit -Filter * | Where-Object {$_.name -eq $ou} | Remove-ADOrganizationalUnit -Recursive -Confirm:$false
}

<#  cleen up USERS
$OUPath = 'OU=LearnIT_Users,DC=core,DC=sec'
$deleteuser = Get-ADUser -Filter * -SearchBase $OUPath
foreach ($user in $deleteuser) {
    Remove-ADUser -Identity $user.SamAccountName -Confirm:$false
}
#>
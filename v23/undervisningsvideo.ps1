# https://gitlab.com/undervisning/dcst1005-demo/-/tree/master/v23

w

Get-Command -Module ActiveDirectory | Where-Object {$_.name -like "*user*"}
Get-Command -Module ActiveDirectory | Where-Object {$_.name -like "*group*"}
Get-Command -Module ActiveDirectory | Where-Object {$_.name -like "*OrganizationalUnit*"}

#############################################################################
#                                                                           #
#     I denne gjennomgangen skal vi gjøre følgende fra MGR:                 #
#       1. Opprette OU-struktur i AD for brukere, maskiner og grupper       #
#       2. Opprette grupper som skal representere en eksempelbedrift        #
#       3. Opprette brukere som skal:                                       #
#             - Plasseres i ønsket OU                                       #
#             - Meldes inn i tiltenkt gruppe for tilganger                  #
#             - Sørge for at de for logget seg inn på ønsket maskin (cl1)   #
#                                                                           #
#                                                                           #
#                                                                           #
#                                                                           #
#############################################################################

New-ADOrganizationalUnit "TestOU" `
            -Description "TEst for å sjekk at cmdlet fungerer" `
            -ProtectedFromAccidentalDeletion:$false
Get-ADOrganizationalUnit -Filter * | Select-Object name
Get-Help Remove-ADOrganizationalUnit -Online
Get-ADOrganizationalUnit -Filter * | Where-Object {$_.name -eq "TestOU"} | Remove-ADOrganizationalUnit -Recursive -Confirm:$false

# Eksempelbedrift - LearnIT
$lit_users = "LearnIT_Users"
$lit_groups = "LearnIT_Groups"
$lit_computers = "LearnIT_Computers"

$topOUs = @($lit_users,$lit_groups,$lit_computers )
$departments = @('hr','it','dev','sale','finance')

foreach ($ou in $topOUs) {
    New-ADOrganizationalUnit $ou -Description "Top OU for LearnIT" -ProtectedFromAccidentalDeletion:$false
    $topOU = Get-ADOrganizationalUnit -Filter * | Where-Object {$_.name -eq "$ou"}
    #Get-ADOrganizationalUnit -Filter * | Where-Object {$_.name -eq $ou} | Remove-ADOrganizationalUnit -Recursive -Confirm:$false
        foreach ($department in $departments) {
            New-ADOrganizationalUnit $department `
                        -Path $topOU.DistinguishedName `
                        -Description "Deparment OU for $department in topOU $topOU" `
                        -ProtectedFromAccidentalDeletion:$false
        }
}

# ----Group Structure----- #

Get-Help New-ADGroup -Online

New-ADGroup -Name "g_TestGruppe" `
            -SamAccountName g_TestGruppe `
            -GroupCategory Security `
            -GroupScope Global `
            -DisplayName "Test" `
            -Path "CN=Users,DC=core,DC=sec" `
            -Description "Test"

foreach ($department in $departments) {
    $path = Get-ADOrganizationalUnit -Filter * | 
            Where-Object {($_.name -eq "$department") `
            -and ($_.DistinguishedName -like "OU=$department,OU=$lit_groups,*")}
    New-ADGroup -Name "g_$department" `
            -SamAccountName "g_$department" `
            -GroupCategory Security `
            -GroupScope Global `
            -DisplayName "g_$department" `
            -Path $path.DistinguishedName `
            -Description "$department group"
}

New-ADGroup -name "g_all_employee" `
            -SamAccountName "g_all_employee" `
            -GroupCategory Security `
            -GroupScope Global `
            -DisplayName "g_all_employee" `
            -path "OU=LearnIT_Groups,DC=core,DC=sec" `
            -Description "all employee"


# ----Create Users ----- #

<#
The easy way or the "hard" way? (advanced - AD liker ikke æ,ø,å. Kanskje en også bør ha en navnestandard?#>
# Easy way - Brukere uten noen særnorske tegn og ingen vasking av input data #

Get-Help New-AdUSer -Online

$password = Read-Host -Prompt "EnterPassword" -AsSecureString
New-ADUser -Name "Hans Hansen" `
 -GivenName "Hans" `
 -Surname "Hansen" `
 -SamAccountName  "hhansen" `
 -UserPrincipalName  "hhansen@core.sec" `
 -Path "OU=IT,OU=LearnIT_Users,DC=core,DC=sec" `
 -AccountPassword $Password -Enabled $true


$users = Import-Csv -Path 'C:\projects\dcst1005-demo\v23\users.csv' -Delimiter ";"

foreach ($user in $users) {
    New-ADUser -Name $user.DisplayName `
    -GivenName $user.GivenName `
    -Surname $user.Surname `
    -SamAccountName  $user.username `
    -UserPrincipalName  $user.UserPrincipalName `
    -Path $user.path `
    -AccountPassword (convertto-securestring $user.password -AsPlainText -Force) `
    -Department $user.department `
    -Enabled $true
}

# Hard way - Brukere MED særnorske tegn og vasking/formatering av data, randomgenerering av passord #

$users = Import-Csv -Path 'C:\projects\dcst1005-demo\v23\users_advanced.csv' -Delimiter ";"
$csvfile = @()
$exportuserspath = 'C:\projects\dcst1005-demo\v23\users_final.csv'
$exportpathfinal = 'C:\projects\dcst1005-demo\v23\users_final-v2.csv'

Function New-UserPassword {
    $chars = [char[]](
        (33..47 | ForEach-Object {[char]$_}) +
        (58..64 | ForEach-Object {[char]$_}) +
        (91..96 | ForEach-Object {[char]$_}) +
        (123..126 | ForEach-Object {[char]$_}) +
        (48..57 | ForEach-Object {[char]$_}) +
        (65..90 | ForEach-Object {[char]$_}) +
        (97..122 | ForEach-Object {[char]$_})
    )

    -join (0..14 | ForEach-Object { $chars | Get-Random })
}


function New-UserInfo {
    param (
        [Parameter(Mandatory=$true)][string] $fornavn,
        [Parameter(Mandatory=$true)][string] $etternavn
    )

    if ($fornavn -match $([char]32)) {
        $oppdelt = $fornavn.Split($([char]32))
        $fornavn = $oppdelt[0]

        for ( $index = 1 ; $index -lt $oppdelt.Length ; $index ++ ) {
            $fornavn += ".$($oppdelt[$index][0])"
        }
    }

    $UserPrincipalName = $("$($fornavn).$($etternavn)").ToLower()
    $UserPrincipalName = $UserPrincipalName.Replace('æ','e')
    $UserPrincipalName = $UserPrincipalName.Replace('ø','o')
    $UserPrincipalName = $UserPrincipalName.Replace('å','a')
    $UserPrincipalName = $UserPrincipalName.Replace('é','e')

    Return $UserPrincipalName
}

foreach ($user in $users) {
    $password = New-UserPassword
    $line = New-Object -TypeName psobject

    Add-Member -InputObject $line -MemberType NoteProperty -Name GivenName -Value $User.GivenName
    Add-Member -InputObject $line -MemberType NoteProperty -Name SurName -Value $user.SurName
    Add-Member -InputObject $line -MemberType NoteProperty -Name UserPrincipalName -Value "$(New-UserInfo -Fornavn $user.GivenName -Etternavn $user.SurName)@core.sec"
    Add-Member -InputObject $line -MemberType NoteProperty -Name DisplayName -Value "$($user.GivenName) $($user.surname)" 
    Add-Member -InputObject $line -MemberType NoteProperty -Name department -Value $user.Department
    Add-Member -InputObject $line -MemberType NoteProperty -Name Password -Value $password
    $csvfile += $line
}

$csvfile | Export-Csv -Path $exportuserspath -NoTypeInformation -Encoding 'UTF8'
Import-Csv -Path $exportuserspath | ConvertTo-Csv -NoTypeInformation | ForEach-Object { $_ -Replace '"', ""} | Out-File $exportpathfinal -Encoding 'UTF8'


$users = Import-Csv -path 'C:\projects\dcst1005-demo\v23\users_final-v2.csv' -Delimiter ","

foreach ($user in $users) {
    $sam = $user.UserPrincipalName.Split("@")
        if ($sam[0].Length -gt 19) {
            "SAM for lang, bruker de 19 første tegnene i variabelen"
            $sam[0] = $sam[0].Substring(0, 19) 
        }
        $sam[0]
        [string] $samaccountname = $sam[0]

        [string] $department = $user.Department
        [string] $searchdn = "OU=$department,OU=$lit_users,*"
        $path = Get-ADOrganizationalUnit -Filter * | Where-Object {($_.name -eq $user.Department) -and ($_.DistinguishedName -like $searchdn)} 
        
        New-ADUser `
            -SamAccountName $samaccountname `
            -UserPrincipalName $user.UserPrincipalName `
            -Name $samaccountname `
            -GivenName $user.GivenName `
            -Surname $user.SurName `
            -Enabled $True `
            -ChangePasswordAtLogon $false `
            -DisplayName $user.Displayname `
            -Department $user.Department `
            -Path $path `
            -AccountPassword (convertto-securestring $user.Password -AsPlainText -Force)

    }


$ADUsers = @()

foreach ($department in $departments) {
    $ADUsers = Get-ADUser -Filter {Department -eq $department} -Properties Department
    #Write-Host "$ADUsers som er funnet under $department"

    foreach ($aduser in $ADUsers) {
        Add-ADPrincipalGroupMembership -Identity $aduser.SamAccountName -MemberOf "g_$department"
    }

}










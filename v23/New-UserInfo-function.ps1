<#This is a PowerShell function named New-UserInfo that takes two mandatory parameters as inputs,
$fornavn and $etternavn (which represent first name and last name respectively).

The function starts by checking if the first name $fornavn contains a space.
If so, it splits the string into an array of substrings using .Split method and separates them by a dot.

Then, it creates a string called $UserPrincipalName by concatenating first name and last name,
converting it to lower case and replacing special characters like æ, ø, å and é with their closest English equivalent.

Finally, the function returns the $UserPrincipalName.

The second part of the code uses a loop to iterate over an array of objects named $users.
For each iteration, the function calls New-UserPassword to generate a password,
creates a new PSObject named $line and adds properties to it using Add-Member cmdlet. The properties include:
-GivenName
-SurName
-UserPrincipalName (which is the result of calling New-UserInfo)
-DisplayName
-Department
-Password
The loop collects all the $line objects in an array called $csvfile which could be exported to a csv file later.
#>

function New-UserInfo {
    param (
        [Parameter(Mandatory=$true)][string] $fornavn,
        [Parameter(Mandatory=$true)][string] $etternavn
    )

    if ($fornavn -match $([char]32)) {
        $oppdelt = $fornavn.Split($([char]32))
        $fornavn = $oppdelt[0]

        for ( $index = 1 ; $index -lt $oppdelt.Length ; $index ++ ) {
            $fornavn += ".$($oppdelt[$index][0])"
        }
    }

    $UserPrincipalName = $("$($fornavn).$($etternavn)").ToLower()
    $UserPrincipalName = $UserPrincipalName.Replace('æ','e')
    $UserPrincipalName = $UserPrincipalName.Replace('ø','o')
    $UserPrincipalName = $UserPrincipalName.Replace('å','a')
    $UserPrincipalName = $UserPrincipalName.Replace('é','e')

    Return $UserPrincipalName
}

foreach ($user in $users) {
    $password = New-UserPassword
    $line = New-Object -TypeName psobject

    Add-Member -InputObject $line -MemberType NoteProperty -Name GivenName -Value $User.GivenName
    Add-Member -InputObject $line -MemberType NoteProperty -Name SurName -Value $user.SurName
    Add-Member -InputObject $line -MemberType NoteProperty -Name UserPrincipalName -Value "$(New-UserInfo -Fornavn $user.GivenName -Etternavn $user.SurName)@core.sec"
    Add-Member -InputObject $line -MemberType NoteProperty -Name DisplayName -Value "$($user.GivenName) $($user.surname)" 
    Add-Member -InputObject $line -MemberType NoteProperty -Name department -Value $user.Department
    Add-Member -InputObject $line -MemberType NoteProperty -Name Password -Value $password
    $csvfile += $line
}

$csvfile | Export-Csv -Path $exportuserspath -NoTypeInformation -Encoding 'UTF8'
Import-Csv -Path $exportuserspath | ConvertTo-Csv -NoTypeInformation | ForEach-Object { $_ -Replace '"', ""} | Out-File $exportpathfinal -Encoding 'UTF8'


Install-module -Name Az
Install-Module Microsoft.Graph
Install-Module Microsoft.Graph -AllowPrerelease -force

Connect-AzAccount
Get-AzContext

Get-AzTenant
Set-AzContext -TenantId 'bd0944c8-c04e-466a-9729-d7086d13a653'

$rgname="rg-undervisning-demo-001"
$location="westeurope"

New-AzResourceGroup -Name $rgname -Location $location
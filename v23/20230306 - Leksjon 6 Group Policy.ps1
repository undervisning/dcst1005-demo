<# 

Porter for remote Group Policy Update
https://learn.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2012-r2-and-2012/jj572986(v=ws.11)

#>

# Firewall rules
Get-NetFirewallRule -DisplayName "Remote Scheduled Tasks Management (RPC)" | 
                    Select-Object displayname,name,profile
Get-NetFirewallRule | Where-Object {($_.DisplayName -eq "Remote Scheduled Tasks Management (RPC)") -and ($_.Profile -eq "Domain")}

Get-NetFirewallRule -DisplayName "Windows Management Instrumentation (WMI-in)" | 
                    Select-Object displayname,name,profile

Get-NetFirewallRule -DisplayName "Remote Scheduled Tasks Management (RPC-EPMAP)"| 
                    Select-Object displayname,name,profile

<# Firewall names:
RemoteTask-In-TCP-NoScope
WMI-WINMGMT-In-TCP-NoScope
RemoteTask-RPCSS-In-TCP-NoScope
#>

Enable-NetFirewallRule -Name "RemoteTask-In-TCP-NoScope","WMI-WINMGMT-In-TCP-NoScope","RemoteTask-RPCSS-In-TCP-NoScope"

Invoke-Command -ComputerName "srv1" `
    -ScriptBlock {Enable-NetFirewallRule -Name "RemoteTask-In-TCP","WMI-WINMGMT-In-TCP","RemoteTask-RPCSS-In-TCP"}

Invoke-Command -ComputerName "cl1", "cl2", "cl3" `
    -ScriptBlock {Enable-NetFirewallRule -Name "RemoteTask-In-TCP-NoScope","WMI-WINMGMT-In-TCP-NoScope","RemoteTask-RPCSS-In-TCP-NoScope"}

Invoke-Command -ComputerName "cl1", "cl2", "cl3", "srv1" `
    -ScriptBlock {gpupdate /force}

$vms =@('cl1','cl2','cl3','srv1')
foreach ($vm in $vms) {
    Invoke-GPUpdate -Computer "$vm.core.sec" -Target "User" -RandomDelayInMinutes 0 -Force
} 

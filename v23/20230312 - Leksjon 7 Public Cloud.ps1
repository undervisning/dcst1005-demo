# Kobler til Azure ved bruk av Az Module
# Install-module -Name Az

# https://docs.microsoft.com/en-us/graph/powershell/installation
# Find-Module Microsoft.Graph*
# Install-Module Microsoft.Graph -Scope AllUsers


Connect-AzAccount

# Merk at om en ikke spesifisere Tenant ID må en spesifisere hvilken tenant en ønsker å jobbe med
Get-AzTenant
Get-AzTenant | Where-object {$_.name -like "Undervisning DIGSEC" }
$tenants = Get-AzTenant | Where-object {$_.name -like "Undervisning DIGSEC" }
$tenants
Set-AzContext -TenantId $tenants.Id
Get-AzContext
$Subscription = Get-AzSubscription -SubscriptionName "DCST1005 - Undervisning"

# https://docs.microsoft.com/en-us/powershell/module/az.resources/get-azaduser?view=azps-7.3.0
Get-AzADUser
# https://docs.microsoft.com/en-us/powershell/module/az.resources/new-azaduser?view=azps-7.3.0
New-AzADUser
# https://docs.microsoft.com/en-us/powershell/module/az.resources/remove-azaduser?view=azps-7.3.0
Remove-AzADUser

# https://docs.microsoft.com/en-us/powershell/module/az.resources/get-azadgroup?view=azps-7.3.0
Get-AzureADGroup
# https://docs.microsoft.com/en-us/powershell/module/az.resources/new-azadgroup?view=azps-7.3.0
New-AzureADGroup
# https://docs.microsoft.com/en-us/powershell/module/az.resources/remove-azadgroup?view=azps-7.3.0
Remove-AzADGroup

# Forslag til god navnekonvensjon i Azure: 
# https://docs.microsoft.com/en-us/azure/cloud-adoption-framework/ready/azure-best-practices/resource-naming 

# 
$rgname="rg-undervisning-demo-002"
$location="westeurope"
$rg = New-AzResourceGroup -Name "rg-undervisning-demo-002" -Location "westeurope"
New-AzResourceGroup -Name $rgname -Location $location

# Splatting: Splatting is a method of passing a collection of parameter values to a command as a unit
$newrg = @{
  Name = $rgname
  Location = $location
}
New-AzResourceGroup @newrg


# Remove-AzResourceGroup -Name $rg.ResourceGroupName -Confirm:$false -force
# Om en lurer på Location: Get-AzLocation



<# Opprette en storage account:
An Azure storage account contains all of your Azure Storage data objects, 
including blobs, file shares, queues, tables, and disks.
https://docs.microsoft.com/en-us/azure/storage/common/storage-account-create?tabs=azure-powershell
#> 
$storageaccount = New-AzStorageAccount -ResourceGroupName $rg.ResourceGroupName `
  -Name "sttim00234" `
  -Location $rg.location `
  -SkuName Standard_RAGRS `
  -Kind StorageV2


# https://docs.microsoft.com/en-us/powershell/module/az.storage/new-azstorageshare?view=azps-7.3.0
$ctx = (Get-AzStorageAccount -ResourceGroupName $rg.ResourceGroupName -Name $storageaccount.StorageAccountName).Context  
$fileshare = New-AzStorageShare -Context $ctx -Name "fs-demo001-norwayeast"

# Storage Connection String (finnes også i Portal sammen med script for å connecte)
$saKey = (Get-AzStorageAccountKey -ResourceGroupName $rg.ResourceGroupName -Name $storageaccount.StorageAccountName)[0].Value

# TAGS
# https://docs.microsoft.com/en-us/powershell/module/az.resources/new-aztag?view=azps-7.3.0
$Tags = @{"costcenter"="12345"; "owner"="tor.i.melling@ntnu.no"}
# Eksempel på å hente rg i westeurope $rgid = Get-AzResourceGroup | Where-Object {$_.Location -eq "westeurope"}
New-AzTag -Tag $Tags -ResourceId $rg.ResourceId
Set-AzResourceGroup -Name $rg.ResourceGroupName -Tag $tags


<#
Del 2 - Policy, Locks og Tags (RBAC tas etter at vi har fått inn flere brukere)
#>

# Policy - MERK: At om en legger en policy på subscription vil det påvirke alle sammen som skal opprette
# ressurser under denne subscription. Det kan også opprettes policyer på lavere nivå, eksempelvis Resource Groups
# https://docs.microsoft.com/en-us/powershell/module/az.resources/new-azpolicydefinition?view=azps-7.3.2



Get-AzPolicyDefinition -Builtin | Select-Object -ExpandProperty Properties | Select-Object Displayname
# https://docs.microsoft.com/en-us/azure/governance/policy/samples/built-in-policies

$Policy = Get-AzPolicyDefinition -BuiltIn | Where-Object {$_.Properties.DisplayName -eq 'Allowed locations'}
$Locations = Get-AzLocation | Where-Object displayname -like '*europe*'
$AllowedLocations = @{'listOfAllowedLocations'=($Locations.location)}
New-AzPolicyAssignment -Name 'RestrictLocationPolicyAssignment' -PolicyDefinition $Policy -Scope "/subscriptions/$($Subscription.Id)" -PolicyParameterObject $AllowedLocations

$Policy = Get-AzPolicyDefinition -BuiltIn | Where-Object {$_.Properties.DisplayName -eq 'Allowed virtual machine SKUs'}
$VMSKU = Get-AzVMImageSku | Where-Object displayname -like 'standard_d2s_v3'
$sku = "standard_d2s_v3" # , "standard_b2ms", "standard_ds1_v2", "standard_ds2_v2"
New-AzPolicyAssignment -Name 'RestrictVirtualMachineSKU' -PolicyDefinition $Policy -Scope "/subscriptions/$($Subscription.Id)" -PolicyParameterObject $sku


<# Management Groups
https://docs.microsoft.com/en-us/powershell/module/az.resources/new-azmanagementgroup?view=azps-7.3.2
https://docs.microsoft.com/en-us/powershell/module/az.resources/get-azmanagementgroup?view=azps-7.3.2
https://docs.microsoft.com/en-us/powershell/module/az.resources/remove-azmanagementgroup?view=azps-7.3.2
https://docs.microsoft.com/en-us/powershell/module/az.resources/update-azmanagementgroup?view=azps-7.3.2

Management groups are containers that help you manage access, policy, and compliance across multiple subscriptions.
Create these containers to build an effective and efficient hierarchy that can be used with Azure Policy and
Azure Role Based Access Controls
Navngivningforslag: mg-<business unit>[-<environment type>]
#>

# Ny Management groups
 New-AzManagementGroup -GroupID 'mg-it-tmp' -DisplayName 'MGMT IT TMP'
 Get-AzManagementGroup
 Remove-AzManagementGroup


# Lag hierarki 
$parentGroup = Get-AzManagementGroup -GroupID 'mg-it-tmp'
New-AzManagementGroup -GroupID 'mg-it-prod-tmp' -ParentId $parentGroup.id
New-AzManagementGroup -GroupID 'mg-it-dev-tmp' -ParentId $parentGroup.id
New-AzManagementGroup -GroupID 'mg-it-test-tmp' -ParentId $parentGroup.id

Get-AzManagementGroup
# MERK: Vi har kun opprettet MGMT Groups, det er ikke satt rettigheter eller lagt noen subscription i disse gruppene.


# IKKE GJØR :) - Dette er aktuelt om en har mange subscriptions hvor kostnadene skal f.eks. fordeles på forskjellige
# avdelinger eller lignende avhengig av hvem tjenestene tjenestegjør.
# Legg til subscription i MGMT Group 
# For å finne Subscription ID: Get-AzSubscription
New-AzManagementGroupSubscription -GroupId 'mg-it-prod' -SubscriptionId '12345678-1234-1234-1234-123456789012'
Remove-AzManagementGroupSubscription -GroupId 'mg-it-prod' -SubscriptionId '12345678-1234-1234-1234-123456789012'

<# Tags
Get-AzTag - https://docs.microsoft.com/en-us/powershell/module/az.resources/get-aztag?view=azps-7.3.2
New-AzTag - https://docs.microsoft.com/en-us/powershell/module/az.resources/new-aztag?view=azps-7.3.2
Remove-AzTag - https://docs.microsoft.com/en-us/powershell/module/az.resources/remove-aztag?view=azps-7.3.2
Update-AzTag - https://docs.microsoft.com/en-us/powershell/module/az.resources/update-aztag?view=azps-7.3.2

Opprett tags og sett de på en ressurs. MERK at sammen kommando overskriver eksisterende tags.
#>
$rgtags = 'rg-demo-tag'
$location = 'norwayeast'
$Tags = @{"owner"="tor.i.melling@ntnu.no"; "costcenter"="1234"}
New-AzResourceGroup -Name $rgtags -Location $location
Set-AzResourceGroup -Name $rgtags -Tag $Tags

# Erstatt eller oppdater
#https://docs.microsoft.com/en-us/powershell/module/az.resources/update-aztag?view=azps-7.3.2
$Tags = @{"department"="it"}
$rgid = Get-AzResourceGroup -Name $rgtags
Update-AzTag -ResourceId $rgid.ResourceId -Tag $Tags -Operation Merge

Get-AzResourceGroup -Tag @{'owner'='tor.i.melling@ntnu.no'} | ft
Get-AzResource -Tag @{'owner'='tor.i.melling@ntnu.no'} | ft

$storageaccount = New-AzStorageAccount -ResourceGroupName $rgtags `
  -Name "sttim004" `
  -Location $rgid.location `
  -SkuName Standard_RAGRS `
  -Kind StorageV2

Set-AzStorageAccount -Name "sttim004" -ResourceGroupName $rgtags -Tag $tags
Update-AzTag -ResourceId $storageaccount.id -Tag $tags -Operation Merge

<# Azure locks
New-AzResourceLock - https://docs.microsoft.com/en-us/powershell/module/az.resources/new-azresourcelock?view=azps-7.3.2
Get-AzResourceLock - https://docs.microsoft.com/en-us/powershell/module/az.resources/get-azresourcelock?view=azps-7.3.2
Remove-AzResourceLock - https://docs.microsoft.com/en-us/powershell/module/az.resources/remove-azresourcelock?view=azps-7.3.2
Set-AzResourceLock - https://docs.microsoft.com/en-us/powershell/module/az.resources/set-azresourcelock?view=azps-7.3.2

#>

$rglocks = 'rg-demo-lock'
$location = 'westeurope'
$rglockname = 'RG-Lock'

# løkke som lopper igjennom og $_ representerer tallene fra 1 til 5
1..5 | foreach { 
    New-AzResourceGroup -Name $rglocks$_ -Location $location
}

$storageaccount = New-AzStorageAccount -ResourceGroupName "rg-demo-lock1" `
    -Name "sttim005" `
    -Location $location `
    -SkuName Standard_RAGRS `
    -Kind StorageV2

# -Locklevel Accepted values: CanNotDelete, ReadOnly
1..5 | foreach { 
    New-AzResourceLock -LockLevel CanNotDelete -LockName $rglockname -ResourceGroupName $rglocks$_
}

Get-AzResourceLock

# List ut basert på porarty ResourceName
Get-AzResourceLock
$getlock = Get-AzResourceLock | Where-Object -Property resourcename -eq 'RG-Lock'
$getlock.ResourceGroupName

$getlock | foreach {
    Remove-AzResourceLock -LockName 'RG-Lock' -ResourceGroupName $_.ResourceGroupName -Force
}

# Fjerne lock
Remove-AzResourceLock `
      -LockName 'RG-Lock' `
      -ResourceGroupName $rglocks



#############################################################################
#                     Microsoft Graph - 2.0 Preview                         #
#############################################################################
Install-Module -Name Microsoft.Graph
Install-Module -Name Microsoft.Graph -AllowPrerelease -force
# FOR WINDOWS: Install-Module -Name Microsoft.Graph -AllowPrerelease -force -AllowClobber
# For Windows: HUSK å ha PowerShell Core (PowerShell 7) installert.
# bruk gjerne choco som tidligere vist bare mot lokal maskin i stedet for RDP mot maskin i OpenStack
# Restart VS Code / PowerShell

$secretid = ''
$clientid = ''
$tenantID = ''

# Krever 2.0 Preview, v1.x støtter ikke bruka av secret.
$cred = Get-Credential
Connect-MgGraph -TenantId $tenantID -ClientSecretCredential $cred
$primarydomain = (Get-MgDomain).ID

#-Scopes "User.ReadWrite.All","Group.ReadWrite.All","Directory.ReadWrite.All","RoleManagement.ReadWrite.Directory"


# Add environment variables to be used by Connect-MgGraph.
$Env:AZURE_CLIENT_ID = ""
$Env:AZURE_TENANT_ID = ""
$Env:AZURE_CLIENT_SECRET = ""

# Tell Connect-MgGraph to use your environment variables.
Connect-MgGraph -EnvironmentVariable


<#  Ved bruk av følgende kommando: Connect-MgGraph -Scopes kan en velge å legge til hvilke rettigheter
    en ønsker at tilkoblingen skal få. Denne metoden åpner en nettleser hvor en autentiserer seg mot ønsket
    tenant. Merk at dette må også godkjennes / godtas i nettleseren ved pålogging.
    Connect-MgGraph -Scopes "User.ReadWrite.All","Group.ReadWrite.All","Directory.ReadWrite.All","RoleManagement.ReadWrite.Directory"
    Vi benytter oss av App Registration og allerede tildelt rettigheter siden vår ntnu-bruker er invitert inn.
#>


# FUNKSJONER VI BENYTTER OSS AV, KJENT FRA ACTIVE DIRECTORY GJENNOMGANGEN #
#Funksjon for å formatere csv-fil data til ønsket og godtatt format

Function New-UserPassword {
  $chars = [char[]](
      (33..47 | ForEach-Object {[char]$_}) +
      (58..64 | ForEach-Object {[char]$_}) +
      (91..96 | ForEach-Object {[char]$_}) +
      (123..126 | ForEach-Object {[char]$_}) +
      (48..57 | ForEach-Object {[char]$_}) +
      (65..90 | ForEach-Object {[char]$_}) +
      (97..122 | ForEach-Object {[char]$_})
  )

  -join (0..14 | ForEach-Object { $chars | Get-Random })
}

function New-UserInfo {
  param (
      [Parameter(Mandatory=$true)][string] $fornavn,
      [Parameter(Mandatory=$true)][string] $etternavn
  )

  if ($fornavn -match $([char]32)) {
      $oppdelt = $fornavn.Split($([char]32))
      $fornavn = $oppdelt[0]

      for ( $index = 1 ; $index -lt $oppdelt.Length ; $index ++ ) {
          $fornavn += ".$($oppdelt[$index][0])"
      }
  }

  $UserPrincipalName = $("$($fornavn).$($etternavn)").ToLower()
  $UserPrincipalName = $UserPrincipalName.Replace('æ','e')
  $UserPrincipalName = $UserPrincipalName.Replace('ø','o')
  $UserPrincipalName = $UserPrincipalName.Replace('å','a')
  $UserPrincipalName = $UserPrincipalName.Replace('é','e')

  Return $UserPrincipalName
}



$csvfile =@()
$successUsers =@()
$existsUsers =@()

[string] $importusers = 'dcst1005-demo/v23/users_advanced.csv'
[string] $newformatusers = 'dcst1005-demo/v23/new-format-users.csv'
[string] $newformatusersfinal = 'dcst1005-demo/v23/new-format-users-final.csv'
[string] $useradded = 'dcst1005-demo/v23/users_added.csv'
[string] $userexists = 'dcst1005-demo/v23/users_exists.csv'


# Formaterer .csv-fil til ønsket format
$users = Import-Csv -Path $importusers -Delimiter ";"

foreach ($user in $users) {
  $password = New-UserPassword
  $line = New-Object -TypeName psobject

  Add-Member -InputObject $line -MemberType NoteProperty -Name GivenName -Value $User.GivenName
  Add-Member -InputObject $line -MemberType NoteProperty -Name SurName -Value $user.SurName
  Add-Member -InputObject $line -MemberType NoteProperty -Name UserPrincipalName `
             -Value "$(New-UserInfo -Fornavn $user.GivenName -Etternavn $user.SurName)$primarydomain"
  Add-Member -InputObject $line -MemberType NoteProperty -Name DisplayName -Value "$($user.GivenName) $($user.surname)" 
  Add-Member -InputObject $line -MemberType NoteProperty -Name department -Value $user.Department
  Add-Member -InputObject $line -MemberType NoteProperty -Name Password -Value $password
  $csvfile += $line
}
# Exporterer .csv-fil
$csvfile | Export-Csv -Path $newformatusers -NoTypeInformation -Encoding 'UTF8'
Import-Csv -Path $newformatusers | ConvertTo-Csv -NoTypeInformation |
          ForEach-Object { $_ -Replace '"', ""} | Out-File $newformatusersfinal -Encoding 'UTF8'

# Hvis en ønsker å finne ut hvilke rettigheter en trenger
Find-MgGraphCommand -command New-MgUser | Select-Object -First 1 -ExpandProperty Permissions

# Importerer opprettet .csv-fil for brukerimport
$users = Import-Csv -path $newformatusersfinal -Delimiter ","

foreach ($user in $users) {
  Write-Host ""
  Write-Host "###############################"
  Write-Host "User info:" -ForegroundColor Green
  Write-Host $user.GivenName -ForegroundColor Green
  Write-Host $user.SurName -ForegroundColor Green
  Write-Host $user.DisplayName -ForegroundColor Green
  Write-Host $user.UserPrincipalName -ForegroundColor Green
  Write-Host $user.Password -ForegroundColor Green
  Write-Host $user.department -ForegroundColor Green
  Write-Host "###############################"
  Write-Host ""
  $mailnickname = $user.UserPrincipalName -split '@'
  $userupn = $User.UserPrincipalName
  $userupn = $userupn.ToString()
  $PasswordProfile = @{
  Password = $user.Password
  }

  # Sjekker om en bruker med samme UPN eksisterer fra før  
  $mgraphuser = Get-MgUser -All | Where-Object {$_.UserPrincipalName -eq "$userupn"}
    if($mgraphuser){ Write-Host "User: $userupn was found - NOT CREATED" -ForegroundColor Yellow
      $existsline = New-Object -TypeName PSObject
      Add-Member -InputObject $existsline -MemberType NoteProperty -Name GivenName -Value $user.GivenName
      Add-Member -InputObject $existsline -MemberType NoteProperty -Name SurName -Value $user.Surname
      Add-Member -InputObject $existsline -MemberType NoteProperty -Name UserPrincipalName -Value $user.UserPrincipalName
      Add-Member -InputObject $existsline -MemberType NoteProperty -Name DisplayName -Value "$($user.GivenName) $($user.Surname)"
      
      # Legger til eksisterende bruker i et array for å få oversikt over hvilke brukere som fantes fra før
      $existsUsers += $existsline
    }
      # Oppretter ny bruker om brukeren ikke eksisterer
    else{ Write-Host "User $userupn was not found - USER WILL BE CREATED" -ForegroundColor Green
      New-MgUser `
        -GivenName $user.GivenName `
        -SurName $user.Surname `
        -DisplayName $user.DisplayName `
        -UserPrincipalName $user.UserPrincipalName `
        -PasswordProfile $PasswordProfile `
        -MailNickname $mailnickname[0] `
        -Department $user.department `
        -AccountEnabled

        $line = New-Object -TypeName PSObject
        Add-Member -InputObject $line -MemberType NoteProperty -Name GivenName -Value $user.GivenName
        Add-Member -InputObject $line -MemberType NoteProperty -Name SurName -Value $user.Surname
        Add-Member -InputObject $line -MemberType NoteProperty -Name UserPrincipalName -Value $user.UserPrincipalName
        Add-Member -InputObject $line -MemberType NoteProperty -Name DisplayName -Value "$($user.GivenName) $($user.Surname)"

        # Legger til opprettet bruker i et array for å få en enkel oversikt over hvilke brukere som ble lagt
        $successUsers += $line
    }
}

# Eksporterer array til .CSV-fil for oppbevaring.
$successUsers | Export-Csv -Path $useradded -NoTypeInformation -Encoding 'UTF8'
$existsUsers | Export-Csv -Path $userexists -NoTypeInformation -Encoding 'UTF8'


#############################################################################
#                  Opprett grupper og legg til i grupper                    #
#############################################################################
# Hvis en ønsker å finne ut hvilke rettigheter en trenger
Find-MgGraphCommand -command New-MgGroup | Select-Object -First 1 -ExpandProperty Permissions

# Oppretter gruppe
$newgroups =@("hr","it","dev","finance","sale")
foreach ($group in $newgroups) {
  New-MgGroup -DisplayName "$group" `
              -MailEnabled:$False  `
              -MailNickName 'testgroup' `
              -SecurityEnabled
}


# Legg til bruker med department-propertien satt
$newgroups =@("hr","it","dev","finance","sale")
foreach ($group in $newgroups) {
  $GroupID = (Get-MgGroup | Where-Object {$_.DisplayName -eq $group}).id
  $usersid = (Get-MgUser -Property Department,UserPrincipalName,ID |
              Where-Object {$_.Department -eq $group} | 
              Select-Object UserPrincipalName,Department,ID).id
    foreach ($ID in $usersID) {
      New-MgGroupMember -GroupId $GroupID -DirectoryObjectId $ID
    }
}

# Sjekker gruppeinnholdet
$newgroups =@("hr","it","dev","finance","sale")
foreach ($group in $newgroups) {
  $GroupID = (Get-MgGroup | Where-Object {$_.DisplayName -eq $group}).id
  $groupusers = (Get-MgGroupMember -GroupId $GroupID).id

  Get-MgUser -Property department,displayname,userprincipalname `
              -UserId $groupusers | Select-Object DisplayName,UserPrincipalName,Department
}


#############################################################################
#                  Tildele rettigheter / roller                             #
#############################################################################
# Eksempel på hvordan en kan tildele en bruker en rolle i Azure AD

<#

Kommer

#>

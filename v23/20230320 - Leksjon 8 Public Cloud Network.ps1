# Kobler til Azure ved bruk av Az Module
# Install-Module -Name Az
# Install-module -Name AzureAD <-- brukes mindre og mindre (deprecated)

Connect-AzAccount

# Merk at om en ikke spesifisere Tenant ID må en spesifisere hvilken tenant en ønsker å jobbe med
Get-AzTenant
Get-AzTenant | Where-object {$_.name -like "Undervisning DIGSEC" }
$tenants = Get-AzTenant | Where-object {$_.name -like "Undervisning DIGSEC" }
$tenants
Set-AzContext -TenantId $tenants.Id
Get-AzContext
#$Subscription = Get-AzSubscription -SubscriptionName "DCST1005 - Undervisning"

################################################################################################
# Variabler brukt for oppretting av ResourceGroup VNET og subnett i scriptet

# For alle:
$initials = "tim"
$location = "westeurope"
$rgvnet = "rg-vnet-$location-$initials"

# VNET 1
$vnet1 = "vnet001"
$vnet1name = "vnet-$initials-$location-001"
$vnet1AddressPrefix = "10.10.0.0/19"
$vnet1subnet1name = "snet-$vnet1-$location-001"
$vnet1subnet1AdressPrefix = "10.10.1.0/24"
$vnet1subnet2name = "snet-$vnet1-$location-002"
$vnet1subnet2AdressPrefix = "10.10.2.0/24"

# VNET 2
$vnet2 = "vnet002"
$vnet2name = "vnet-$initials-$location-002"
$vnet2AddressPrefix = "10.20.0.0/19"
$vnet2subnet1name = "snet-$vnet2-$location-001"
$vnet2subnet1AdressPrefix = "10.20.1.0/24"
$vnet2subnet2name = "snet-$vnet2-$location-002"
$vnet2subnet2AdressPrefix = "10.20.2.0/24"

################################################################################################

<# VNET
New-AzVirtualNetwork - https://docs.microsoft.com/en-us/powershell/module/az.network/new-azvirtualnetwork?view=azps-7.3.2
Get-AzVirtualNetwork - https://docs.microsoft.com/en-us/powershell/module/az.network/get-azvirtualnetwork?view=azps-7.3.2
Set-AzVirtualNetwork - https://docs.microsoft.com/en-us/powershell/module/az.network/set-azvirtualnetwork?view=azps-7.3.2
Remove-AzVirtualNetwork - https://docs.microsoft.com/en-us/powershell/module/az.network/remove-azvirtualnetwork?view=azps-7.3.2
#>

<#
VNET er som alle andre ressurser i Azure og må være i en Resource Group 
En av følgende metoder kan benyttes for å opprette ressurser ved bruk av PowerShell
Fordelen med førstnevnet er at en får god oversikt om kommandoene er lange
Enten: (som kalles splatting: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_splatting?view=powershell-7.2)
$newrg = @{
    Name = $rgvnet
    Location = $location
}
New-AzResourceGroup @newrg
Eller:
New-AzResourceGroup -Name $rgvnet -Location $location
#>
# Oppretter RG med navnet i variablen $rgvnet og legger den på lokasjonen spesifisert i $location
$newrg = @{
    Name = $rgvnet
    Location = $location
}
New-AzResourceGroup @newrg
# New-AzResourceGroup -Name $rgvnet -Location $location
# Remove-AzResourceGroup -Name $rgvnet

# Oppretter VNET1 med oppgitt størrelse og subnett med oppgitt størrelse
$newvnet = @{
    Name = $vnet1name
    AddressPrefix = $vnet1AddressPrefix
    ResourceGroupName = $rgvnet
    Location = $location
}
# Vi legger det nyopprettede VNET-et i en variabel, siden vi må oppdatere det med subnett-informasjo
# i kommandoen under for opprettelsen av suvnettet
$vnet1info = New-AzVirtualNetwork @newvnet

# Oppretter snet1 i vnet1
$vnet1subnet1 = @{
    Name = $vnet1subnet1name
    AddressPrefix = $vnet1subnet1AdressPrefix
    VirtualNetwork = $vnet1info
}
$vnet1subnet1info = Add-AzVirtualNetworkSubnetConfig @vnet1subnet1
# Add-AzVirtualNetworkSubnetConfig 
# https://docs.microsoft.com/en-us/powershell/module/az.network/add-azvirtualnetworksubnetconfig?view=azps-7.3.2
# Legger til nylig opprettet subnet i VNET1
$vnet1info = $vnet1info | Set-AzVirtualNetwork

# Oppretter snet2 i vnet1
$vnet1subnet2 = @{
    Name = $vnet1subnet2name
    AddressPrefix = $vnet1subnet2AdressPrefix
    VirtualNetwork = $vnet1info
}
$vnet1subnet2info = Add-AzVirtualNetworkSubnetConfig @vnet1subnet2
# Legger til nylig opprettet subnet i VNET1
$vnet1info = $vnet1info | Set-AzVirtualNetwork

# Oppretter VNET2 med oppgitt størrelse og subnett med oppgitt størrelse uten kommentarer i koden (likt som over)
# VNET2
$newvnet = @{
    Name = $vnet2name
    AddressPrefix = $vnet2AddressPrefix
    ResourceGroupName = $rgvnet
    Location = $location
}
$vnet2info = New-AzVirtualNetwork @newvnet
# Subnet 1 og 2 for VNET2
$vnet2subnet1 = @{
    Name = $vnet2subnet1name
    AddressPrefix = $vnet2subnet1AdressPrefix
    VirtualNetwork = $vnet2info
}
$vnet2subnet1info = Add-AzVirtualNetworkSubnetConfig @vnet2subnet1

$vnet2subnet2 = @{
    Name = $vnet2subnet2name
    AddressPrefix = $vnet2subnet2AdressPrefix
    VirtualNetwork = $vnet2info
}
$vnet2subnet2info = Add-AzVirtualNetworkSubnetConfig @vnet2subnet2
$vnet2info = $vnet2info | Set-AzVirtualNetwork

<# For å Fjerne Subnett: Merk at -VirtualNetwork må inneholder informasjon til VNET
F.eks: 
$virtualNetwork = Get-AzVirtualNetwork -Name <navnpåsubnet>
Remove-AzVirtualNetworkSubnetConfig -Name <navnpåsubnet> -VirtualNetwork $virtualNetwork
$virtualNetwork | Set-AzVirtualNetwork
#>
<#
  10.10.0.0/19        10.20.0.0/19
_________________   _________________
|    VNET 1     |   |    VNET 2     |
|               |   |               |
| SNET1  SNET2  |   | SNET1  SNET2  |
|               |   |               |
|_______________|   |_______________|
  10.10.1.0/24        10.20.1.0/24
  10.10.2.0/24        10.20.2.0/24 
#>


################################################################################################
<# 
Oppretter VM-er i de forskjellige subnetene for teste kommunikasjon mellom subnet og VNET.
En god praksis for å sette passord på VM-er i Azure er å ha et KeyVault med en secret som
representerer passordet som skal settes. Vi kommer tilbake til hva et KeyVault er. 
#>
# Variabler for KeyVault og VM-er
$rgvm = "rg-vm-$location-$initials"
$rgkv = "rg-kv-$location-$initials"
# VM-er
$vmv1s1 = "vm-$initials-v1s1"
$vmv1s2 = "vm-$initials-v1s2"
$vmv2s1 = "vm-$initials-v2s1"
$vmv2s2 = "vm-$initials-v2s2"

# Key Vault - MERKK at random tall vil bli forskjellig for hver gang det kjøres!! Dvs. en må presisere
# hvilket vault en skal hente om en kjører scriptet en annen gang.
$kvrandom = -join ((48..57) + (97..122) | Get-Random -Count 4 | % {[char]$_})
# $kvName = "kv-$kvrandom-$initials"
$kvName = "kv-jhca-tim"
$secretName = "vm-secret-$initials"
$localVMadmin = "melling"

################################################################################################

# RG for VM-er og Key Vault
$newrg = @{
    Name = $rgvm
    Location = $location
}
New-AzResourceGroup @newrg

$newrg = @{
    Name = $rgkv
    Location = $location
}
New-AzResourceGroup @newrg

# Oppretter Azure Key Vault
# New-AzKeyVault - https://docs.microsoft.com/en-us/powershell/module/az.keyvault/new-azkeyvault?view=azps-7.3.2
New-AzKeyVault `
    -VaultName $kvName `
    -ResourceGroupName $rgkv `
    -Location $location `
    -EnabledForDeployment `
    -EnabledForTemplateDeployment `
    -EnabledForDiskEncryption `
    -Sku standard

# Gi deretter tilgangsrettigheter til Azure Key Vault for den påloggede brukerkontoen
Get-AzAdUser | Format-Table # hvis en ønsker å liste ut brukere
# Set-AzKeyVaultAccessPolicy - https://docs.microsoft.com/en-us/powershell/module/az.keyvault/set-azkeyvaultaccesspolicy?view=azps-7.3.2
Set-AzKeyVaultAccessPolicy `
    -VaultName $kvName `
    -ResourceGroupName $rgkv `
    -UserPrincipalName "melling_ntnu.no#EXT#@digsec.onmicrosoft.com" `
    -PermissionsToSecrets get, set, list

# Opprett en secret med passord for VM-er
# Set-AzKeyVaultSecret - https://docs.microsoft.com/en-us/powershell/module/az.keyvault/set-azkeyvaultsecret?view=azps-7.3.2
$password = read-host -assecurestring
Set-AzKeyVaultSecret `
    -VaultName $kvName `
    -Name $secretName `
    -SecretValue $password

# Etter at en har opprettet en secret, kan en hente ut secreten, opprette et nytt PSCredential-objekt
# som kan brukes i f.eks. VM-utrulling.

$secret = Get-AzKeyVaultSecret `
    -VaultName $kvName `
    -Name $secretName
# Create a new PSCredential object
$cred = [PSCredential]::new($localVMadmin,$secret.SecretValue)

################################################################################################
# Oppretter VM-er
# Informasjon som benyttes for VM-ene som skal opprettes
# Array med hashtable: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_hash_tables?view=powershell-7.2
$VNETVMs = @(
  @{
    VmName = $vmv1s1
    nicName = "$vmv1s1-nic"
    ip = "10.10.1.4"
    vmsize = "Standard_D2s_v3"
    vNet = $vnet1name
    subnet = $vnet1subnet1name
  },
  @{
    VmName = $vmv1s2
    nicName = "$vmv1s2-nic"
    ip = "10.10.2.4"
    vmsize = "Standard_D2s_v3"
    vNet = $vnet1name
    subnet = $vnet1subnet2name
  },
  @{
    VmName = $vmv2s1
    nicName = "$vmv2s1-nic"
    ip = "10.20.1.4"
    vmsize = "Standard_D2s_v3"
    vNet = $vnet2name
    subnet = $vnet2subnet1name
  },
  @{
    VmName = $vmv2s2
    nicName = "$vmv2s2-nic"
    ip = "10.20.2.4"
    vmsize = "Standard_D2s_v3"
    vNet = $vnet2name
    subnet = $vnet2subnet2name
  }
)

foreach($vm in $VNETVMs){
 
    $vnetinfo = Get-AzVirtualNetwork -name $vm.vNet
    $subnetinfo = Get-AzVirtualNetworkSubnetConfig -Name $vm.subnet -VirtualNetwork $vnetinfo
    $IPconfig = New-AzNetworkInterfaceIpConfig -Name "IPConfig1" -PrivateIpAddressVersion IPv4 -PrivateIpAddress $vm.ip -SubnetId $subnetinfo.id
    $NIC = New-AzNetworkInterface -Name $vm.nicName -ResourceGroupName $rgvm -Location $location -IpConfiguration $IPconfig
     
    $secret = Get-AzKeyVaultSecret `
    -VaultName $kvName `
    -Name $secretName
    # Create a new PSCredential object
    $cred = [PSCredential]::new($localVMadmin,$secret.
    SecretValue)
     
    $VirtualMachine = New-AzVMConfig -VMName $vm.VmName -VMSize $vm.vmsize
    $VirtualMachine = Set-AzVMOperatingSystem -VM $VirtualMachine -Windows -ComputerName $vm.VmName -Credential $cred -ProvisionVMAgent -EnableAutoUpdate
    $VirtualMachine = Add-AzVMNetworkInterface -VM $VirtualMachine -Id $NIC.Id
    $VirtualMachine = Set-AzVMSourceImage -VM $VirtualMachine -PublisherName 'MicrosoftWindowsServer' -Offer 'WindowsServer' -Skus '2019-Datacenter' -Version latest
    
    # New-AzVM - https://docs.microsoft.com/en-us/powershell/module/az.compute/new-azvm?view=azps-7.3.2
    New-AzVM -ResourceGroupName $rgvm -Location $location -VM $VirtualMachine -Verbose -AsJob
}

# Legger til en brannmurregel så maskinene svarer på PING.
# Invoke-AzVmRunCommand - https://docs.microsoft.com/en-us/powershell/module/az.compute/invoke-azvmruncommand?view=azps-7.3.2
#  
$vmnames = @($vmv1s1, $vmv1s2, $vmv2s1, $vmv2s2)
$vmnames | foreach {
    "VM: " + "$_"
Invoke-AzVmRunCommand `
     -ResourceGroupName $rgvm `
     -VMName $_ `
     -CommandId "RunPowerShellScript" `
     -ScriptPath "v22/azinvoke-firewall.ps1" -AsJob
    }


# Test ping
# Ping mellom VM-er på forskjellige SUBNET i samme VNET fungerer
# Ping mellom VM-er på forskjellige SUBNET i forksjellige VNET fungerer ikke

# PEERING muliggjør kommunikasjon på kryss av VNET
$vnet1vnet2peeringname = "vnet1-vnet2-peering-$initials"
$vnet2vnet1peeringname = "vnet2-vnet1-peering-$initials"
# Henter inn nødvendig informasjon fra VNET og legger det i variabler:
$vnet1info = Get-AzVirtualNetwork -Name $vnet1name
$vnet2info = Get-AzVirtualNetwork -Name $vnet2name

$VNET1toVNET2 = @{
    Name = $vnet1vnet2peeringname
    VirtualNetwork = $vnet1info
    RemoteVirtualNetworkId = $vnet2info.Id
}
Add-AzVirtualNetworkPeering @VNET1toVNET2

$VNET2toVNET1 = @{
    Name = $vnet2vnet1peeringname
    VirtualNetwork = $vnet2info
    RemoteVirtualNetworkId = $vnet1info.Id
}
Add-AzVirtualNetworkPeering @VNET2toVNET1

# Test ping i Portal
# Ping mellom VM-er på forskjellige SUBNET i samme VNET fungerer
# Ping mellom VM-er på forskjellige SUBNET i forksjellige VNET fungerer

$vmnames = @($vmv1s1)
$vmnames | foreach {
    "VM: " + "$_ forsøker å pinge de andre VM-ene"
Invoke-AzVmRunCommand `
     -ResourceGroupName $rgvm `
     -VMName $_ `
     -CommandId "RunPowerShellScript" `
     -ScriptPath "v22/azinvoke-ping.ps1"
    }


################################################################################################
# DEL 2 - UKE 14
################################################################################################

<#
Network Security Groups kan benyttes på både VM NIC og SUBNET, er med på å kontrollere
trafikken som flyter inn på subnettet eller inn på VM-en.
#>
# Henter inn nødvendig informasjon fra VNET og legger det i variabler:
$vnet1info = Get-AzVirtualNetwork -Name $vnet1name
$vnet2info = Get-AzVirtualNetwork -Name $vnet2name

$vnet1subnet1nsg = "$vnet1subnet1name-nsg"
$vnet1subnet2nsg = "$vnet1subnet2name-nsg"
$vnet2subnet1nsg = "$vnet2subnet1name-nsg"
$vnet2subnet2nsg = "$vnet2subnet2name-nsg"

$v1s1nsginfo = New-AzNetworkSecurityGroup -Name $vnet1subnet1nsg -ResourceGroupName $rgvnet -Location $location
$v1s2nsginfo = New-AzNetworkSecurityGroup -Name $vnet1subnet2nsg -ResourceGroupName $rgvnet -Location $location
$v2s1nsginfo = New-AzNetworkSecurityGroup -Name $vnet2subnet1nsg -ResourceGroupName $rgvnet -Location $location
$v2s2nsginfo = New-AzNetworkSecurityGroup -Name $vnet2subnet2nsg -ResourceGroupName $rgvnet -Location $location


# Hekter på opprettet NSG på VNET1 subnet1
Set-AzVirtualNetworkSubnetConfig -Name $vnet1subnet1name `
                                -VirtualNetwork $vnet1info `
                                -AddressPrefix $vnet1subnet1AdressPrefix `
                                -NetworkSecurityGroup $v1s1nsginfo  
$vnet1info | Set-AzVirtualNetwork

# Hekter på opprettet NSG på VNET1 subnet2
Set-AzVirtualNetworkSubnetConfig -Name $vnet1subnet2name `
                                -VirtualNetwork $vnet1info `
                                -AddressPrefix $vnet1subnet2AdressPrefix `
                                -NetworkSecurityGroup $v1s2nsginfo  
$vnet1info | Set-AzVirtualNetwork

# Hekter på opprettet NSG på VNET2 subnet1
Set-AzVirtualNetworkSubnetConfig -Name $vnet2subnet1name `
                                -VirtualNetwork $vnet2info `
                                -AddressPrefix $vnet2subnet1AdressPrefix `
                                -NetworkSecurityGroup $v2s1nsginfo  
$vnet2info | Set-AzVirtualNetwork

# Hekter på opprettet NSG på VNET2 subnet2
Set-AzVirtualNetworkSubnetConfig -Name $vnet2subnet2name `
                                -VirtualNetwork $vnet2info `
                                -AddressPrefix $vnet2subnet2AdressPrefix `
                                -NetworkSecurityGroup $v2s2nsginfo  
$vnet2info | Set-AzVirtualNetwork

# Test ping etter NSG

$vmnames = @($vmv1s1)
$vmnames | foreach {
    "VM: " + "$_ forsøker å pinge de andre VM-ene"
Invoke-AzVmRunCommand `
     -ResourceGroupName $rgvm `
     -VMName $_ `
     -CommandId "RunPowerShellScript" `
     -ScriptPath "v22/azinvoke-ping.ps1"
    }

# Se informasjon om hvilke regler som er satt i NSG
$v1s1nsginfo
$v1s1nsginfo.DefaultSecurityRules
$v1s1nsginfo.DefaultSecurityRules | Where-Object -Property direction -eq "Inbound" | ft
$v1s1nsginfo.DefaultSecurityRules | Where-Object -Property direction -eq "Outbound" | ft

# Så lenge det kommer fra et VNET (med peering) i Azure, tillates trafikken.
# Allow inbound traffic from all VMs in VNET

# Oppdaterer NSG med ny regler - Get-Help <ønsket cmdlet> -Online åpner MS docs i browser (mindblow) 
Get-help Get-AzNetworkSecurityGroup -Online
Get-help Add-AzNetworkSecurityRuleConfig -Online
Get-help Set-AzNetworkSecurityGroup -Online

Get-AzNetworkSecurityGroup -Name $vnet1subnet1nsg `
    -ResourceGroupName $rgvnet | Add-AzNetworkSecurityRuleConfig `
        -Name "http-Rule" `
        -Description "Allow " `
        -Access "Allow" `
        -Protocol "Tcp" `
        -Direction "Inbound" `
        -Priority 100 `
        -SourceAddressPrefix "Internet" `
        -SourcePortRange "*" `
        -DestinationAddressPrefix "*" `
        -DestinationPortRange "80" | Set-AzNetworkSecurityGroup

<#
Nå videreutvikler vi oppsettet vårt med et HUB-VNET hvor vi skal plassere VPN Gateway og Azure Firewall
Det vi må huske å gjøre om HUB-VNET-et skal bestemme hva som tillates ut til VNET1 og VNET2 må vi 
fjerne peeringen mellom VNET1 <-> VNET2
#>

# Fjerner Peering
Get-AzVirtualNetworkPeering -Name $vnet1vnet2peeringname `
-VirtualNetworkName $vnet1name `
-ResourceGroupName $rgvnet | Remove-AzVirtualNetworkPeering -force -AsJob

Get-AzVirtualNetworkPeering -Name $vnet2vnet1peeringname `
-VirtualNetworkName $vnet2name `
-ResourceGroupName $rgvnet | Remove-AzVirtualNetworkPeering -force -AsJob

# Variabler for HUB VNET, subnet og addressprefix

# Hub vNet
$hubvnetName = "vnet-hub-$initials-$location"
$hubvnetaddressprefix = "10.30.0.0/19"
$hubfwsubnetName = "AzureFirewallSubnet" # Må være dette navnet
$hubfwsubnetaddressprefix = "10.30.10.0/26"
$hubvpnsubnetName = "GatewaySubnet" # Må være dette navnet
$hubvpnsubnetaddressprefix = "10.30.31.0/27"
$hubmgmtsubnetName = "managmenetSubnet"
$hubmgmtsubnetaddressprefix = "10.30.1.0/24"
$hubvnet1peeringName = "hub-vnet1-peering-$initials"
$hubvnet2peeringName = "hub-vnet2-peering-$initials"
$vnet1hubpeeringName = "vnet1-hub-peering-$initials"
$vnet2hubpeeringName = "vnet2-hub-peering-$initials"

# Azure VPN Gateway
$AzVPNgwPIpName = "azvpngw-pip-$initials"
$AzVPNgwName = "azvpngw-$initials"
$AzVPNgwConfig = "azvpngwConfig-$initials"
$azVPNgwClientAddressPool="192.168.0.0/24"

# Azure Firewall
$AzFirewallpipName = "azfw-pip-$initials"
$AzFirewallName = "azfw-$initials"
$AzFirewallRouteTableName = "azfirewallRouteTable-$initials"
$azFirewallDefaultRoute = "azfw-defaultroute-$initials"
$AzFirewallNatRuleName = "rpd-fw-$vmv1s1"
$AzFirewallNatRuleCollectionName = "azfw-http-collection"

# Oppretter HUB VNET
$hubvnet = @{
    ResourceGroupName = $rgvnet
    Location = $Location
    Name = $hubvnetName
    AddressPrefix = $hubvnetaddressprefix
}
$hubvnetinfo = New-AzVirtualNetwork @hubvnet 

# Oppretter peering mellom hub og vnet1/2
# Henter først info om nettverkene
$vnet1info = Get-AzVirtualNetwork -Name $vnet1name
$vnet2info = Get-AzVirtualNetwork -Name $vnet2name
$hubvnetinfo = Get-AzVirtualNetwork -Name $hubvnetName
# Oppretter Peering
$HUBtoVNET1 = @{
    Name = $hubvnet1peeringName
    VirtualNetwork = $hubvnetinfo
    RemoteVirtualNetworkId = $vnet1info.Id
}
Add-AzVirtualNetworkPeering @HUBtoVNET1 -AsJob

$HUBtoVNET2 = @{
    Name = $hubvnet2peeringName
    VirtualNetwork = $hubvnetinfo
    RemoteVirtualNetworkId = $vnet2info.Id
}
Add-AzVirtualNetworkPeering @HUBtoVNET2 -AsJob

$VNET1toHUB = @{
    Name = $vnet1hubpeeringName
    VirtualNetwork = $vnet1info
    RemoteVirtualNetworkId = $hubvnetinfo.Id
}
Add-AzVirtualNetworkPeering @VNET1toHUB -AsJob

$HUBtoVNET2 = @{
    Name = $hubvnet2peeringName
    VirtualNetwork = $vnet2info
    RemoteVirtualNetworkId = $hubvnetinfo.Id
}
Add-AzVirtualNetworkPeering @HUBtoVNET2 -AsJob

# Oppretter FW subnet i HUB VNET
$fwsn = @{
    Name = $hubfwsubnetName
    AddressPrefix = $hubfwsubnetaddressprefix
    VirtualNetwork = $hubvnetinfo
}
$fwsubnetinfo = Add-AzVirtualNetworkSubnetConfig @fwsn
# Legger til nylig opprettet subnet i hub-vnet
$hubvnetinfo = $hubvnetinfo | Set-AzVirtualNetwork

# Oppretter VPN GW Subnet i HUB VNET
$gwsn = @{
    Name = $hubvpnsubnetName
    AddressPrefix = $hubvpnsubnetaddressprefix
    VirtualNetwork = $hubvnetinfo
}
$gwsubnetinfo = Add-AzVirtualNetworkSubnetConfig @gwsn
# Legger til nylig opprettet subnet i hub-vnet
$hubvnetinfo = $hubvnetinfo | Set-AzVirtualNetwork

# MGMT Subnet
$mgmtsn = @{
    Name = $hubmgmtsubnetName
    AddressPrefix = $hubmgmtsubnetaddressprefix
    VirtualNetwork = $hubvnetinfo
}
$mgmtsubnetinfo = Add-AzVirtualNetworkSubnetConfig @mgmtsn
# Legger til nylig opprettet subnet i hub-vnet
$hubvnetinfo = $hubvnetinfo | Set-AzVirtualNetwork

# VPN Gateway trenger en public IP
$gwpublicip = @{
    Name = $AzVPNgwPIpName
    ResourceGroupName = $rgvnet
    AllocationMethod ="Dynamic"
    Location = $Location
}
$gwpip = New-AzPublicIpAddress @gwpublicip

$gwSubnet = Get-AzVirtualNetworkSubnetConfig -Name $hubvpnsubnetName -VirtualNetwork $hubvnetinfo
$gwipconf = New-AzVirtualNetworkGatewayIpConfig -Name $AzVPNgwConfig -Subnet $gwSubnet -PublicIpAddress $gwpip

# Oppretter VPN Gateway
New-AzVirtualNetworkGateway -Name $AzVPNgwName -ResourceGroupName $rgvnet `
                            -Location $Location -IpConfigurations $gwipconf -GatewayType Vpn `
                            -VpnType RouteBased -GatewaySku Basic -AsJob

<#
Full artikkel: https://docs.microsoft.com/en-us/azure/vpn-gateway/vpn-gateway-howto-point-to-site-rm-ps
Windowsversjonen av hvordan en oppretter certificat og kobler seg til
For Mac: https://docs.microsoft.com/en-us/azure/vpn-gateway/point-to-site-vpn-client-configuration-azure-cert
#>

# Oppretter sertifikat for sikker tilkobling
$certStoreLocation = "Cert:\CurrentUser\My"
Get-ChildItem -Path "Cert:\CurrentUser\My"

# Oppretter self-signed root certificate
$cert = New-SelfSignedCertificate -Type Custom -KeySpec Signature `
                                    -Subject "CN=P2SRootCert" -KeyExportPolicy Exportable `
                                    -HashAlgorithm sha256 -KeyLength 2048 `
                                    -CertStoreLocation $certStoreLocation -KeyUsageProperty Sign -KeyUsage CertSign

New-SelfSignedCertificate -Type Custom -DnsName "P2SRootClient" -KeySpec Signature `
                            -Subject "CN=P2SRootClient" -KeyExportPolicy Exportable `
                            -HashAlgorithm sha256 -KeyLength 2048 `
                            -CertStoreLocation $certStoreLocation `
                            -Signer $cert -TextExtension @("2.5.29.37={text}1.3.6.1.5.5.7.3.2")

$azVPNgwinfo = Get-AzVirtualNetworkGateway -Name $AzVPNgwName -ResourceGroupName $rgvnet
$rootCaBase64 = [system.convert]::ToBase64String($cert.RawData)

# Disabling BGP
# VPN: You can, optionally use BGP. For details, see BGP with site-to-site VPN connections.
# https://docs.microsoft.com/en-us/azure/vpn-gateway/vpn-gateway-bgp-overview?toc=/azure/virtual-network/toc.json
$azVPNgwinfo.BgpSettings =$null

# Setting the VPN Client Address Pool
Set-AzVirtualNetworkGateway -VirtualNetworkGateway $azVPNgwinfo -VpnClientAddressPool $azVPNgwClientAddressPool

# Uploading the Certificate to the gateway
Add-AzVpnClientRootCertificate -VpnClientRootCertificateName "P2SRootCA" `
                                -VirtualNetworkGatewayname $AzVPNgwName `
                                -ResourceGroupName $rgvnet `
                                -PublicCertData $rootCaBase64

Get-ChildItem -Path "Cert:\CurrentUser\My"

<# MÅ VURDERES - Skal maskin(er) tilkoblet med VPN få kommunisere med spokes? og i såfall med hvilke?
I dette eksemplet konfigureres KUN VNET1 til å tillate Gateway Transit (kan gjøres for så mange og hvilke en vil)
Dvs. når en er tilkoblet med VPN vil "on-prem" maskinen kunne kommunisere med Spoke1
MERK at en må laste ned Client på nytt etter denne konfigurasjonen.
#>

#https://docs.microsoft.com/en-us/azure/vpn-gateway/vpn-gateway-peering-gateway-transit
$LinkHubToVNET1 = Get-AzVirtualNetworkPeering `
                      -VirtualNetworkName $hubvnetName `
                      -ResourceGroupName $rgvnet `
                      -Name $hubvnet1peeringName

$LinkHubToSpoke1.AllowGatewayTransit = $True
$LinkHubToSpoke1.AllowForwardedTraffic = $True

Set-AzVirtualNetworkPeering -VirtualNetworkPeering $LinkHubToVNET1

$LinkVNET1ToHub = Get-AzVirtualNetworkPeering `
                      -VirtualNetworkName $vnet1name `
                      -ResourceGroupName $rgvnet `
                      -Name $vnet1hubpeeringName

$LinkVNET1ToHub.UseRemoteGateways = $True
$LinkVNET1ToHub.AllowForwardedTraffic = $True

Set-AzVirtualNetworkPeering -VirtualNetworkPeering $LinkVNET1ToHub


# AZURE FIREWALL 
$hubvnetinfo = Get-AzVirtualNetwork -Name $hubvnetName # legger hub vNet i variablen $hubvnetinfo
# Azure Firewall trenger en Public IP
$firewallPip = New-AzPublicIpAddress `
                  -ResourceGroupName $rgvnet `
                  -Location $Location `
                  -AllocationMethod Static `
                  -Sku Standard `
                  -Name $AzFirewallpipName

# Oppretter Firewall
$azFirewall = New-AzFirewall `
                 -Name $azFirewallName `
                 -ResourceGroupName $rgvnet  `
                 -Location $Location `
                 -VirtualNetworkName $hubvnetinfo.Name `
                 -PublicIpName $firewallPip.Name -AsJob # husk å senere endre til -PublicIpAddress og .IpAddress 

# Vi må deretter opprette en default route for hvert subnet som skal gå via AzFirewall
# Route table
$AzFirewallRouteTable = New-AzRouteTable `
                -Name $AzFirewallRouteTableName `
                -ResourceGroupName $rgvnet `
                -location $Location

# Oppretter ny route - Default route
Get-AzRouteTable -ResourceGroupName $rgvnet `
                -Name $AzFirewallRouteTableName `
                | Add-AzRouteConfig `
                -Name $azFirewallDefaultRoute `
                -AddressPrefix 0.0.0.0/0 `
                -NextHopType "VirtualAppliance" `
                -NextHopIpAddress $AzFirewall.IpConfigurations.privateipaddress `
                | Set-AzRouteTable

# Hekter på default route på vm subnet i vnet1
$vnet1info = Get-AzVirtualNetwork -Name $vnet1name

Set-AzVirtualNetworkSubnetConfig `
                -VirtualNetwork $vnet1info `
                -Name $vnet1subnet1name `
                -AddressPrefix $vnet1subnet1AdressPrefix `
                -RouteTable $AzFirewallRouteTable | Set-AzVirtualNetwork

############################################################################
# Åpner for ekstern tilkobling gjennom Azure Firewall til VM VNET1 Subnet1 #
############################################################################

$AzFirewallPublicIP = Get-AzPublicIpAddress -name $AzFirewallpipName
$Vm = Get-AzVM -Name $vmv1s1
$nic = Get-AzNetworkInterface -ResourceId $Vm.NetworkProfile.NetworkInterfaces.id
$nic.IpConfigurations.PrivateIpAddress
 
$natrulehttp = New-AzFirewallNatRule `
            -Name $AzFirewallNatRuleName `
            -Protocol "TCP" `
            -SourceAddress "*" `
            -DestinationAddress $AzFirewallPublicIP.IpAddress `
            -DestinationPort "80" `
            -TranslatedAddress $nic.IpConfigurations.PrivateIpAddress `
            -TranslatedPort "80"
                
$httpcollectionrule = New-AzFirewallNatRuleCollection `
                -Name $AzFirewallNatRuleCollectionName `
                -Priority 100 `
                -Rule $natrulehttp
 
$AzFirewall.NatRuleCollections = $httpcollectionrule
$AzFirewall | Set-AzFirewall



$vmnames = @($vmv1s1)
$vmnames | foreach {
    "VM: " + "$_ Installerer IIS"
Invoke-AzVmRunCommand `
     -ResourceGroupName $rgvm `
     -VMName $_ `
     -CommandId "RunPowerShellScript" `
     -ScriptPath "v22/azinvoke-install-iis.ps1"
    }